﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
  	public enum MemChannel
	{
		GetNbrOfMems  = Channel.Channel0,

        GetMemInfo  = Channel.Channel0,

		SetMemErase   = Channel.Channel0,

		MemRead = Channel.Channel1,

		MemWrite = Channel.Channel2
	}

    public class MemPacketHeader
        : OutputPacketHeader, IMemPacketHeader
    {
        public MemPacketHeader(byte headerByte)
			: base(headerByte)
		{
		}

		public MemPacketHeader(Channel channel)
			: base(Port.Mem, channel)
		{
		}
        public MemPacketHeader(MemChannel memChannel)
            :base(Port.Mem, (Channel)memChannel)
        {
        }
    }
}
