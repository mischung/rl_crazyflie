﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    public class LowLvlPacketPayload
        : PacketPayload, ILowLvlPacketPayload
    {
        private byte[] payloadBytes = null;

   		public LowLvlPacketPayload(byte[] payload)
		{
			if (payload == null || 
                (payload[0] == 0x01 && payload.Length != 5) ||
                (payload[0] == 0x00 && payload.Length != 1))
			{
				throw new ArgumentOutOfRangeException("payloadBytes");
			}
            payloadBytes = payload;
		}
        // We set the command byte to 0x01 to set motor output.
        public LowLvlPacketPayload(byte motor1 = 0x00, byte motor2 = 0x00, byte motor3 = 0x00, byte motor4 = 0x00)
        {
            payloadBytes = new byte[] {0x01, motor1, motor2, motor3, motor4};
        }
        // If no motor output is specified, we set the command byte to 0x00 to read data only.
        public LowLvlPacketPayload()
        {
            payloadBytes = new byte[] { 0x00 };
        }
        protected override byte[] GetPacketPayloadBytes()
        {
            return payloadBytes;
        }
    }
}
