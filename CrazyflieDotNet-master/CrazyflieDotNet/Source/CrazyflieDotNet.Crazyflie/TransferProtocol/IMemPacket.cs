﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    public interface IMemPacket
        : IOutputPacket<IMemPacketHeader, IMemPacketPayload>
    {
    }
}
