﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    public class LowLvlPacket
                : OutputPacket<ILowLvlPacketHeader, ILowLvlPacketPayload>, ILowLvlPacket
    {
        public LowLvlPacket(byte[] packetBytes)
			: base(packetBytes)
		{
		}

        public LowLvlPacket(ILowLvlPacketHeader header, ILowLvlPacketPayload payload)
			: base(header, payload)
		{
		}
        protected override ILowLvlPacketHeader ParseHeader(byte[] packetBytes)
        {
            if (packetBytes != null && packetBytes.Length != 0)
            {
                var packetHeader = new LowLvlPacketHeader(packetBytes[0]);
                return packetHeader;
            }

            return null;
        }

        protected override ILowLvlPacketPayload ParsePayload(byte[] packetBytes)
        {
            if (packetBytes != null && packetBytes.Length != 0)
            {
                
                var packetPayload = new LowLvlPacketPayload((byte[])packetBytes);
                return packetPayload;
            }

            return null;
        }

    }
}
