﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    public class LowLvlPacketHeader
        : OutputPacketHeader, ILowLvlPacketHeader
    {
        public LowLvlPacketHeader(byte headerByte)
			: base(headerByte)
		{
		}

		public LowLvlPacketHeader(Channel channel)
			: base(Port.LowLvlCommander, channel)
		{
		}
    }
}
