﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    class AckPacketPayload
                : PacketPayload , IAckPacketPayload
    {
        byte[] payloadBytes = null;

        public AckPacketPayload(byte[] packetBytes)
        {
            if (packetBytes.Length > 1)
            {
                payloadBytes = new byte[packetBytes.Length - 1];
                Array.Copy(packetBytes, 1, payloadBytes, 0, packetBytes.Length - 1);
            }
        }

        protected override byte[] GetPacketPayloadBytes()
        {
            return payloadBytes;
        }
    }
}
