﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    public class MemPacket
                : OutputPacket<IMemPacketHeader, IMemPacketPayload>, IMemPacket
    {
        public MemPacket(byte[] packetBytes)
			: base(packetBytes)
		{
		}

        public MemPacket(IMemPacketHeader header, IMemPacketPayload payload)
			: base(header, payload)
		{
		}
        protected override IMemPacketHeader ParseHeader(byte[] packetBytes)
        {
            if (packetBytes != null && packetBytes.Length != 0)
            {
                var packetHeader = new MemPacketHeader(packetBytes[0]);
                return packetHeader;
            }

            return null;
        }

        protected override IMemPacketPayload ParsePayload(byte[] packetBytes)
        {
            if (packetBytes != null && packetBytes.Length != 0)
            {
                
                var packetPayload = new MemPacketPayload((byte[])packetBytes.Skip(1));
                return packetPayload;
            }

            return null;
        }

    }
}
