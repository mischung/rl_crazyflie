﻿namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
	public enum Port
	{
		Console = 0x00,

		Parameters = 0x02,

		Commander = 0x03,

        Mem = 0x04,

		Logging = 0x05,

		Debugging = 0x0E,

		LinkControl = 0x0F,

        LowLvlCommander = 0x0A,

		All = 0xFF
	}
}