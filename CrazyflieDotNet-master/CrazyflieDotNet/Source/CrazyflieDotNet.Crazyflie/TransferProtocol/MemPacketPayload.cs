﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrazyflieDotNet.Crazyflie.TransferProtocol
{
    public enum MemCommand
	{
		GetNbrOfMems  = 0x01,

        GetMemInfo  = 0x02,

		SetMemErase   = 0x03
	}

    public class MemPacketPayload
        : PacketPayload, IMemPacketPayload
    {
        private byte[] payloadBytes = null;

        public MemPacketPayload(MemCommand command, byte memID = 0x00)
            :base ()
        {
            if (command == MemCommand.GetNbrOfMems)
            {
                payloadBytes = new byte[] { (byte)command };
            }
            else
            {
                payloadBytes = new byte[] { (byte)command, memID };
            }
        }
        public MemPacketPayload(byte memoryID, UInt32 address, byte readLength)
            : base()
        {
            byte[] addressBytes = BitConverter.GetBytes(address);
            payloadBytes = new byte[] { (byte)memoryID, addressBytes[0], addressBytes[1], addressBytes[2], addressBytes[3], readLength };
        }
        public MemPacketPayload(byte memoryID, UInt32 address, byte[] dataToWrite)
            : base()
        {
            byte[] addressBytes = BitConverter.GetBytes(address);
            payloadBytes = new byte[1 + addressBytes.Length + dataToWrite.Length];
            payloadBytes[0] = (byte)memoryID;
            addressBytes.CopyTo(payloadBytes, 1);
            dataToWrite.CopyTo(payloadBytes, addressBytes.Length + 1);
        }
   		public MemPacketPayload(byte[] payload)
		{
			if (payload == null)
			{
				throw new ArgumentNullException("payloadBytes");
			}
            payloadBytes = payload;
		}

        protected override byte[] GetPacketPayloadBytes()
        {
            return payloadBytes;
        }
    }
}
