﻿#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using CrazyflieDotNet.Crazyflie.TransferProtocol;
using CrazyflieDotNet.Crazyradio.Driver;
using log4net;
using log4net.Config;

#endregion

namespace CrazyflieDotNet
{
	/// <summary>
	///     Currently, this Program is only a small Test like executable for testing during development.
	/// </summary>
	internal class Program
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof (Program));

		private static void Main(string[] args)
		{
			SetUpLogging();

			IEnumerable<ICrazyradioDriver> crazyradioDrivers = null;

			try
			{
				Log.Debug("Starting Crazyradio USB dongle tests.");

				crazyradioDrivers = CrazyradioDriver.GetCrazyradios();
			}
			catch (Exception ex)
			{
				Log.Error("Error getting Crazyradios.", ex);
			}

			if (crazyradioDrivers != null && crazyradioDrivers.Any())
			{
				var crazyradioDriver = crazyradioDrivers.First();

				try
				{
					crazyradioDriver.Open();

					var scanResults = crazyradioDriver.ScanChannels(RadioChannel.Channel0, RadioChannel.Channel125);
                    if (scanResults.Any())
                    {
                        var firstScanResult = scanResults.First();

                        var dataRateWithCrazyflie = firstScanResult.DataRate;
                        var channelWithCrazyflie = firstScanResult.Channels.First();

                        crazyradioDriver.DataRate = dataRateWithCrazyflie;
                        crazyradioDriver.Channel = channelWithCrazyflie;
                        var pingPacket = new PingPacket();
                        var pingPacketBytes = pingPacket.GetBytes();

                        var crazyRadioMessenger = new CrazyradioMessenger(crazyradioDriver);
                        IAckPacket ackPacket = null;
                        byte[] ackPacketBytes = null;
                        var loop = true;
                        while (loop)
                        {
                            // test 2 (using CTRP lib)
                            {
                                Log.InfoFormat("Ping Packet Bytes: {0}", BitConverter.ToString(pingPacketBytes));

                                ackPacket = crazyRadioMessenger.SendMessage(new PingPacket());
                                ackPacketBytes = ackPacket.GetBytes();
                                // Sleep for 10ms to prevent overflow of the radio buffers.
                                Thread.Sleep(10);
                                Log.InfoFormat("ACK Response Bytes (using CTRP): {0}", BitConverter.ToString(ackPacketBytes));
                            }

                            if (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Spacebar)
                            {
                                loop = false;
                            }
                        }
                        loop = true;
                        while (loop)
                        {
                            // Latency test (using CTRP lib)
                            {
                                Stopwatch timer = Stopwatch.StartNew();
                                CommanderPacket commPacket = new CommanderPacket(0.0f, 0.0f, 0.0f, 0);
                                byte[] commPacketBytes = commPacket.GetBytes();
                                Log.InfoFormat("Commander Packet Bytes: {0}", BitConverter.ToString(commPacketBytes));
                                ackPacket = null;
                                ackPacketBytes = null;
                                for (int i = 0; i < 1000; i++)
                                {
                                    try
                                    {
                                        ackPacket = crazyRadioMessenger.SendMessage(commPacket);
                                        ackPacketBytes = ackPacket.GetBytes();
                                    }
                                    catch
                                    {
                                        Log.InfoFormat("Packet lost.");
                                    }
                                }
                                timer.Stop();
                                Log.InfoFormat("ACK Response Bytes (using CTRP): {0}. Round trip latency: {1}", BitConverter.ToString(ackPacketBytes), timer.ElapsedMilliseconds.ToString());
                            }

                            if (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Spacebar)
                            {
                                loop = false;
                            }
                        }
                        /*
                        // Latency using memory reads (CTRP lib).
                        MemPacket memPacket = null;
                        do
                        {
                            memPacket = new MemPacket(new MemPacketHeader(MemChannel.GetNbrOfMems), new MemPacketPayload(MemCommand.GetNbrOfMems));
                            ackPacket = crazyRadioMessenger.SendMessage(memPacket);
                            ackPacketBytes = ackPacket.GetBytes();
                        } while (ackPacket.Payload.GetBytes().Length < 2);
                        uint numMemories = ackPacket.Payload.GetBytes()[1];
                        // Proceed with there test if there is at least one memory available.
                        if (numMemories > 0)
                        {
                            do
                            {
                                // Get size of first memory.
                                memPacket = new MemPacket(new MemPacketHeader(MemChannel.GetMemInfo), new MemPacketPayload(MemCommand.GetMemInfo, 0x00));
                                ackPacket = crazyRadioMessenger.SendMessage(memPacket);
                            } while (ackPacket.Payload.GetBytes().Length < 7);
                            byte memSize = (byte)Math.Min(BitConverter.ToUInt32(ackPacket.Payload.GetBytes(), 3), 0x18);
                            loop = true;
                            int counter = 0;
                            int dropped = 0;
                            List<long> latency = new List<long>();
                            //Read the first 24bytes.
                            Stopwatch timer = Stopwatch.StartNew();
                            // Set to hybrid mode.
                            //crazyradioDriver.Mode = RadioMode.HybridMode;
                            while (loop)
                            {
                                ackPacket = null;
                                ackPacketBytes = null;
                                memPacket = new MemPacket(new MemPacketHeader(MemChannel.MemRead), new MemPacketPayload(0x00, 0, memSize));
                                try
                                {

                                    ackPacket = crazyRadioMessenger.SendMessage(memPacket);
                                    ackPacketBytes = ackPacket.Payload.GetBytes();
                                }
                                catch
                                {
                                    Log.InfoFormat("Packet lost.");
                                }

                                if (ackPacketBytes.Length >= 7)
                                {
                                    counter++;
                                    timer.Stop();
                                    latency.Add(timer.ElapsedTicks);
                                    timer = Stopwatch.StartNew();
                                    
                                }
                                else
                                {
                                    dropped++;
                                }
                                if (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Spacebar || counter >= 999)
                                {
                                    loop = false;
                                }
                            }
                            timer.Stop(); 
                            //Compute the Average      
                            double avg = latency.Average();
                            //Perform the Sum of (value-avg)_2_2      
                            double sum = latency.Sum(d => Math.Pow(d - avg, 2));
                            //Put it all together      
                            double stdev = Math.Sqrt((sum) / (latency.Count() - 1));
                            double max = latency.Max();
                            double min = latency.Min();
                            Log.InfoFormat("ACK Response Bytes (using CTRP): {0}. Round trip latency: Avg({1}ms), Stdev({2}ms), Max({3}ms), Min({4}ms) . Packets dropped: {5} %", BitConverter.ToString(ackPacketBytes), avg / Stopwatch.Frequency * 1000, stdev / Stopwatch.Frequency * 1000, max / Stopwatch.Frequency * 1000, min /Stopwatch.Frequency * 1000, (float)dropped / (counter + dropped) * 100f);
                        } */

                        loop = true;
                        int counter = 0;
                        int dropped = 0;
                        List<long> latency = new List<long>();
                        //Read the first 24bytes.
                        Stopwatch timer2 = Stopwatch.StartNew();
                        // Set to hybrid mode.
                        //crazyradioDriver.Mode = RadioMode.HybridMode;
                        while (loop)
                        {
                            ackPacket = null;
                            ackPacketBytes = null;
                            try
                            {

                                ackPacket = crazyRadioMessenger.SendMessage(new LowLvlPacket(new LowLvlPacketHeader(Channel.Channel0), new LowLvlPacketPayload(new byte[] {0x10, 0x10, 0x10, 0x10})));
                                ackPacketBytes = ackPacket.Payload.GetBytes();
                             }
                            catch
                            {
                                Log.InfoFormat("Packet lost.");
                            }

                            if (ackPacketBytes.Length >= 7)
                            {
                                counter++;
                                timer2.Stop();
                                latency.Add(timer2.ElapsedTicks);
                                Thread.Sleep(5);
                                LowLevelCommanderDataBundle data = new LowLevelCommanderDataBundle(ackPacket.Payload.GetBytes());
                                Log.Info(data.DisplayValues("Packet", ""));
                                timer2 = Stopwatch.StartNew();

                            }
                            else
                            {
                                dropped++;
                            }
                            if (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Spacebar || counter >= 99999)
                            {
                                loop = false;
                                ackPacket = crazyRadioMessenger.SendMessage(new LowLvlPacket(new LowLvlPacketHeader(Channel.Channel0), new LowLvlPacketPayload(new byte[] { 0x00, 0x00, 0x00, 0x00 })));
                                ackPacketBytes = ackPacket.Payload.GetBytes();
                            }
                        }
                        timer2.Stop();
                        //Compute the Average      
                        double avg = latency.Average();
                        //Perform the Sum of (value-avg)_2_2      
                        double sum = latency.Sum(d => Math.Pow(d - avg, 2));
                        //Put it all together      
                        double stdev = Math.Sqrt((sum) / (latency.Count() - 1));
                        double max = latency.Max();
                        double min = latency.Min();
                        Log.InfoFormat("ACK Response Bytes (using CTRP): {0}. Round trip latency: Avg({1}ms), Stdev({2}ms), Max({3}ms), Min({4}ms) . Packets dropped: {5} %", BitConverter.ToString(ackPacketBytes), avg / Stopwatch.Frequency * 1000, stdev / Stopwatch.Frequency * 1000, max / Stopwatch.Frequency * 1000, min / Stopwatch.Frequency * 1000, (float)dropped / (counter + dropped) * 100f);
                    }
                    else
                    {
                        Log.Warn("No Crazyflie Quadcopters found!");
                    }
				}
				catch (Exception ex)
				{
					Log.Error("Error testing Crazyradio.", ex);
				}
				finally
				{
					crazyradioDriver.Close();
				}
			}
			else
			{
				Log.Warn("No Crazyradio USB dongles found!");
			}

			Log.Info("Sleepy time...Hit space to exit.");

			var sleep = true;
			while (sleep)
			{
				if (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Spacebar)
				{
					sleep = false;
				}
			}
		}

		private static void SetUpLogging()
		{
			BasicConfigurator.Configure();
		}
	}
}