﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


namespace CrazyflieDotNet
{
    /*
    * Row - 2bytes (int16_t in increments of 0.1 degrees)
    * Pitch - 2bytes (int16_t in increments of 0.1 degrees)
    * Yaw - 2bytes (int16_t in in increments of 0.1 degrees/second)
    * accelerationX - 2bytes (int16_t - in mG).
    * accelerationY - 2bytes (int16_t - in mG).
    * accelerationZ - 2bytes (int16_t - in mG).
    * magX - 2bytes (int16_t magnetometer in mT tesla)
    * magY - 2bytes (int16_t magnetometer in mT tesla)
    * magZ - 2bytes (int16_t magnetometer in mT tesla)
    * Motor outputs - 4bytes (uint8_t for each Motor, in same order as output).
    * Altitude - 4bytes (uint32_t - in cm).
    * battery voltage - 1byte (uint8_t - in 0.1V)
    * time - 2bytes (uint16_t - in ms, value cycles back, therefore should be used for only comparing short time spans).
   static struct LowlevelcommanderCrtpValues
   {
     int16_t roll;
     int16_t pitch;
     int16_t yaw;
     int16_t accX;
     int16_t accY;
     int16_t accZ;
     int16_t magX;
     int16_t magY;
     int16_t magZ;
     uint8_t motor1;
     uint8_t motor2;
     uint8_t motor3;
     uint8_t motor4;
     int32_t altitude;
     uint8_t vbat;
     uint16_t time;
   } __attribute__((packed)) currentValues;

    */

    public class LowLevelCommanderDataBundle
    {
        public Byte Header;
        public Int16 Roll, Pitch, Yaw, AccX, AccY, AccZ, MagX, MagY, MagZ;
        public Byte Motor1, Motor2, Motor3, Motor4, VBattery;
        public Int32 Altitidue;
        public UInt16 Time;

        public LowLevelCommanderDataBundle(byte[] rawBytes)
        {
            Header = rawBytes[0];  
            Roll = BitConverter.ToInt16(rawBytes, 1);
            Pitch = BitConverter.ToInt16(rawBytes, 3);
            Yaw = BitConverter.ToInt16(rawBytes, 5);
            AccX = BitConverter.ToInt16(rawBytes, 7);
            AccY = BitConverter.ToInt16(rawBytes, 9);
            AccZ = BitConverter.ToInt16(rawBytes, 11);
            MagX = BitConverter.ToInt16(rawBytes, 13);
            MagY = BitConverter.ToInt16(rawBytes, 15);
            MagZ = BitConverter.ToInt16(rawBytes, 17);
            Motor1 = rawBytes[19];
            Motor2 = rawBytes[20];
            Motor3 = rawBytes[21];
            Motor4 = rawBytes[22];
            Altitidue = BitConverter.ToInt32(rawBytes, 23);
            VBattery = rawBytes[27];
            Time = BitConverter.ToUInt16(rawBytes, 28);
        }

        public string DisplayValues(string varName, string units)
        {
            return Util.DisplayValues<LowLevelCommanderDataBundle>(varName, this, units);
        }

    }
/*
    static struct LowlevelcommanderCrtpValues
{
  int16_t roll;
  int16_t pitch;
  int16_t yaw;
  int16_t accX;
  int16_t accY;
  int16_t accZ;
  int16_t magX;
  int16_t magY;
  int16_t magZ;
  uint8_t motor1;
  uint8_t motor2;
  uint8_t motor3;
  uint8_t motor4;
  int32_t altitude;
  uint8_t vbat;
  uint16_t time;
} __attribute__((packed)) currentValues;
    */

    public static class Util
    {
        public static bool IsNumber(this object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }

        public static string DisplayValues<T>(string varName, T variable, string units) where T : class
        {
            StringBuilder sb = new StringBuilder(varName + ": (");
            IList<FieldInfo> properties = typeof(T).GetFields();
            foreach (var property in properties)
            {
                if (IsNumber(property.GetValue(variable)))
                {
                    sb.Append(" ").Append(String.Format("{0}: {1:0.000}", property.Name, property.GetValue(variable))).Append(units).Append(",");
                }
                else
                {
                    sb.Append(" ").Append(String.Format("{0}: {1}", property.Name, property.GetValue(variable))).Append(units).Append(",");
                }
            }
            sb.Remove(sb.Length - 1, 1).Append(")").AppendLine();
            return sb.ToString();
        }
    }
}
