/*
 * pwm_capture_mb10x3.h
 *
 *  Created on: Dec 2, 2015
 *      Author: Martin Chung
 *      Pulse width capture driver for the HRLV-MaxSonar ultrasonic range finder.
 */

#ifndef PWM_CAPTURE_MB10X3_H_
#define PWM_CAPTURE_MB10X3_H_
#define MB10x3_UPDATE_FREQ 10

#include <stdbool.h>

/**
 * Initialize the MB1043 range finder.
 */
void pwmMB10X3Init(void);

/**
 * Test MB10X3.
 *
 */
bool pwmMB10X3Test(void);

/**
 * Interrupt service routine handling for timer interrupts.
 */
void pwmMB10X3Isr(void);

/**
 * Get range data from MB10x3 in meters.
 */
float mb10X3GetRange(void);




#endif /* PWM_CAPTURE_MB10X3_H_ */
