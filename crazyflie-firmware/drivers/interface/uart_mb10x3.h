/*
 * uart_mb10x3.h - HRLV-MaxSonar-EZ ultra-sound range finder using the RS323 output.
 *
 *  Created on: Nov 28, 2015
 *      Author: Martin Chung
 *
 *
 */

#ifndef UART_MB10X3_H_
#define UART_MB10X3_H_

#include <stdbool.h>

#include "crtp.h"
#include "eprintf.h"

#define EXT_UART1               //TX1/RX1 Serial Port on LEFT Side Pins

#if defined (EXT_UART1)         //UART4 does not have flow control
// APB1 - uart4-5 & usart2-3
// APB2 - usart1,6
  #define UARTn_TYPE             UART4
  #define UARTn_PERIF            RCC_APB1Periph_UART4
  #define ENABLE_UARTn_RCC       RCC_APB1PeriphClockCmd
  #define UARTn_IRQ              UART4_IRQn

  #define UARTn_DMA_IRQ          DMA1_Stream2_IRQn
  #define UARTn_DMA_IT_TC        DMA1_IT_TC
  #define UARTn_DMA_STREAM       DMA1_Stream2

  #define UARTn_GPIO_PERIF       RCC_AHB1Periph_GPIOC
  #define UARTn_GPIO_PORT        GPIOC
  #define UARTn_GPIO_TX_PIN      GPIO_Pin_10
  #define UARTn_GPIO_RX_PIN      GPIO_Pin_11
  #define UARTn_GPIO_AF_TX_PIN   GPIO_PinSource10
  #define UARTn_GPIO_AF_RX_PIN   GPIO_PinSource11
  #define UARTn_GPIO_AF_TX       GPIO_AF_UART4
  #define UARTn_GPIO_AF_RX       GPIO_AF_UART4

#else                           //EXT_UART2
// APB1 - uart4-5 & usart2-3
// APB2 - usart1,6
  #define UARTn_TYPE             USART2
  #define UARTn_PERIF            RCC_APB1Periph_USART2
  #define ENABLE_UARTn_RCC       RCC_APB1PeriphClockCmd
  #define UARTn_IRQ              USART2_IRQn

  #define UARTn_DMA_IRQ          DMA1_Stream5_IRQn
  #define UARTn_DMA_IT_TC        DMA1_IT_TC
  #define UARTn_DMA_STREAM       DMA1_Stream5

  #define UARTn_GPIO_PERIF       RCC_AHB1Periph_GPIOA
  #define UARTn_GPIO_PORT        GPIOA
  #define UARTn_GPIO_TX_PIN      GPIO_Pin_2
  #define UARTn_GPIO_RX_PIN      GPIO_Pin_3
  #define UARTn_GPIO_AF_TX_PIN   GPIO_PinSource2
  #define UARTn_GPIO_AF_RX_PIN   GPIO_PinSource3
  #define UARTn_GPIO_AF_TX       GPIO_AF_USART2
  #define UARTn_GPIO_AF_RX       GPIO_AF_USART2

#ifdef UART_SPINLOOP_FLOWCTRL
  #define UARTn_TXEN_PERIF       RCC_AHB1Periph_GPIOB
  #define UARTn_TXEN_PORT        GPIOB
  #define UARTn_TXEN_PIN         GPIO_Pin_4
  #define UARTn_TXEN_EXTI        EXTI_Line4
#endif

#endif

/**
 * Initialize the MB1043 range finder.
 */
void uartMB10X3Init(void);

/**
 * Test MB10X3.
 *
 */
bool uartMB10X3Test(void);

/**
 * Interrupt service routine handling UART interrupts.
 */
void uartMB10X3Isr(void);

/**
 * Get range data from MB10x3 in meters.
 */
float mb10X3GetRange(void);


#endif /* UART_MB10X3_H_ */
