/*
 * pwm_capture_mb10x3.c
 *
 *  Created on: Dec 2, 2015
 *      Author: Martin Chung
 *      Pulse width capture driver for the HRLV-MaxSonar ultrasonic range finder.
 */


#include <stdbool.h>
#include "stm32fxxx.h"
#include "FreeRTOS.h"
#include "nvicconf.h"
#include "log.h"
#include "cfassert.h"

#define MAX_SONAR_RANGE 5
#define MIN_SONAR_RANGE 0.3
#undef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))

bool isInit = false;
float range = 0; //range in meters.
static TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
static TIM_ICInitTypeDef TIM_ICInitStructure;
static GPIO_InitTypeDef GPIO_InitStructure;
//static DMA_InitTypeDef DMA_InitStructure;
static NVIC_InitTypeDef NVIC_InitStructure;


void pwmMB10X3Init(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9, ENABLE);
	// Configure the GPIO PA2 for the timer output
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	//Map timer to alternate functions
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_TIM9);
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 0x0000ffff; // 1Hz
	TIM_TimeBaseStructure.TIM_Prescaler = 168-1;//168 - 1; //1MHz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM9, &TIM_TimeBaseStructure);
	/* PWM input mode configuration: Channel1 */
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising ;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter = 0;
	TIM_PWMIConfig(TIM9, &TIM_ICInitStructure);
	/* Select input trigger */
	TIM_SelectInputTrigger(TIM9, TIM_TS_TI1FP1);
	/* Select the slave Mode: Reset Mode */
	TIM_SelectSlaveMode(TIM9, TIM_SlaveMode_Reset);
	TIM_SelectMasterSlaveMode(TIM9, TIM_MasterSlaveMode_Enable);
	/* TIM enable counter */
	TIM_Cmd(TIM9, ENABLE);
	/* Enable compare interrupt request */
	TIM_ITConfig(TIM9, TIM_IT_CC1, ENABLE);
	TIM_ITConfig(TIM9, TIM_IT_CC2, ENABLE);
	//TIM_ITConfig(TIM9, TIM_IT_Update, ENABLE);

	/* Enable the TIM global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	isInit = true;
}

bool pwmMB10X3Test(void)
{
	return isInit;
}

void pwmMB10X3Isr(void)
{
	float IC2Value = 0; //Value of counter 1. 0 for start of rise.
	if (TIM_GetITStatus(TIM9, TIM_IT_CC1)) {
			/* Clear pending bit */
			TIM_ClearITPendingBit(TIM9, TIM_IT_CC1);
	}
	if (TIM_GetITStatus(TIM9, TIM_IT_CC2)) {
		/* Clear pending bit */
		TIM_ClearITPendingBit(TIM9, TIM_IT_CC2);
		/* Get the Input Capture value */
		IC2Value = (float)TIM_GetCapture2(TIM9);
		if (IC2Value >= 0 && IC2Value <= MAX_SONAR_RANGE * 1000) {
			//Calculate range at 1usec for 0.001m.
			range = MAX((float)MIN_SONAR_RANGE, (float)TIM_GetCapture2(TIM9) / 1000.0f);
		}
	}
	if (TIM_GetITStatus(TIM9, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM9, TIM_IT_Update);
	}
}

/**
 * Get range data from MB10x3 in meters.
 */
float mb10X3GetRange(void)
{
	return range;
}

LOG_GROUP_START(rangeSensor) LOG_ADD(LOG_FLOAT, range, &range)
LOG_GROUP_STOP(rangeSensor)



