/*
 * vl6180x.c
 *
 *  Created on: Dec 18, 2015
 *      Author: Martin Chung
 */
#include "vl6180x.h"
#include "i2cdev.h"
#include "cpal_i2c.h"
#include "stm32f40x_i2c_cpal_conf.h"
#include "log.h"
#include "cfassert.h"
/*FreeRtos includes*/
#include "FreeRTOS.h"
#include "task.h"
#include "config.h"


#define VL6180X_ADDRESS        0x29

static I2C_Dev *I2Cx;
static bool isInit = false;
static float range = 0.0f; //range in m.
static uint8_t buffer[3];
void vl6180xRxTask(void *param);
int VL6180x_WrByte(uint8_t dev, uint16_t index, uint8_t data);
int VL6180x_RdByte(uint8_t dev, uint16_t index, uint8_t *data);

void vl6180xInit() {
	uint8_t status;
	if (isInit)
		return;
	I2Cx = I2C1_DEV;
	i2cdevInit(I2Cx);
	VL6180x_RdByte(VL6180X_ADDRESS, 0x0016, &status);
	if (status != 0x01) {
		isInit = false;
		ASSERT(false);
		return;
	}
	VL6180x_RdByte(VL6180X_ADDRESS, 0x0000, &status);
	if (status != 0xB4) {
		isInit = false;
		ASSERT(false);
		return;
	}
	isInit = true;
	// Write private registers in the vl6180x, in section 9 of the application note AN4545.
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0207, 0x01);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0208, 0x01);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0096, 0x00);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0097, 0xFD);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00e3, 0x00);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00e4, 0x04);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00e5, 0x02);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00e6, 0x01);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00e7, 0x03);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00f5, 0x02);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00d9, 0x05);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00db, 0xce);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00dc, 0x03);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00dd, 0xf8);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x009f, 0x00);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00a3, 0x3c);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00b7, 0x00);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00bb, 0x3c);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00b2, 0x09);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00ca, 0x09);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0198, 0x01);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x01b0, 0x17);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x01ad, 0x00);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x00ff, 0x05);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0100, 0x05);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0199, 0x05);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x01a6, 0x1b);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x01ac, 0x3e);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x01a7, 0x1f);
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0030, 0x00);

	//Set public registers.
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0011, 0x10); // Enables polling for ‘New Sample ready’ when measurement completes
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x010a, 0x64); // 90ms. Set the averaging sample period (compromise between lower noise and increased execution time)
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x003f, 0x46); // Sets the light and dark gain (upper nibble). Dark gain should not be changed.
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0031, 0xFF); // sets the # of range measurements after which auto calibration of system is performed
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0040, 0x63); // Set ALS integration time to 100ms
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x002e, 0x01); // perform a single temperature calibration of the ranging sensor
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x001b, 0x09); // Inter-measurement ranging period of 100ms.
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x003e, 0x31); // .
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0014, 0x24); // .

	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0016, 0x00); //Poll for system fresh out of reset.

	VL6180x_RdByte(VL6180X_ADDRESS, 0x0000, &status);
	if (status != 0xB4) {
		isInit = false;
		ASSERT(false);
		return;
	}

	// Initialize for continuous range mode.
	isInit &= VL6180x_WrByte(VL6180X_ADDRESS, 0x0018, 0x03);
	xTaskCreate(vl6180xRxTask,
			(const signed char * const )"VL6180X",
			configMINIMAL_STACK_SIZE, NULL, 3, NULL);
	isInit = true;

}
bool vl6180xSelfTest(void) {
	return isInit;
}
//bool vl6180xSetMode(VL6180X_MODES mode);
float vl6180GetRange(void)
{
	return range;
}
//int32_t vl6180GetAmbientLight(float gain, float integrationTime);

void vl6180xRxTask(void *param) {
	uint8_t status;
	uint32_t lastWakeTime;
	uint8_t rangeInMM = 0;
	lastWakeTime = xTaskGetTickCount();
	while (1) {
		vTaskDelayUntil(&lastWakeTime, F2T(10)); // 10Hz
		//Poll for ready signal.
		VL6180x_RdByte(VL6180X_ADDRESS, 0x004F, &status);
		if (status & 0x04) {
			//ASSERT(false);
			//If signal is ready, read range.
			VL6180x_RdByte(VL6180X_ADDRESS, 0x0062, &rangeInMM);
			range = (float)rangeInMM / 1000.0f;
			//Clear interrupt.
			VL6180x_WrByte(VL6180X_ADDRESS, 0x0015, 0x07);
		}
	}
}

int VL6180x_I2CWrite(uint8_t addr, uint8_t  *buff, uint8_t len)
{
    int status;

    status = i2cdevWrite(I2Cx, addr, I2CDEV_NO_MEM_ADDR, len, buff);
    if (status)
      return 0;
    else
      return 1;
}

int VL6180x_I2CRead(uint8_t addr, uint8_t  *buff, uint8_t len)
{
    int status;

    status = i2cdevRead(I2Cx, addr, I2CDEV_NO_MEM_ADDR, len, buff);
    if (status)
      return 0;
    else
      return 1;
}

int VL6180x_WrByte(uint8_t dev, uint16_t index, uint8_t data){
    int  status;

    buffer[0]=index>>8;
    buffer[1]=index&0xFF;
    buffer[2]=data;

    status=VL6180x_I2CWrite(dev, buffer,(uint8_t)3);
    return status;
}

int VL6180x_RdByte(uint8_t dev, uint16_t index, uint8_t *data){
    int  status;

    buffer[0]=index>>8;
    buffer[1]=index&0xFF;

    status=VL6180x_I2CWrite(dev, buffer, (uint8_t)2);
    if( !status ){
        status=VL6180x_I2CRead(dev, buffer,1);
        if( !status ){
            *data=buffer[0];
        }
    }

    return status;
}


LOG_GROUP_START(rangeSensor2) LOG_ADD(LOG_FLOAT, range, &range)
LOG_GROUP_STOP(rangeSensor2)
