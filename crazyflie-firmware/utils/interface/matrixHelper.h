/*
 * matrixHelper.h
 *
 *  Created on: Jan 4, 2016
 *      Author: Martin Chung
 */

#ifndef MATRIXHELPER_H_
#define MATRIXHELPER_H_

#include <stdbool.h>
#include "arm_math.h"

#undef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#undef CLAMP
#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })
#ifndef MATRIX_TRANSPOSE
#define MATRIX_TRANSPOSE(a,b) (arm_mat_trans_f32(&(a), &(b)))
#endif
#ifndef MATRIX_INVERSE
#define MATRIX_INVERSE(a,b) (invertMatrixAndPreserveSource(&(a), &(b)))
#endif
#ifndef MATRIX_SCALE
#define MATRIX_SCALE(a,b,c) (arm_mat_scale_f32(&(a), b, &(c)))
#endif
#ifndef MATRIX_INIT
#define MATRIX_INIT(a,b,c,d) (arm_mat_init_f32(&(a), b, c, d))
#endif
#ifndef MATRIX_MULT
#define MATRIX_MULT(a,b,c) (arm_mat_mult_f32(&(a), &(b), &(c)))
#endif
#ifndef MATRIX_ADD
#define MATRIX_ADD(a,b,c) (arm_mat_add_f32(&(a), &(b), &(c)))
#endif
#ifndef MATRIX_SUB
#define MATRIX_SUB(a,b,c) (arm_mat_sub_f32(&(a), &(b), &(c)))
#endif
#define MATRIX_CLAMP(a,b,c,d) (clampMatrix(&(a), &(b), &(c), &(d)))
#define ASSIGN_MATRIX(a,b) (assignMatrix(&(a), &(b)))
#define COPY_NEW_MATRIX(a,b) (copyAndInitMatrix(&(a), &(b)))
#define IDENTITY_MATRIX_NEW(a,b) (r8mat_identity_new(a, &(b)))
#define EXP_MATRIX(a,b) (expMat(&(a), &(b)))

void assignMatrix(const arm_matrix_instance_f32 *from,
		arm_matrix_instance_f32 *to);
void copyAndInitMatrix(const arm_matrix_instance_f32 *source,
		arm_matrix_instance_f32 *dst);
void clampMatrix(const arm_matrix_instance_f32 *source,
		const arm_matrix_instance_f32 *min, const arm_matrix_instance_f32 *max,
		const arm_matrix_instance_f32 *dst);
float r8mat_norm_li(const arm_matrix_instance_f32 *a);
void r8mat_add(float alpha, const arm_matrix_instance_f32 *a, float beta,
		const arm_matrix_instance_f32 *b, arm_matrix_instance_f32 *c);
void r8mat_identity_new(int n, arm_matrix_instance_f32 *dst);
float r8_log_2(float x);
float r8_big();
void expMat(const arm_matrix_instance_f32 *a, arm_matrix_instance_f32 *dst);
arm_status invertMatrixAndPreserveSource(const arm_matrix_instance_f32 * pSrc,
		arm_matrix_instance_f32 * pDst);

#endif /* MATRIXHELPER_H_ */
