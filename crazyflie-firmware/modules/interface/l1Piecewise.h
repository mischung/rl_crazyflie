/*
 * l1Piecewise.h
 * Implements the L1 state-feedback adaptive control with no unmatched uncertainties with piecewise adaptation.
 * Uses a simple low pass filter D(s) = 1/s.
 *  Created on: Nov 18, 2015
 *      Author: Martin Chung
 */

#ifndef L1PIECEWISE_H_
#define L1PIECEWISE_H_
#include <stdbool.h>
#include "arm_math.h"

typedef struct
{
  arm_matrix_instance_f32 aModel;
  arm_matrix_instance_f32 bModel;
  arm_matrix_instance_f32 cModel;
  arm_matrix_instance_f32 initialState;
  arm_matrix_instance_f32 predState;
  arm_matrix_instance_f32 predOutput;
  arm_matrix_instance_f32 errorState;
  arm_matrix_instance_f32 errorOutput;
  arm_matrix_instance_f32 desiredOutput;
  arm_matrix_instance_f32 u;
  arm_matrix_instance_f32 kG;
  arm_matrix_instance_f32 sigma;
  arm_matrix_instance_f32 phiTs;
  float ts;
  float omega;
  // Temporary matrices for calculations.
  arm_matrix_instance_f32 invPhiTs;
  arm_matrix_instance_f32 expAmodelTs;
  arm_matrix_instance_f32 invAModel;
  arm_matrix_instance_f32 invBModel;
  arm_matrix_instance_f32 invBModelInvPhiTs;
  arm_matrix_instance_f32 invBModelInvPhiTsExpAmodelTs;
  arm_matrix_instance_f32 invBModelInvPhiTsExpAmodelTsError;
  // for updating u.
  arm_matrix_instance_f32 stepwKgr;
  arm_matrix_instance_f32 stepwSigma;
  arm_matrix_instance_f32 stepCurrentU;
  // for updating predicted state.
  arm_matrix_instance_f32 tsAmplusI;
  arm_matrix_instance_f32 tsbModel;
  arm_matrix_instance_f32 tsbModeluPlusSigma;
  arm_matrix_instance_f32 tsAmplusIdentityPredX;
  arm_matrix_instance_f32 uPlusSigma;
} L1Object;

typedef struct
{
  float aModel;
  float bModel;
  float cModel;
  float initialState;
  float predState;
  float predOutput;
  float errorState;
  float errorOutput;
  float desiredOutput;
  float u;
  float kG;
  float sigma;
  float phiTs;
  float ts;
  float omega;
  // Temporary matrices for calculations.
  float invPhiTs;
  float expAmodelTs;
  //float invAModel;
  //float invBModel;
  //float invBModelInvPhiTs;
  float invBModelInvPhiTsExpAmodelTs;
  //float invBModelInvPhiTsExpAmodelTsError;
  // for updating u.
  //float stepwKgr;
  //float stepwSigma;
  //float stepCurrentU;
  // for updating predicted state.
  float tsAmplusI;
  float tsbModel;
  //float tsbModeluPlusSigma;
  //float tsAmplusIdentityPredX;
  //float uPlusSigma;
} L1SISOObject;

/**
 * @brief Initializes the L1 object, assumes initial state is the same as the desired state.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @param[in]     state        vector of initial prediction state of the system.
 * @param[in]     aModel       matrix (Am) describing the dynamics of the model.
 * @param[in]     bModel       matrix (B) describing the dynamics of the model.
 * @param[in]     cModel       matrix (C) describing the dynamics of the model.
 * @param[in]     u            vector (u) describing the input to the plant.
 * @param[in]     omega        value for the bandwidth of the low pass filter.
 * @param[in]     ts		   sampling time.
 * @return        none
 *
 */
void l1Init(L1Object* l1Obj, const arm_matrix_instance_f32 *state, const arm_matrix_instance_f32 *aModel,
             const arm_matrix_instance_f32 *bModel, const arm_matrix_instance_f32 *cModel, const float omega,
			 const float ts);
/**
 * @brief Initializes the L1 SISO (Single input, single output) object, assumes initial state is the same as the desired state.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @param[in]     state        vector of initial prediction state of the system.
 * @param[in]     aModel       matrix (Am) describing the dynamics of the model.
 * @param[in]     bModel       matrix (B) describing the dynamics of the model.
 * @param[in]     cModel       matrix (C) describing the dynamics of the model.
 * @param[in]     u            vector (u) describing the input to the plant.
 * @param[in]     omega        value for the bandwidth of the low pass filter.
 * @param[in]     ts		   sampling time.
 * @return        none
 *
 */
void l1InitSISO(L1SISOObject* l1Obj, const float state, const float aModel,
             const float bModel, const float cModel, const float omega,
			 const float ts);
/**
 * @brief Resets the L1 predicted state to the initial state.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @return        none
 *
 */
void l1Reset(L1Object* l1Obj);
/**
 * @brief Resets the L1 predicted state to the initial state.
 * @param[in,out] *l1SISOObj         instance to all L1 states.
 * @return        none
 *
 */
void l1ResetSISO(L1SISOObject* l1Obj);

/**
 * @brief Updates L1 states and adaptation law.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @param[in] measured           measured states from the plant.
 * @param[in] adaptError         the adaptation law is updated if it is true. For use with a deadband.
 * @return        none
 *
 */
void l1Update(L1Object* l1Obj, const arm_matrix_instance_f32 *measured, const arm_matrix_instance_f32 *minU, const arm_matrix_instance_f32 *maxU, const arm_matrix_instance_f32 *deadZone);
/**
 * @brief Updates L1 states and adaptation law.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @param[in] measured           measured states from the plant.
 * @param[in] adaptError         the adaptation law is updated if it is true. For use with a deadband.
 * @return        none
 *
 */
void l1UpdateSISO(L1SISOObject* l1Obj, const float measured, const float minU, const float maxU, const float deadZone);

/**
 * @brief Set a new desired state to track.
 * @param[in,out] *l1Obj     instance to all L1 states.
 * @param[in] minU           minimum value of u allowed, used for pseudo-control hedging.
 * @param[in] maxU           maximum value of u allowed, used for pseudo-control hedging.
 * @param[in] deadZone       allow adaptation any element of the difference between the predicted and measured outputs is outside deadZone.
 * @return        none
 *
 */
void l1SetDesired(L1Object* l1Obj, const arm_matrix_instance_f32 *desiredState);
/**
 * @brief Set a new desired state to track.
 * @param[in,out] *l1Obj     instance to all L1 states.
 * @param[in] minU           minimum value of u allowed, used for pseudo-control hedging.
 * @param[in] maxU           maximum value of u allowed, used for pseudo-control hedging.
 * @param[in] deadZone       allow adaptation any element of the difference between the predicted and measured outputs is outside deadZone.
 * @return        none
 *
 */
void l1SetDesiredSISO(L1SISOObject* l1Obj, const float desiredState);

/**
 * @brief Set a new sampling time.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @param[in] ts       		     sampling time.
 * @return        none
 *
 */
void l1SetDt(L1Object* l1Obj, const float ts);
/**
 * @brief Set a new sampling time.
 * @param[in,out] *l1Obj         instance to all L1 states.
 * @param[in] ts       		     sampling time.
 * @return        none
 *
 */
void l1SetDtSISO(L1SISOObject* l1Obj, const float ts);

#endif /* L1PIECEWISE_H_ */
