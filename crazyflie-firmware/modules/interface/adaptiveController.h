/*
 * adaptiveController.h
 *
 *  Created on: Oct 29, 2015
 *      Author: Martin Chung
 */

#ifndef ADAPTIVECONTROLLER_H_
#define ADAPTIVECONTROLLER_H_

#include <stdbool.h>
#include "commander.h"
#include "l1Piecewise.h"

#define ADAPTATION_FREQUENCY 1000
#define ADAPTIVE_UPDATE_DT (float)(1.0 / ADAPTATION_FREQUENCY)



void adaptiveControllerInit(void);
bool adaptiveControllerTest(void);

/**
 * Make the controller run an update of the adaptive control. The output is
 * the desired rate which should be fed into a rate controller. The
 * attitude controller can be run in a slower update rate than the rate
 * controller.
 */
void adaptiveControllerCorrectAttitude(
       float eulerRollActual, float eulerPitchActual, float eulerYawActual, float heightActual,
       float eulerRollDesired, float eulerPitchDesired, float eulerYawDesired, float heightDesired,
       float* rollRateDesired, float* pitchRateDesired, float* yawRateDesired, float *verticalVelocityDesired);

/**
 * Make the controller run an update of the rate PID. The output is
 * the actuator force.
 */
void adaptiveControllerCorrectRate(float rollRateActual, float pitchRateActual,
		float yawRateActual, float verticalVelocityActual, float rollRateDesired, float pitchRateDesired,
		float yawRateDesired, float verticalVelocityDesired);

/**
 * Reset controller roll, pitch and yaw PID's.
 */
void adaptiveControllerResetAll(void);

/**
 * Get the actuator output.
 */
void adaptiveControllerGetActuatorOutput(uint16_t* m1, uint16_t* m2,
		uint16_t* m3, uint16_t* m4);

/**
 * Launches the plant with the specified vertical velocity.
 */
void startLaunch(float velocity);

/**
 * Stop Launching the plant.
 */
void stopLaunch(void);

/**
 * Check if plant is launching.
 */
bool isLaunching(void);

#endif /* ADAPTIVECONTROLLER_H_ */
