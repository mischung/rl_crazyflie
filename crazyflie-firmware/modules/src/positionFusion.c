/*
 * positionFusion.c
 *
 *  Created on: Dec 15, 2015
 *      Author: Martin Chung
 */
#include "arm_math.h"
#include "stdbool.h"
#include "positionFusion.h"
#include "matrixHelper.h"

#ifndef UINT32_MAX
#define UINT32_MAX 0xffffffff
#endif

void kalmanPredict(PosFusionObj* obj);
void kalmanUpdate(PosFusionObj* obj);
void resetPositionMeasurements(PosFusionObj* obj);
float extrapolatePositionMeasurement(PosFusionObj* obj);
int factorial(int n);

int factorial(int n) {
	int fact = 1;
	if (n == 0) {
		return 1;
	} else if (n == 1) {
		return fact;
	} else {
		fact = n * factorial(n - 1);
		return fact;
	}
}

void initPosFusion(PosFusionObj* obj, uint32_t tickFreq) {
	obj->tickFreq = tickFreq;
	obj->positionMeasurementIndex = 0;
	obj->positionMeasurementsTaken = false;
	obj->w = 0.001;
	obj->n = MAX_NUMBER_OF_POSITION_MEASUREMENTS;
	obj->processMatrixData[0] = 1.0f;
	obj->processMatrixData[1] = KALMAN_UPDATE_INTERVAL;
	obj->processMatrixData[2] = 0.5f * KALMAN_UPDATE_INTERVAL
			* KALMAN_UPDATE_INTERVAL;
	obj->processMatrixData[3] = 0.0f;
	obj->processMatrixData[4] = 1.0f;
	obj->processMatrixData[5] = KALMAN_UPDATE_INTERVAL;
	obj->processMatrixData[6] = 0.0f;
	obj->processMatrixData[7] = 0.0f;
	obj->processMatrixData[8] = 1.0f;
	obj->observationMatrixData[0] = 1.0f; //We only use position and acceleration measurements only.
	obj->observationMatrixData[8] = 1.0f; //We only use position and acceleration measurements only.
	obj->observationMatrixData[1] = 0.0f;
	obj->observationMatrixData[2] = 0.0f;
	obj->observationMatrixData[3] = 0.0f;
	obj->observationMatrixData[4] = 0.0f;
	obj->observationMatrixData[5] = 0.0f;
	obj->observationMatrixData[6] = 0.0f;
	obj->observationMatrixData[7] = 0.0f;
	obj->errorCovarianceMatrixData[0] = 0.001f;
	obj->errorCovarianceMatrixData[8] = 0.001f;
	obj->errorCovarianceMatrixData[1] = 0.0f;
	obj->errorCovarianceMatrixData[2] = 0.0f;
	obj->errorCovarianceMatrixData[3] = 0.0f;
	obj->errorCovarianceMatrixData[4] = 0.0f;
	obj->errorCovarianceMatrixData[5] = 0.0f;
	obj->errorCovarianceMatrixData[6] = 0.0f;
	obj->errorCovarianceMatrixData[7] = 0.0f;
	obj->errorMatrixData[0] = 0.0025f; // 1mm for position and 0.05ms-2 for acceleration.
	obj->errorMatrixData[8] = 0.05f; // 1mm for position and 0.05ms-2 for acceleration.
	obj->errorMatrixData[1] = 0.0f;
	obj->errorMatrixData[2] = 0.0f;
	obj->errorMatrixData[3] = 0.0f;
	obj->errorMatrixData[4] = 0.0f;
	obj->errorMatrixData[5] = 0.0f;
	obj->errorMatrixData[6] = 0.0f;
	obj->errorMatrixData[7] = 0.0f;
	obj->kalmanGainMatrixData[0] = 0.0f;
	obj->kalmanGainMatrixData[1] = 0.0f;
	obj->kalmanGainMatrixData[2] = 0.0f;
	obj->kalmanGainMatrixData[3] = 0.0f;
	obj->kalmanGainMatrixData[4] = 0.0f;
	obj->kalmanGainMatrixData[5] = 0.0f;
	obj->kalmanGainMatrixData[6] = 0.0f;
	obj->kalmanGainMatrixData[7] = 0.0f;
	obj->kalmanGainMatrixData[8] = 0.0f;
	obj->measurmentNoiseMatrixData[0] = 0.001f;
	obj->measurmentNoiseMatrixData[8] = 0.05f;
	obj->measurmentNoiseMatrixData[1] = 0.0f;
	obj->measurmentNoiseMatrixData[2] = 0.0f;
	obj->measurmentNoiseMatrixData[3] = 0.0f;
	obj->measurmentNoiseMatrixData[4] = 0.0f;
	obj->measurmentNoiseMatrixData[5] = 0.0f;
	obj->measurmentNoiseMatrixData[6] = 0.0f;
	obj->measurmentNoiseMatrixData[7] = 0.0f;
	obj->XPredData[0] = 0.0f;
	obj->XPredData[1] = 0.0f;
	obj->XPredData[2] = 0.0f;
	obj->FtransposeData[0] = 1.0f;
	obj->FtransposeData[1] = KALMAN_UPDATE_INTERVAL;
	obj->FtransposeData[2] = 0.5f * KALMAN_UPDATE_INTERVAL
			* KALMAN_UPDATE_INTERVAL;
	obj->FtransposeData[3] = 0.0f;
	obj->FtransposeData[4] = 1.0f;
	obj->FtransposeData[5] = KALMAN_UPDATE_INTERVAL;
	obj->FtransposeData[6] = 0.0f;
	obj->FtransposeData[7] = 0.0f;
	obj->FtransposeData[8] = 1.0f;
	obj->FPData[0] = 0.0f;
	obj->FPData[1] = 0.0f;
	obj->FPData[2] = 0.0f;
	obj->FPData[3] = 0.0f;
	obj->FPData[4] = 0.0f;
	obj->FPData[5] = 0.0f;
	obj->FPData[6] = 0.0f;
	obj->FPData[7] = 0.0f;
	obj->FPData[8] = 0.0f;
	obj->FPFtransposeData[0] = 0.0f;
	obj->FPFtransposeData[1] = 0.0f;
	obj->FPFtransposeData[2] = 0.0f;
	obj->FPFtransposeData[3] = 0.0f;
	obj->FPFtransposeData[4] = 0.0f;
	obj->FPFtransposeData[5] = 0.0f;
	obj->FPFtransposeData[6] = 0.0f;
	obj->FPFtransposeData[7] = 0.0f;
	obj->FPFtransposeData[8] = 0.0f;
	obj->FXPredData[0] = 0.0f;
	obj->FXPredData[1] = 0.0f;
	obj->FXPredData[2] = 0.0f;
	obj->PPredData[0] = 0.0f;
	obj->PPredData[1] = 0.0f;
	obj->PPredData[2] = 0.0f;
	obj->PPredData[3] = 0.0f;
	obj->PPredData[4] = 0.0f;
	obj->PPredData[5] = 0.0f;
	obj->PPredData[6] = 0.0f;
	obj->PPredData[7] = 0.0f;
	obj->PPredData[8] = 0.0f;
	obj->YData[0] = 0.0f;
	obj->YData[1] = 0.0f;
	obj->YData[2] = 0.0f;
	obj->HXData[0] = 0.0f;
	obj->HXData[1] = 0.0f;
	obj->HXData[2] = 0.0f;
	obj->HtransposeData[0] = 1.0f;
	obj->HtransposeData[8] = 1.0f;
	obj->HtransposeData[1] = 0.0f;
	obj->HtransposeData[2] = 0.0f;
	obj->HtransposeData[3] = 0.0f;
	obj->HtransposeData[4] = 0.0f;
	obj->HtransposeData[5] = 0.0f;
	obj->HtransposeData[6] = 0.0f;
	obj->HtransposeData[7] = 0.0f;
	obj->HPData[0] = 0.0f;
	obj->HPData[1] = 0.0f;
	obj->HPData[2] = 0.0f;
	obj->HPData[3] = 0.0f;
	obj->HPData[4] = 0.0f;
	obj->HPData[5] = 0.0f;
	obj->HPData[6] = 0.0f;
	obj->HPData[7] = 0.0f;
	obj->HPData[8] = 0.0f;
	obj->HPHtransposeData[0] = 0.0f;
	obj->HPHtransposeData[1] = 0.0f;
	obj->HPHtransposeData[2] = 0.0f;
	obj->HPHtransposeData[3] = 0.0f;
	obj->HPHtransposeData[4] = 0.0f;
	obj->HPHtransposeData[5] = 0.0f;
	obj->HPHtransposeData[6] = 0.0f;
	obj->HPHtransposeData[7] = 0.0f;
	obj->HPHtransposeData[8] = 0.0f;
	obj->SData[0] = 0.0f;
	obj->SData[1] = 0.0f;
	obj->SData[2] = 0.0f;
	obj->SData[3] = 0.0f;
	obj->SData[4] = 0.0f;
	obj->SData[5] = 0.0f;
	obj->SData[6] = 0.0f;
	obj->SData[7] = 0.0f;
	obj->SData[8] = 0.0f;
	obj->InvSData[0] = 0.0f;
	obj->InvSData[1] = 0.0f;
	obj->InvSData[2] = 0.0f;
	obj->InvSData[3] = 0.0f;
	obj->InvSData[4] = 0.0f;
	obj->InvSData[5] = 0.0f;
	obj->InvSData[6] = 0.0f;
	obj->InvSData[7] = 0.0f;
	obj->InvSData[8] = 0.0f;
	obj->PHtransposeData[0] = 0.0f;
	obj->PHtransposeData[1] = 0.0f;
	obj->PHtransposeData[2] = 0.0f;
	obj->PHtransposeData[3] = 0.0f;
	obj->PHtransposeData[4] = 0.0f;
	obj->PHtransposeData[5] = 0.0f;
	obj->PHtransposeData[6] = 0.0f;
	obj->PHtransposeData[7] = 0.0f;
	obj->PHtransposeData[8] = 0.0f;
	obj->KYData[0] = 0.0f;
	obj->KYData[1] = 0.0f;
	obj->KYData[2] = 0.0f;
	obj->IData[0] = 1.0f;
	obj->IData[1] = 0.0f;
	obj->IData[2] = 0.0f;
	obj->IData[3] = 0.0f;
	obj->IData[4] = 1.0f;
	obj->IData[5] = 0.0f;
	obj->IData[6] = 0.0f;
	obj->IData[7] = 0.0f;
	obj->IData[8] = 1.0f;
	obj->KHData[0] = 1.0f;
	obj->KHData[1] = 0.0f;
	obj->KHData[2] = 0.0f;
	obj->KHData[3] = 0.0f;
	obj->KHData[4] = 1.0f;
	obj->KHData[5] = 0.0f;
	obj->KHData[6] = 0.0f;
	obj->KHData[7] = 0.0f;
	obj->KHData[8] = 1.0f;
	obj->IminusKHData[0] = 1.0f;
	obj->IminusKHData[1] = 0.0f;
	obj->IminusKHData[2] = 0.0f;
	obj->IminusKHData[3] = 0.0f;
	obj->IminusKHData[4] = 1.0f;
	obj->IminusKHData[5] = 0.0f;
	obj->IminusKHData[6] = 0.0f;
	obj->IminusKHData[7] = 0.0f;
	obj->IminusKHData[8] = 1.0f;
	obj->XUpdData[0] = 0.0f;
	obj->XUpdData[1] = 0.0f;
	obj->XUpdData[2] = 0.0f;
	obj->ZData[0] = 0.0f;
	obj->ZData[1] = 0.0f;
	obj->ZData[2] = 0.0f;
	arm_mat_init_f32(&(obj->processMatrix), 3, 3, obj->processMatrixData); //F
	arm_mat_init_f32(&(obj->observationMatrix), 3, 3, obj->observationMatrixData); //H
	arm_mat_init_f32(&(obj->errorCovarianceMatrix), 3, 3, obj->errorCovarianceMatrixData); //P
	arm_mat_init_f32(&(obj->errorMatrix), 3, 3, obj->errorMatrixData); //Q
	arm_mat_init_f32(&(obj->measurmentNoiseMatrix), 3, 3, obj->measurmentNoiseMatrixData); //R
	arm_mat_init_f32(&(obj->XPred), 3, 1, obj->XPredData); //X (predict)
	arm_mat_init_f32(&(obj->Ftranspose), 3, 3, obj->FtransposeData); //F(transpose)
	MATRIX_TRANSPOSE(obj->processMatrix, obj->Ftranspose);
	arm_mat_init_f32(&(obj->FP), 3, 3, obj->FPData); //F*P
	arm_mat_init_f32(&(obj->FPFtranspose), 3, 3, obj->FPFtransposeData); //F*P*F(transpose)
	arm_mat_init_f32(&(obj->FXPred), 3, 1, obj->FXPredData); //F*X (Prediction phase)
	arm_mat_init_f32(&(obj->PPred), 3, 3, obj->PPredData); //P (Prediction phase)
	arm_mat_init_f32(&(obj->Y), 3, 1, obj->YData); //y
	arm_mat_init_f32(&(obj->HX), 3, 1, obj->HXData); //H * XPred
	arm_mat_init_f32(&(obj->Htranspose), 3, 3, obj->HtransposeData); //H(transpose)
	arm_mat_init_f32(&(obj->HP), 3, 3, obj->HPData); //H * P
	arm_mat_init_f32(&(obj->HPHtranspose), 3, 3, obj->HPHtransposeData); //H * P * H(transpose)
	arm_mat_init_f32(&(obj->S), 3, 3, obj->SData); //S
	arm_mat_init_f32(&(obj->InvS), 3, 3, obj->InvSData); //S (inverse)
	arm_mat_init_f32(&(obj->PHtranspose), 3, 3, obj->PHtransposeData); //P * H (transpose)
	arm_mat_init_f32(&(obj->kalmanGainMatrix), 3, 3, obj->kalmanGainMatrixData); //K
	arm_mat_init_f32(&(obj->KY), 3, 1, obj->KYData); //K * y
	arm_mat_init_f32(&(obj->I), 3, 3, obj->IData); //I
	arm_mat_init_f32(&(obj->KH), 3, 3, obj->KHData); //K * H
	arm_mat_init_f32(&(obj->IminusKH), 3, 3, obj->IminusKHData); // I - K * H
	arm_mat_init_f32(&(obj->XUpd), 3, 1, obj->XUpdData); //X (update)
	arm_mat_init_f32(&(obj->ZUpd), 3, 1, obj->ZData); //Z
	resetPositionMeasurements(obj);
}

void resetPositionMeasurements(PosFusionObj* obj) {
	obj->positionMeasurementIndex = 0;
}

void shiftPositionMeasurements(PosFusionObj* obj) {
	int i;
	for (i = 1; i < obj->positionMeasurementIndex - 1; i++) {
		obj->positionMeasurements[i - 1] = obj->positionMeasurements[i];
		obj->positionMeasurementTimes[i - 1] = obj->positionMeasurementTimes[i];
	}
	obj->positionMeasurementIndex--;
}

void kalmanPredict(PosFusionObj* obj) {
	//Xpred = F * X
	MATRIX_MULT(obj->processMatrix, obj->XUpd, obj->XPred);

	//Ppred = F * P * F(transpose) + Q
	MATRIX_MULT(obj->processMatrix, obj->errorCovarianceMatrix, obj->FP);
	MATRIX_MULT(obj->FP, obj->Ftranspose, obj->FPFtranspose);
	MATRIX_ADD(obj->FPFtranspose, obj->errorMatrix, obj->PPred);

	//H * Xpred
	MATRIX_MULT(obj->observationMatrix, obj->XPred, obj->HX);

}
void kalmanUpdate(PosFusionObj* obj) {
	// y = Z - H * Xpred
	//MATRIX_MULT(obj->observationMatrix, obj->XPred, obj->HX); //(moved to kalmanPredict).
	MATRIX_SUB(obj->ZUpd, obj->HX, obj->Y);
	// S = H * Ppred * H (transpose) + R
	MATRIX_MULT(obj->observationMatrix, obj->PPred, obj->HP);
	MATRIX_MULT(obj->HP, obj->Htranspose, obj->HPHtranspose);
	MATRIX_ADD(obj->HPHtranspose, obj->measurmentNoiseMatrix, obj->S);
	// K = Ppred * H (transpose) * S (inv)
	MATRIX_MULT(obj->PPred, obj->Htranspose, obj->PHtranspose);
	MATRIX_INVERSE(obj->S, obj->InvS);
	MATRIX_MULT(obj->PHtranspose, obj->InvS, obj->kalmanGainMatrix);
	// Xupd = Xpred + K * y
	MATRIX_MULT(obj->kalmanGainMatrix, obj->Y, obj->KY);
	MATRIX_ADD(obj->KY, obj->XPred, obj->XUpd);
	// Pupd = ( I - K * H) * Ppred
	MATRIX_MULT(obj->kalmanGainMatrix, obj->observationMatrix, obj->KH);
	MATRIX_SUB(obj->I, obj->KH, obj->IminusKH);
	MATRIX_MULT(obj->IminusKH, obj->PPred, obj->errorCovarianceMatrix);
}

void accelerationMeasure(PosFusionObj* obj, float measurement, uint32_t time) {
	kalmanPredict(obj);
	obj->latestTime = time;
	//Update with slow measurement points if available.
	if (obj->positionMeasurementsTaken) {
		obj->ZUpd.pData[0] =
				obj->positionMeasurements[obj->positionMeasurementIndex - 1];
		obj->positionMeasurementsTaken = false;
	} else if (obj->positionMeasurementIndex < 2) {
		obj->ZUpd.pData[0] = obj->HX.pData[0];
	} else {
		//else extrapolate slow measurements using the most current state estimates.
		if (obj->positionMeasurementIndex == MAX_NUMBER_OF_POSITION_MEASUREMENTS)
			shiftPositionMeasurements(obj);
		obj->ZUpd.pData[0] = obj->HX.pData[0];//obj->ZUpd.pData[0] = extrapolatePositionMeasurement(obj);
	}
	obj->ZUpd.pData[2] = measurement;
	kalmanUpdate(obj);
}
float extrapolatePositionMeasurement(PosFusionObj* obj) {
	float result = 0.0f;
	float weight = 0.0f;
	int i;
	float w1;
	float w2;
	float Tn;
	obj->positionMeasurements[obj->positionMeasurementIndex] = obj->HX.pData[0];
	obj->positionMeasurementTimes[obj->positionMeasurementIndex] =
			obj->latestTime;
	//if (obj->positionMeasurementTimes[obj->positionMeasurementIndex] < obj->positionMeasurementTimes[obj->positionMeasurementIndex - 1])
	//	obj->positionMeasurementTimes[obj->positionMeasurementIndex]  = UINT16_MAX - obj->positionMeasurementTimes[obj->positionMeasurementIndex - 1] + obj->positionMeasurementTimes[obj->positionMeasurementIndex];
	if (obj->positionMeasurementTimes[obj->positionMeasurementIndex - 1]
			< obj->positionMeasurementTimes[0]) {
		Tn =
		(float)(UINT32_MAX - obj->positionMeasurementTimes[0]
				+ obj->positionMeasurementTimes[obj->positionMeasurementIndex
						- 1]) / (float)obj->tickFreq;
	} else {
		Tn = (float)(obj->positionMeasurementTimes[obj->positionMeasurementIndex - 1]
				- obj->positionMeasurementTimes[0]) / (float)obj->tickFreq;
	}
	if (obj->positionMeasurementTimes[obj->positionMeasurementIndex]
			< obj->positionMeasurementTimes[0]) {
		w1 = -(float)(UINT32_MAX - obj->positionMeasurementTimes[0]
				+ obj->positionMeasurementTimes[obj->positionMeasurementIndex]) / (float)obj->tickFreq
				/ Tn;
	} else {
		w1 = -(float)(obj->positionMeasurementTimes[obj->positionMeasurementIndex]
				- obj->positionMeasurementTimes[0]) / (float)obj->tickFreq / Tn;
	}
	w2 = obj->w / Tn;
	for (i = 0; i <= obj->positionMeasurementIndex; i++) {
		weight = factorial(obj->positionMeasurementIndex)
				/ (factorial(i) * factorial(obj->positionMeasurementIndex - i))
				* pow(w1, obj->positionMeasurementIndex - i) * pow(w2, i);
		result += weight * obj->positionMeasurements[i];
	}
	return result;
}
void positionMeasure(PosFusionObj* obj, float measurement, uint32_t time) {
	//If slow measurement interval is too long, reset slow measurements.
	uint16_t diff;
	if (time
			>= obj->positionMeasurementTimes[obj->positionMeasurementIndex - 1]) {
		diff = time
				- obj->positionMeasurementTimes[obj->positionMeasurementIndex
						- 1];
	} else {
		diff =
		UINT32_MAX
				- obj->positionMeasurementTimes[obj->positionMeasurementIndex
						- 1] + time;
	}
	if (diff > MAX_POSITION_MEASUREMENT_INTERVAL) {
		resetPositionMeasurements(obj);
	}
	if (obj->positionMeasurementIndex == MAX_NUMBER_OF_POSITION_MEASUREMENTS)
		shiftPositionMeasurements(obj);
	obj->positionMeasurements[obj->positionMeasurementIndex] = measurement;
	obj->positionMeasurementTimes[obj->positionMeasurementIndex] = time;
	obj->positionMeasurementIndex++;
	obj->positionMeasurementsTaken = true;
}
float getPositionEstimate(PosFusionObj* obj) {
	return obj->XUpd.pData[0];
}
float getVelocityEstimate(PosFusionObj* obj) {
	return obj->XUpd.pData[1];
}

float getAccelerationEstimate(PosFusionObj* obj) {
	return obj->XUpd.pData[2];
}
