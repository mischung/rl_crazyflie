/*
 * l1Piecewise.c
 *
 *  Created on: Nov 19, 2015
 *      Author: Martin Chung
 */

#include "l1Piecewise.h"
#include <stdbool.h>
#include "cfassert.h"
#include "arm_math.h"
#include "FreeRTOS.h"
#include "matrixHelper.h"

void updateSamplingTimeDependentVariables(L1Object* l1Obj);
void updateSamplingTimeDependentVariablesSISO(L1SISOObject* l1Obj);

void l1Init(L1Object* l1Obj, const arm_matrix_instance_f32 *state,
		const arm_matrix_instance_f32 *aModel,
		const arm_matrix_instance_f32 *bModel,
		const arm_matrix_instance_f32 *cModel, const float omega,
		const float ts) {
	arm_status status;
	COPY_NEW_MATRIX(*aModel, l1Obj->aModel);
	MATRIX_INIT(l1Obj->invAModel, aModel->numRows, aModel->numCols,
			(float32_t * )pvPortMalloc(
					aModel->numRows * aModel->numCols * sizeof(float32_t)));
	status = MATRIX_INVERSE(*aModel, l1Obj->invAModel);
	ASSERT(status == ARM_MATH_SUCCESS);
	MATRIX_INIT(l1Obj->phiTs, aModel->numRows, aModel->numCols,
			(float32_t * )pvPortMalloc(
					aModel->numRows * aModel->numCols * sizeof(float32_t)));
	MATRIX_INIT(l1Obj->invPhiTs, aModel->numRows, aModel->numCols,
			(float32_t * )pvPortMalloc(
					aModel->numRows * aModel->numCols * sizeof(float32_t)));
	COPY_NEW_MATRIX(*bModel, l1Obj->bModel);
	MATRIX_INIT(l1Obj->invBModel, bModel->numRows, bModel->numCols,
			(float32_t * )pvPortMalloc(
					bModel->numRows * bModel->numCols * sizeof(float32_t)));
	status = MATRIX_INVERSE(*bModel, l1Obj->invBModel);
	ASSERT(status == ARM_MATH_SUCCESS);
	COPY_NEW_MATRIX(*cModel, l1Obj->cModel);
	l1Obj->omega = omega;
	l1Obj->ts = ts;
	// Assumes initial state is the starting predicted state and the desired state.
	COPY_NEW_MATRIX(*state, l1Obj->initialState);
	COPY_NEW_MATRIX(*state, l1Obj->predState);
	COPY_NEW_MATRIX(*state, l1Obj->errorState);
	status = MATRIX_SCALE(l1Obj->errorState, 0.0f, l1Obj->errorState);
	ASSERT(status == ARM_MATH_SUCCESS);
	MATRIX_INIT(l1Obj->desiredOutput, l1Obj->cModel.numRows, 1,
			(float32_t * )pvPortMalloc(
					l1Obj->cModel.numRows * sizeof(float32_t)));
	status = MATRIX_SCALE(l1Obj->desiredOutput, 0.0f, l1Obj->desiredOutput);
	ASSERT(status == ARM_MATH_SUCCESS);
	COPY_NEW_MATRIX(l1Obj->desiredOutput, l1Obj->predOutput);
	COPY_NEW_MATRIX(l1Obj->desiredOutput, l1Obj->errorOutput);
	float32_t *CinvAData = (float32_t *) pvPortMalloc(
			l1Obj->aModel.numCols * l1Obj->cModel.numRows * sizeof(float32_t));
	arm_matrix_instance_f32 CinvA = { l1Obj->cModel.numRows,
			l1Obj->aModel.numCols, CinvAData };
	float32_t *CinvABData = (float32_t *) pvPortMalloc(
			l1Obj->bModel.numCols * CinvA.numRows * sizeof(float32_t));
	arm_matrix_instance_f32 CinvAB = { CinvA.numRows, l1Obj->bModel.numCols,
			CinvABData };
	status = MATRIX_MULT(l1Obj->cModel, l1Obj->invAModel, CinvA);
	ASSERT(status == ARM_MATH_SUCCESS);
	status = MATRIX_MULT(CinvA, l1Obj->bModel, CinvAB);
	ASSERT(status == ARM_MATH_SUCCESS);
	COPY_NEW_MATRIX(CinvAB, l1Obj->kG);
	status = MATRIX_INVERSE(CinvAB, l1Obj->kG);
	ASSERT(status == ARM_MATH_SUCCESS);
	status = MATRIX_SCALE(l1Obj->kG, -1.0f, l1Obj->kG);
	ASSERT(status == ARM_MATH_SUCCESS);
	MATRIX_INIT(l1Obj->sigma, l1Obj->bModel.numCols, 1,
			(float32_t * )pvPortMalloc(
					l1Obj->bModel.numCols * sizeof(float32_t)));
	status = MATRIX_SCALE(l1Obj->sigma, 0.0f, l1Obj->sigma);
	ASSERT(status == ARM_MATH_SUCCESS);
	MATRIX_INIT(l1Obj->u, l1Obj->bModel.numCols, 1,
			(float32_t * )pvPortMalloc(
					l1Obj->bModel.numCols * sizeof(float32_t)));
	status = MATRIX_SCALE(l1Obj->u, 0.0f, l1Obj->u);
	ASSERT(status == ARM_MATH_SUCCESS);
	IDENTITY_MATRIX_NEW(l1Obj->aModel.numCols, l1Obj->expAmodelTs); //Assumes aModel is a square matrix.
	MATRIX_INIT(l1Obj->invBModelInvPhiTs, l1Obj->invBModel.numRows,
			l1Obj->invPhiTs.numCols,
			(float32_t * )pvPortMalloc(
					l1Obj->invBModel.numRows * l1Obj->invPhiTs.numCols
							* sizeof(float32_t)));
	MATRIX_INIT(l1Obj->invBModelInvPhiTsExpAmodelTs,
			l1Obj->invBModelInvPhiTs.numRows, l1Obj->expAmodelTs.numCols,
			(float32_t * )pvPortMalloc(
					l1Obj->invBModelInvPhiTs.numRows
							* l1Obj->expAmodelTs.numCols * sizeof(float32_t)));
	MATRIX_INIT(l1Obj->invBModelInvPhiTsExpAmodelTsError,
			l1Obj->invBModelInvPhiTs.numRows, l1Obj->errorState.numCols,
			(float32_t * )pvPortMalloc(
					l1Obj->invBModelInvPhiTs.numRows * l1Obj->errorState.numCols
							* sizeof(float32_t)));
	MATRIX_INIT(l1Obj->stepwKgr, l1Obj->kG.numRows, 1,
			(float32_t * )pvPortMalloc(l1Obj->kG.numRows * sizeof(float32_t)));
	MATRIX_INIT(l1Obj->stepwSigma, l1Obj->sigma.numRows, 1,
			(float32_t * )pvPortMalloc(
					l1Obj->sigma.numRows * sizeof(float32_t)));
	MATRIX_INIT(l1Obj->stepCurrentU, l1Obj->u.numRows, 1,
			(float32_t * )pvPortMalloc(l1Obj->u.numRows * sizeof(float32_t)));
	COPY_NEW_MATRIX(l1Obj->aModel, l1Obj->tsAmplusI);
	COPY_NEW_MATRIX(l1Obj->bModel, l1Obj->tsbModel);
	COPY_NEW_MATRIX(l1Obj->predState, l1Obj->tsbModeluPlusSigma);
	COPY_NEW_MATRIX(l1Obj->predState, l1Obj->tsAmplusIdentityPredX);
	COPY_NEW_MATRIX(l1Obj->u, l1Obj->uPlusSigma);
	updateSamplingTimeDependentVariables(l1Obj);
	vPortFree(CinvA.pData);
	vPortFree(CinvAB.pData);
}
void l1InitSISO(L1SISOObject* l1Obj, const float state, const float aModel,
		const float bModel, const float cModel, const float omega,
		const float ts) {
	l1Obj->aModel = aModel;
	l1Obj->bModel = bModel;
	l1Obj->cModel = cModel;
	l1Obj->omega = omega;
	l1Obj->ts = ts;
	// Assumes initial state is the starting predicted state and the desired state.
	l1Obj->initialState = state;
	l1Obj->predState = state;
	l1Obj->desiredOutput = 0.0f;
	l1Obj->predOutput = 0.0f;
	l1Obj->errorOutput = 0.0f;
	l1Obj->kG = -1.0f / (l1Obj->cModel / l1Obj->aModel * l1Obj->bModel);
	l1Obj->sigma = 0.0f;
	l1Obj->u = 0.0f;
	updateSamplingTimeDependentVariablesSISO(l1Obj);
}

void l1Reset(L1Object* l1Obj) {
	ASSIGN_MATRIX(l1Obj->initialState, l1Obj->predState);
	MATRIX_SCALE(l1Obj->sigma, 0.0f, l1Obj->sigma);
	MATRIX_SCALE(l1Obj->u, 0.0f, l1Obj->u);
}

void l1ResetSISO(L1SISOObject* l1Obj) {
	l1Obj->predState = l1Obj->initialState;
	l1Obj->sigma = 0.0f;
	l1Obj->u = 0.0f;
}

void l1Update(L1Object* l1Obj, const arm_matrix_instance_f32 *measured,
		const arm_matrix_instance_f32 *minU,
		const arm_matrix_instance_f32 *maxU,
		const arm_matrix_instance_f32 *deadZone) {
	float step = l1Obj->ts * l1Obj->omega;
	MATRIX_SUB(l1Obj->predState, *measured, l1Obj->errorState);
	MATRIX_MULT(l1Obj->cModel, l1Obj->predState, l1Obj->predOutput);
	MATRIX_SUB(l1Obj->predOutput, l1Obj->desiredOutput, l1Obj->errorOutput);
// Adapt if any element of the error has exceeded their corresponding deadzone.
	int i;
	bool adapt = false;
	for (i = 0; i < l1Obj->errorOutput.numRows; i++) {
		if (l1Obj->errorOutput.pData[i] > deadZone->pData[i])
			adapt = true;
	}
	if (adapt) {
		// sigma = -invBModel* invPhiTs * expAmodelTs * stateError
		MATRIX_MULT(l1Obj->invBModelInvPhiTsExpAmodelTs, l1Obj->errorState,
				l1Obj->invBModelInvPhiTsExpAmodelTsError);
		MATRIX_SCALE(l1Obj->invBModelInvPhiTsExpAmodelTsError, -1.0f,
				l1Obj->sigma);
	}
//u(t+Ts) = (1- Ts * w) * u(t) + Ts * w * Kg * r(t)  - Ts * w * sigma(t)
	MATRIX_MULT(l1Obj->kG, l1Obj->desiredOutput, l1Obj->stepwKgr);
	MATRIX_SCALE(l1Obj->stepwKgr, step, l1Obj->stepwKgr);
	MATRIX_SCALE(l1Obj->sigma, step, l1Obj->stepwSigma);
	MATRIX_SCALE(l1Obj->u, 1.0f - step, l1Obj->stepCurrentU);
	MATRIX_ADD(l1Obj->stepwKgr, l1Obj->stepCurrentU, l1Obj->u);
	MATRIX_SUB(l1Obj->u, l1Obj->stepwSigma, l1Obj->u);

// clamp u to within bounds.
	MATRIX_CLAMP(l1Obj->u, *minU, *maxU, l1Obj->u);
// predX(t+Ts) = (T * Am + I) predX(t) + T * Bm * (u(t) + sigma(t))
	MATRIX_MULT(l1Obj->tsAmplusI, l1Obj->predState,
			l1Obj->tsAmplusIdentityPredX);
	MATRIX_ADD(l1Obj->u, l1Obj->sigma, l1Obj->uPlusSigma);
	MATRIX_MULT(l1Obj->tsbModel, l1Obj->uPlusSigma, l1Obj->tsbModeluPlusSigma);
	MATRIX_ADD(l1Obj->tsAmplusIdentityPredX, l1Obj->tsbModeluPlusSigma,
			l1Obj->predState);
}

void l1UpdateSISO(L1SISOObject* l1Obj, const float measured, const float minU,
		const float maxU, const float deadZone) {
	float step = l1Obj->ts * l1Obj->omega;
	l1Obj->errorState = l1Obj->predState - measured;
	l1Obj->predOutput = l1Obj->cModel * l1Obj->predState;
	l1Obj->errorOutput = l1Obj->predOutput - l1Obj->desiredOutput;
// Adapt if any element of the error has exceeded their corresponding deadzone.
	bool adapt = false;
	if (l1Obj->errorOutput > deadZone)
		adapt = true;
	if (adapt) {
		// sigma = -invBModel* invPhiTs * expAmodelTs * stateError
		l1Obj->sigma = -l1Obj->invBModelInvPhiTsExpAmodelTs * l1Obj->errorState;
	}
//u(t+Ts) = (1- Ts * w) * u(t) + Ts * w * Kg * r(t)  - Ts * w * sigma(t)
	l1Obj->u = (1 - step) * l1Obj->u + step * l1Obj->kG * l1Obj->desiredOutput
			- step * l1Obj->sigma;
// clamp u to within bounds.
	CLAMP(l1Obj->u, minU, maxU);
// predX(t+Ts) = (T * Am + I) predX(t) + T * Bm * (u(t) + sigma(t))
	l1Obj->predState = l1Obj->tsAmplusI * l1Obj->predState
			+ l1Obj->tsbModel * (l1Obj->u + l1Obj->sigma);
}

void l1SetDesired(L1Object* l1Obj, const arm_matrix_instance_f32 *desiredState) {
	ASSIGN_MATRIX(*desiredState, l1Obj->desiredOutput);
}
void l1SetDesiredSISO(L1SISOObject* l1Obj, const float desiredState) {
	l1Obj->desiredOutput = desiredState;
}

void l1SetDt(L1Object* l1Obj, const float ts) {
	l1Obj->ts = ts;
	updateSamplingTimeDependentVariables(l1Obj);
}

void l1SetDtSISO(L1SISOObject* l1Obj, const float ts) {
	l1Obj->ts = ts;
	updateSamplingTimeDependentVariablesSISO(l1Obj);
}

void updateSamplingTimeDependentVariables(L1Object* l1Obj) {
	arm_status status;
	arm_matrix_instance_f32 aMTs;
	arm_matrix_instance_f32 expAmodelTsMinusI;
	arm_matrix_instance_f32 invA;
	arm_matrix_instance_f32 identity;
	COPY_NEW_MATRIX(l1Obj->aModel, aMTs);
	COPY_NEW_MATRIX(l1Obj->aModel, expAmodelTsMinusI);
	COPY_NEW_MATRIX(l1Obj->aModel, invA);
	IDENTITY_MATRIX_NEW(l1Obj->expAmodelTs.numCols, identity);
// expAmodelTs = exp(aModel * ts);
	status = MATRIX_SCALE(aMTs, l1Obj->ts, aMTs);
	ASSERT(status == ARM_MATH_SUCCESS);
	EXP_MATRIX(aMTs, l1Obj->expAmodelTs);
	status = MATRIX_SUB(l1Obj->expAmodelTs, identity, expAmodelTsMinusI);
	ASSERT(status == ARM_MATH_SUCCESS);
// phiTs = inv(aModel)[exp(aModel * ts) - I]
	status = MATRIX_MULT(l1Obj->invAModel, expAmodelTsMinusI, l1Obj->phiTs);
	ASSERT(status == ARM_MATH_SUCCESS);
	status = MATRIX_INVERSE(l1Obj->phiTs, l1Obj->invPhiTs);
	ASSERT(status == ARM_MATH_SUCCESS);
// sigma = -invBModel* invPhiTs * expAmodelTs * stateError
	status = MATRIX_MULT(l1Obj->invBModel, l1Obj->invPhiTs,
			l1Obj->invBModelInvPhiTs);
	ASSERT(status == ARM_MATH_SUCCESS);
	status = MATRIX_MULT(l1Obj->invBModelInvPhiTs, l1Obj->expAmodelTs,
			l1Obj->invBModelInvPhiTsExpAmodelTs);
	ASSERT(status == ARM_MATH_SUCCESS);
// predX(t+Ts) = (Ts * Am + I) predX(t) + Ts * Bm * (u(t) + sigma(t))
// reuse identity matrix since they are of same size.
	ASSIGN_MATRIX(l1Obj->aModel, l1Obj->tsAmplusI);
	status = MATRIX_SCALE(l1Obj->tsAmplusI, l1Obj->ts, l1Obj->tsAmplusI);
	ASSERT(status == ARM_MATH_SUCCESS);
	status = MATRIX_ADD(l1Obj->tsAmplusI, identity, l1Obj->tsAmplusI);
	ASSERT(status == ARM_MATH_SUCCESS);
	status = MATRIX_SCALE(l1Obj->bModel, l1Obj->ts, l1Obj->tsbModel);
	ASSERT(status == ARM_MATH_SUCCESS);
	vPortFree(aMTs.pData);
	vPortFree(expAmodelTsMinusI.pData);
	vPortFree(invA.pData);
	vPortFree(identity.pData);
}

void updateSamplingTimeDependentVariablesSISO(L1SISOObject* l1Obj) {
// expAmodelTs = exp(aModel * ts);
	l1Obj->expAmodelTs = exp(l1Obj->aModel * l1Obj->ts);
// phiTs = inv(aModel)[exp(aModel * ts) - I]
	l1Obj->phiTs = (l1Obj->expAmodelTs - 1.0f) / l1Obj->aModel;
	l1Obj->invPhiTs = 1.0f / l1Obj->phiTs;
// sigma = -invBModel* invPhiTs * expAmodelTs * stateError
	l1Obj->invBModelInvPhiTsExpAmodelTs = 1.0f / l1Obj->bModel * l1Obj->invPhiTs
			* l1Obj->expAmodelTs;
// predX(t+Ts) = (Ts * Am + I) predX(t) + Ts * Bm * (u(t) + sigma(t))
// reuse identity matrix since they are of same size.
	l1Obj->tsAmplusI = l1Obj->ts * l1Obj->aModel + 1.0f;
	l1Obj->tsbModel = l1Obj->ts * l1Obj->bModel;
}
