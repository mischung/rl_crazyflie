/*
 * lowlevelcommander - Low level flight commander for the stock Crazyflie 2.0.
 *
 * Created on: Aug 26, 2015
 * Author: Martin Chung.
 *
 * Input - Input voltage of the four motors (1 byte each).
 * Output - Crazyflie 2.0 flight information: (conveniently fits into a 29byte packet)
 *
 * Row - 2bytes (int16_t in increments of 0.1 degrees)
 * Pitch - 2bytes (int16_t in increments of 0.1 degrees)
 * Yaw - 2bytes (int16_t in in increments of 0.1 degrees)
 * accelerationX - 2bytes (int16_t - in mG).
 * accelerationY - 2bytes (int16_t - in mG).
 * accelerationZ - 2bytes (int16_t - in mG).
 * magX - 2bytes (int16_t magnetometer in mT tesla)
 * magY - 2bytes (int16_t magnetometer in mT tesla)
 * magZ - 2bytes (int16_t magnetometer in mT tesla)
 * Motor outputs - 4bytes (uint8_t for each Motor, in same order as output).
 * Altitude - 4bytes (uint32_t - in cm).
 * battery voltage - 1byte (uint8_t - in 0.1V)
 * time - 2bytes (uint16_t - in ms, value cycles back, therefore should be used for only comparing short time spans).
 */


#include <math.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"

#include "lowlevelcommander.h"
#include "config.h"
#include "system.h"
#include "pm.h"
#include "sensfusion6.h"
#include "imu.h"
#include "motors.h"
#include "ledseq.h"
#include "crtp.h"
#include "log.h"
#ifdef PLATFORM_CF1
  #include "ms5611.h"
#else
  #include "lps25h.h"
#endif

#undef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#undef CLAMP
#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })
//#define INT16_MAX 32767
//#define INT16_MIN -32678
//#define UINT16_MAX 65535
#define UINT16_MIN 0
#define UINT8_MIN 0
//#define INT32_MAX 2147483647
//#define INT32_MIN -2147483648
#define ATTITUDE_UPDATE_RATE_DIVIDER  1
#define FUSION_UPDATE_DT  (float)(1.0 / (IMU_UPDATE_FREQ / ATTITUDE_UPDATE_RATE_DIVIDER)) // 250hz
#define LOWLEVELCOMMANDER_UPDATE_RATE_DIVIDER  3 // 500hz/5 = 167hz for barometer measurements
#define LOWLEVELCOMMANDER_UPDATE_DT  (float)(1.0 / (IMU_UPDATE_FREQ / LOWLEVELCOMMANDER_UPDATE_RATE_DIVIDER))   // 100hz

static struct LowlevelcommanderCrtpValues
{
  int16_t roll;
  int16_t pitch;
  int16_t yaw;
  int16_t accX;
  int16_t accY;
  int16_t accZ;
  int16_t magX;
  int16_t magY;
  int16_t magZ;
  uint8_t motor1;
  uint8_t motor2;
  uint8_t motor3;
  uint8_t motor4;
  int32_t altitude;
  uint8_t vbat;
  uint16_t time;
} __attribute__((packed)) currentValues;

static CRTPPacket currentPacket;
static bool isInit = false;
static uint32_t lastUpdate = 0;
static uint8_t motor1 = 0;  // Motor 1 power output
static uint8_t motor2 = 0;  // Motor 2 power output
static uint8_t motor3 = 0;  // Motor 3 power output
static uint8_t motor4 = 0;  // Motor 4 power output

static Axis3f gyro; // Gyro axis data in deg/s
static Axis3f acc;  // Accelerometer axis data in mG
static Axis3f mag;  // Magnetometer axis data in testla

static void lowlevelcommanderCrtpCB(CRTPPacket* pk);
static void lowlevelcommanderTask(void* param);
static void writePacket(void);
static void distributePower(void);
static int16_t roundInt16(float x);
//static uint16_t roundUint16(float x);
static int32_t roundInt32(float x);
static uint8_t roundUint8(float x);

void lowlevelcommanderInit(void)
{
	if(isInit)
		return;

	motorsInit();
	imu6Init();
	sensfusion6Init();
	crtpInit();
	crtpRegisterPortCB(CRTP_PORT_LOWLEVELCOMMANDER, lowlevelcommanderCrtpCB);
	lastUpdate = xTaskGetTickCount();

	xTaskCreate(lowlevelcommanderTask, (const signed char * const)LLC_TASK_NAME,
	            LLC_TASK_STACKSIZE, NULL, LLC_TASK_PRI, NULL);
	//xTaskCreate(lowlevelcommanderTask, (const signed char * const)STABILIZER_TASK_NAME,
	//		STABILIZER_TASK_STACKSIZE, NULL, STABILIZER_TASK_PRI, NULL);
	currentPacket.header = CRTP_HEADER(CRTP_PORT_LOWLEVELCOMMANDER, 0x00);
	ASSERT(sizeof(struct LowlevelcommanderCrtpValues) <= CRTP_MAX_DATA_SIZE);
	currentPacket.size = sizeof(struct LowlevelcommanderCrtpValues);

	currentValues.roll = 0;
	currentValues.pitch = 0;
	currentValues.yaw = 0;
   	currentValues.accX = 0;
   	currentValues.accY = 0;
 	currentValues.accZ = 0;
	currentValues.magX = 0;
	currentValues.magY = 0;
	currentValues.magZ = 0;
	currentValues.motor1 = 0;
	currentValues.motor2 = 0;
	currentValues.motor3 = 0;
	currentValues.motor4 = 0;
	currentValues.altitude = 0;
	currentValues.vbat = 0;
	currentValues.time = 0;

	LOG_GROUP_START(mag)
	LOG_ADD(LOG_INT16, x, &currentValues.magX)
	LOG_ADD(LOG_INT16, y, &currentValues.magY)
	LOG_ADD(LOG_INT16, z, &currentValues.magZ)
	LOG_GROUP_STOP(mag)
	isInit = true;
}

bool lowlevelcommanderTest(void)
{
	bool pass = true;

	pass &= motorsTest();
	pass &= imu6Test();
	pass &= sensfusion6Test();
	pass &= crtpTest();

	return pass;
}

static void lowlevelcommanderCrtpCB(CRTPPacket* pk)
{
	if (pk->data[0] == 1)
	{
		if (pk->size == 5)
		{
			motor1 = pk->data[1];
			motor2 = pk->data[2];
			motor3 = pk->data[3];
			motor4 = pk->data[4];
	        distributePower();
	        // Send back a packet after receiving one.
	        crtpSendPacket(&currentPacket);
		}
	}
	else if (pk->data[0] == 0)
	{
		if (pk->size == 1)
			crtpSendPacket(&currentPacket);
	}
}

static void lowlevelcommanderTask(void* param)
{
  uint32_t attitudeCounter = 0;
  uint32_t lowlevelcommanderUpdateCounter = 0;
  uint32_t lastWakeTime;

//  vTaskSetApplicationTaskTag(0, (void*)TASK_LLC_ID_NBR);
//  vTaskSetApplicationTaskTag(0, (void*)TASK_STABILIZER_ID_NBR);

  //Wait for the system to be fully started to start stabilization loop
  systemWaitStart();

  lastWakeTime = xTaskGetTickCount();

  while(1)
  {
    vTaskDelayUntil(&lastWakeTime, F2T(IMU_UPDATE_FREQ)); // 500Hz

    // Magnetometer not yet used more then for logging.

    imu9Read(&gyro, &acc, &mag);

    if (imu6IsCalibrated())
    {
      // 500HZ
      if (++attitudeCounter >= ATTITUDE_UPDATE_RATE_DIVIDER)
      {
   	    sensfusion6UpdateQwithMag(gyro.x, gyro.y, gyro.z, acc.x, acc.y, acc.z, mag.y, mag.x, mag.z, FUSION_UPDATE_DT);
    	//sensfusion6UpdateQ(gyro.x, gyro.y, gyro.z, acc.x, acc.y, acc.z, FUSION_UPDATE_DT);
    	attitudeCounter = 0;
      }
      // 167HZ
      if (++lowlevelcommanderUpdateCounter >= LOWLEVELCOMMANDER_UPDATE_RATE_DIVIDER)
      {
        //sensfusion6UpdateQwithMag(gyro.x, gyro.y, gyro.z, acc.x, acc.y, acc.z, mag.x, mag.y, mag.z, LOWLEVELCOMMANDER_UPDATE_DT);
    	//lastUpdate = xTaskGetTickCount();
        lowlevelcommanderUpdateCounter = 0;
        //distributePower();
        writePacket();
      }
    }
  }
}

static void writePacket(void)
{
	float roll;
	float pitch;
	float yaw;
	float pressure;
	float temperature;
	float aslRaw = 0.0f;
	sensfusion6GetEulerRPY(&roll, &pitch, &yaw);
	//roll = gyro.x;
	//pitch = gyro.y;
	//yaw = gyro.z;
	currentValues.roll = roundInt16(CLAMP(roll * 10, INT16_MIN , INT16_MAX));
	currentValues.pitch = roundInt16(CLAMP(pitch * 10, INT16_MIN , INT16_MAX));
	currentValues.yaw = roundInt16(CLAMP(yaw * 10, INT16_MIN , INT16_MAX));
   	currentValues.accX = roundInt16(CLAMP(acc.x * 1000, INT16_MIN , INT16_MAX));
   	currentValues.accY = roundInt16(CLAMP(acc.y * 1000, INT16_MIN , INT16_MAX));
 	currentValues.accZ = roundInt16(CLAMP(acc.z * 1000, INT16_MIN , INT16_MAX));
	currentValues.magX = roundInt16(CLAMP(mag.x * 1000, INT16_MIN , INT16_MAX));
	currentValues.magY = roundInt16(CLAMP(mag.y * 1000, INT16_MIN , INT16_MAX));
	currentValues.magZ = roundInt16(CLAMP(mag.z * 1000, INT16_MIN , INT16_MAX));
	currentValues.motor1 = (uint8_t)(motorsGetRatio(MOTOR_M1) >> 8);
	currentValues.motor2 = (uint8_t)(motorsGetRatio(MOTOR_M2) >> 8);
	currentValues.motor3 = (uint8_t)(motorsGetRatio(MOTOR_M3) >> 8);
	currentValues.motor4 = (uint8_t)(motorsGetRatio(MOTOR_M4) >> 8);
	if(imuHasBarometer())
	{
#ifdef PLATFORM_CF1
		ms5611GetData(&pressure, &temperature, &aslRaw);
#else
		lps25hGetData(&pressure, &temperature, &aslRaw);
#endif
		currentValues.altitude = roundInt32(CLAMP(aslRaw * 100, INT32_MIN , INT32_MAX)); //Convert altitude (in meters) to cm.
	}
	currentValues.vbat = roundUint8(pmGetBatteryVoltage() * 10);
	currentValues.time = (uint16_t) xTaskGetTickCount();
	memcpy(&currentPacket.data[0], &currentValues, sizeof(struct LowlevelcommanderCrtpValues));
}

static void distributePower(void)
{
  motorsSetRatio(MOTOR_M1, ((uint16_t)motor1) << 8);
  motorsSetRatio(MOTOR_M2, ((uint16_t)motor2) << 8);
  motorsSetRatio(MOTOR_M3, ((uint16_t)motor3) << 8);
  motorsSetRatio(MOTOR_M4, ((uint16_t)motor4) << 8);
}

static int16_t roundInt16(float x)
{
   ASSERT(x >= INT16_MIN-0.5);
   ASSERT(x <= INT16_MAX+0.5);
   if (x >= 0)
      return (int16_t) (x+0.5);
   return (int16_t) (x-0.5);
}
/*static uint16_t roundUint16(float x)
{
   ASSERT(x >= UINT16_MIN-0.5);
   ASSERT(x <= UINT16_MAX+0.5);
   if (x >= 0)
      return (uint16_t) (x+0.5);
   return (uint16_t) (x-0.5);
}*/
static int32_t roundInt32(float x)
{
   ASSERT(x >= INT32_MIN-0.5);
   ASSERT(x <= INT32_MAX+0.5);
   if (x >= 0)
      return (int32_t) (x+0.5);
   return (int32_t) (x-0.5);
}
static uint8_t roundUint8(float x)
{
   ASSERT(x >= UINT8_MIN-0.5);
   ASSERT(x <= UINT8_MAX+0.5);
   if (x >= 0)
      return (uint8_t) (x+0.5);
   return (uint8_t) (x-0.5);
}
