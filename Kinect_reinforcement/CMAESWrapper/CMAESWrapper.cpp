// This is the main DLL file.

#include "stdafx.h"

#include "CMAESWrapper.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\cmaes.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\parameters.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\random.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\timings.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\utils.h"

namespace CMAESWrapper {
	CMA_ES::CMA_ES()
	{
		cmaes = new CMAES<float>();
		parameters = new Parameters<float>();
	}
	void CMA_ES::Init(int dimensions, int lambda, float stStopFitness, float stopMaxIter, array<float>^ xstart, array<float>^ stdev)
	{
		parameters->lambda = lambda;
		parameters->stStopFitness.val = stStopFitness;
		parameters->stStopFitness.flg = true;
		parameters->stopMaxIter = stopMaxIter;
		pin_ptr<float> pxstart = &xstart[0];  
		pin_ptr<float> pstdev = &stdev[0];
		float* npxstart = pxstart;
		float* npstdev = pstdev;
		parameters->init(dimensions, npxstart, npstdev);
		pin_ptr<Parameters<float>> pparameters = parameters;
		Parameters<float> *npparameters = pparameters;
		cmaes->init(*npparameters);
	}
	bool CMA_ES::TestForTermination()
	{
		return cmaes->testForTermination();
	}
	array<float, 2>^ CMA_ES::SamplePopulation()
	{
		array<float, 2>^ result = gcnew array<float, 2>(cmaes->get(CMAES<float>::PopSize), cmaes->get(CMAES<float>::Dimension));
		//Marshal::Copy(IntPtr((void*)cmaes->samplePopulation()), result, 0, sizeof(result));
		float *const* pop = cmaes->samplePopulation();
		for (int i = 0; i < cmaes->get(CMAES<float>::PopSize); i++)
			for (int j = 0; j < cmaes->get(CMAES<float>::Dimension); j++)
				result[i, j] = pop[i][j];
		return result;
	}
	void CMA_ES::UpdateDistribution(array<float>^ fitvals)
	{
		pin_ptr<float> pfitvals = &fitvals[0];
		float* npfitvals = pfitvals;
		cmaes->updateDistribution(npfitvals);
	}
	array<float>^ CMA_ES::GetFBestEver()
	{
		array<float>^ result = gcnew array<float>(cmaes->get(CMAES<float>::Dimension));
		const float* resultPtr = cmaes->getPtr(CMAES<float>::XBestEver);
		for (int i = 0; i < cmaes->get(CMAES<float>::Dimension); i++)
			result[i] = resultPtr[i];
		return result;
	}
	CMA_ES::~CMA_ES()
	{
		delete(cmaes);
		delete(parameters);
	}

}
