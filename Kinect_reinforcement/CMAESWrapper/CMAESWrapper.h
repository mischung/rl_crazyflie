// CMAESWrapper.h

#pragma once


#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\parameters.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\random.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\timings.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\utils.h"
#include "C:\cygwin64\home\Martin\rl_crazyflie\Kinect_reinforcement\CMAES\cmaes.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace CMAESWrapper {

	public ref class CMA_ES
	{
	public:
		CMA_ES();
		// TODO: Add your methods for this class here.
		void Init(int dimensions, int lambda, float stopTolX, float stopMaxIter, array<float>^ xstart, array<float>^ stdev);
		bool TestForTermination();
		array<float, 2>^ SamplePopulation();
		void UpdateDistribution(array<float>^ fitvals);
		array<float>^ GetFBestEver();
		~CMA_ES();
	private:
		CMAES<float> *cmaes;
		Parameters<float> *parameters;

	};
}
