﻿using System;
using Microsoft.Kinect;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Media.Effects;
using System.Windows.Media;
using System.Windows;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Linq;
using Emgu.Util;
using Emgu.CV.Features2D;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.XFeatures2D;
using Emgu.CV.CvEnum;
using Emgu.CV.Cuda;
using Kinect_reinforcement;


// Feature tracking and image display.

namespace Kinect_reinforcement.Feature_Tracking
{
    public struct Coord3D
    {
        public float X, Y, Z;
        public Coord3D(float x = 0, float y = 0, float z = 0)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
    public enum BitmapType
    {
        Color = 0,
        Infrared = 1,
        Depth = 2,
        Coord3D = 3,
        DepthBGRA = 4
    }
    public enum MappedTo
    {
        ColorCoord = 1,
        InfraredCoord = 2,
        DepthCoord = 3
    }    

    public enum FeatureDetector
    {
        SURF,
        FAST
    }
    // Feature tracker object. 
    public class FeatureTracker
    {
        public static string GetKey(BitmapType bmp, MappedTo map)
        {
            return bmp + "," + map;
        }

        public class KeyPointsAndDescriptors
        {
            public VectorOfKeyPoint KeyPoints { get; private set; }
            public Mat Descriptors { get; private set; }

            public KeyPointsAndDescriptors(VectorOfKeyPoint keyPoints, Mat descriptors = null)
            {
                KeyPoints = new VectorOfKeyPoint(keyPoints.ToArray());
                if (descriptors != null)
                {
                    Descriptors = descriptors.Clone();
                }
                else
                {
                    Descriptors = null;
                }
            }
            ~KeyPointsAndDescriptors()
            {
                KeyPoints.Dispose();
                if (Descriptors != null)
                {
                    Descriptors.Dispose();
                }
            }
        }
        public class TrackingOutput 
        {
            public Coord3D[]  CameraSpacePoints;
            public System.Drawing.PointF[] FeaturePoints, FramePoints;
            public TimeSpan FrameTime;
            public float response;

            public TrackingOutput(System.Drawing.PointF[] feaP, System.Drawing.PointF[] frP, Coord3D[] camP, TimeSpan frT, float resp = 0)
            {
                Debug.Assert(feaP.Length == frP.Length && feaP.Length == camP.Length, "TrackingOutput: number of feature point not equal to frame points or camera points.");
                FeaturePoints = feaP;
                FramePoints = frP;
                CameraSpacePoints = camP;
                FrameTime = frT;
                response = resp;
            }
        }
        //Class describing the features detected in a frame.
        public class Features
        {
            public BitmapType FrameType { get; private set; }
            public VectorOfKeyPoint KeyPoints { get; private set; }
            public MappedTo Mapping { get; private set; }
            public Coord3D[][] PointClouds { get; private set; }
            public Rect[] PointCloudMapping { get; private set; } //Mapping of the PointClouds reletive to the KeyPoints
            public Mat Descriptor { get; private set; }
            public TimeSpan Time { get; private set; }

            public Features(BitmapType frameType, MappedTo mapping, VectorOfKeyPoint keyPoints, Coord3D[][] pointClouds, Rect[] pointCloudMapping, TimeSpan time, Mat descriptor = null)
            {
                FrameType = frameType;
                KeyPoints = new VectorOfKeyPoint(keyPoints.ToArray());
                Mapping = mapping;
                PointClouds = pointClouds;
                Descriptor = descriptor;
                Time = time;
                PointCloudMapping = pointCloudMapping;
            }
            ~Features()
            {
                KeyPoints.Dispose();
                if (Descriptor != null)
                {
                    Descriptor.Dispose();
                }
            }
        }
        // Description of the enity to be tracked. Used for feature classification/identification and metrics calculation.
        public class EntityDescriptor
        {
            public Mat Descriptors { get; private set; } //Yes! a managed object. Feature descriptor for matching.
            public Coord3D EntitySize { get; private set; }
            public Coord3D FeatureSize { get; private set; } //Search size for feature and entity (in meters).
            public Func<Features, EntityDescriptor, List<Entity>> FeatureClassifierAndMetricsCalculator { get; private set; } //Function to classify(cluster) features to entities and calculate entity metrics.

            public EntityDescriptor(Func<Features, EntityDescriptor, List<Entity>> featurefunc, Coord3D entitySize, Coord3D featureSize, Mat descriptors = null)
            {
                Descriptors = descriptors;
                FeatureClassifierAndMetricsCalculator = featurefunc;
                EntitySize = entitySize;
                FeatureSize = featureSize;
            }
            // Identifies and calculates entity metrics from a set of features using the FeatureClassifierAndMetricsCalculator.
            public List<Entity> GetEntities(Features features) 
            {
                return FeatureClassifierAndMetricsCalculator(features, this);
            }
        }
        public class CrazyFlieConfiguration1 : EntityDescriptor
        {
            private static Coord3D featureToCenter(Coord3D mean, Coord3D normal)
            {
                float distToCenter = 0.0175f; //0.036m is the distance to the centre of the Crazyflie.
                return new Coord3D(mean.X + normal.X * distToCenter, mean.Y + normal.Y * distToCenter,
                        mean.Z + normal.Z * distToCenter);

            }
            private static List<Entity> featureFunc(Features features, EntityDescriptor entityDescriptor)
            {
                List<Entity> result = new List<Entity>();
                List<Coord3D> featureCenters = new List<Coord3D>();
                //Get positionsAndNormals.
                for (int i = 0; i < features.PointClouds.Length; i++)
                {
                    //Search for valid point of the mapped 2D point closest to the keypoint.
                    bool found = false;
                    Coord3D closestPt = new Coord3D();
                    int MaxIterations = (int)Math.Max(features.PointCloudMapping[i].Width, features.PointCloudMapping[i].Height);
                    int iteration = 0;
                    int[] x = new int[2];
                    int[] y = new int[2];
                    while (!found && iteration < MaxIterations)
                    {
                        x[0] = (int)features.PointCloudMapping[i].X - iteration;
                        x[1] = (int)features.PointCloudMapping[i].X + iteration;
                        y[0] = (int)features.PointCloudMapping[i].Y - iteration;
                        y[1] = (int)features.PointCloudMapping[i].Y + iteration;
                        for (int j = 0; j < 2; j++)
                        {
                            x[j] = (int)Math.Max(0, Math.Min(x[j], features.PointCloudMapping[i].Width));
                            y[j] = (int)Math.Max(0, Math.Min(y[j], features.PointCloudMapping[i].Height));
                        }
                        List<Coord3D> foundCoordsList = new List<Coord3D>();
                        // Search values for the four sides of the rect.
                        for (int horizontal = x[0]; horizontal < x[1]; horizontal++)
                        {
                            Coord3D value = features.PointClouds[i][y[0] * (int)features.PointCloudMapping[i].Width + horizontal];
                            if (!float.IsInfinity(value.X) && !float.IsInfinity(value.Y) && !float.IsInfinity(value.Z)
                                    && !float.IsNaN(value.X) && !float.IsNaN(value.Y) && !float.IsNaN(value.Z))
                            {
                                foundCoordsList.Add(value);
                            }
                            value = features.PointClouds[i][y[1] * (int)features.PointCloudMapping[i].Width + horizontal];
                            if (!float.IsInfinity(value.X) && !float.IsInfinity(value.Y) && !float.IsInfinity(value.Z)
                                    && !float.IsNaN(value.X) && !float.IsNaN(value.Y) && !float.IsNaN(value.Z))
                            {
                                foundCoordsList.Add(value);
                            }
                        }
                        for (int vertical = y[0]; vertical < y[1]; vertical++)
                        {
                            Coord3D value = features.PointClouds[i][vertical * (int)features.PointCloudMapping[i].Width + x[0]];
                            if (!float.IsInfinity(value.X) && !float.IsInfinity(value.Y) && !float.IsInfinity(value.Z)
                                    && !float.IsNaN(value.X) && !float.IsNaN(value.Y) && !float.IsNaN(value.Z))
                            {
                                foundCoordsList.Add(value);
                            }
                            value = features.PointClouds[i][vertical * (int)features.PointCloudMapping[i].Width + x[1]];
                            if (!float.IsInfinity(value.X) && !float.IsInfinity(value.Y) && !float.IsInfinity(value.Z)
                                    && !float.IsNaN(value.X) && !float.IsNaN(value.Y) && !float.IsNaN(value.Z))
                            {
                                foundCoordsList.Add(value);
                            }
                        }
                        if (foundCoordsList.Count > 0)
                        {
                            closestPt = Util.GetMean(foundCoordsList.ToArray());
                            found = true;
                        }
                            iteration++;
                    }
                    //Continue processing if a closestPt is found.
                    if (iteration <= MaxIterations)
                    {
                        List<Coord3D> coordsList = new List<Coord3D>();
                        foreach (Coord3D pt in features.PointClouds[i])
                        {
                            if (Math.Abs(closestPt.X - pt.X) < entityDescriptor.FeatureSize.X &&
                                Math.Abs(closestPt.Y - pt.Y) < entityDescriptor.FeatureSize.Y &&
                                Math.Abs(closestPt.Z - pt.Z) < entityDescriptor.FeatureSize.Z)
                            {
                                coordsList.Add(pt);
                            }
                        }
                        Coord3D pos, norm;
                        Util.GetPositionAndNormal(coordsList.ToArray(), out pos, out norm);
                        featureCenters.Add(featureToCenter(pos, norm));
                    }
                }
                if (featureCenters.Count == 0)
                {
                    return result;
                }
                else if (featureCenters.Count == 1)
                {
                    TimeSpan time = features.Time;
                    result.Add(new Entity(entityDescriptor, time, featureCenters[0]));
                    return result;
                }
                else if (featureCenters.Count == 2)
                {
                    if (Math.Abs(featureCenters[0].X - featureCenters[1].X) < entityDescriptor.EntitySize.X &&
                                Math.Abs(featureCenters[0].Y - featureCenters[1].Y) < entityDescriptor.EntitySize.Y &&
                                Math.Abs(featureCenters[0].Z - featureCenters[1].Z) < entityDescriptor.EntitySize.Z)
                    {
                        TimeSpan time = features.Time;
                        result.Add(new Entity(entityDescriptor, time, Util.GetMean(featureCenters.ToArray())));
                    }
                    else
                    {
                        TimeSpan time = features.Time;
                        result.Add(new Entity(entityDescriptor, time, featureCenters[0]));
                        result.Add(new Entity(entityDescriptor, time, featureCenters[1]));
                    }
                    return result;
                }
                else 
                {
                    float[,] matData = new float[featureCenters.Count,3];
                    for (int i = 0; i < featureCenters.Count; i++)
                    {
                        matData[i, 0] = featureCenters[i].X;
                        matData[i, 1] = featureCenters[i].Y;
                        matData[i, 2] = featureCenters[i].Z;
                    }

                    int k = (int)Math.Ceiling(featureCenters.Count / 2.0);
                    int attempts = 2;
                    MCvTermCriteria terms = new MCvTermCriteria();
                    bool clusteringOK = false;
                    while (!clusteringOK)
                    {
                        Coord3D[] kMeansCentroids = new Coord3D[k];
                        float[] distance = new float[k * (k - 1)];
                        using (Matrix<int> labels = new Matrix<int>(featureCenters.Count, 1))
                        using (Matrix<float> mat = new Matrix<float>(matData))
                        using (Matrix<float> centerMat = new Matrix<float>(k, 3))
                        {
                            CvInvoke.Kmeans(mat, k, labels,  terms, attempts, 0, centerMat);

                            for (int i = 0; i < k; i++)
                            {
                                kMeansCentroids[i] = new Coord3D(centerMat[i, 0], centerMat[i, 1], centerMat[i, 2]);
                            }
                            for (int i = 0; i < distance.Length; i++)
                            {
                                int index1 = (int)Math.Floor(i /(k - 1.0));
                                int index2 = i % (k - 1);
                                if (index2 >= index1)
                                {
                                    index2++;
                                }
                                distance[i] = (float)Util.GetDistance(kMeansCentroids[index1], kMeansCentroids[index2]);
                            }
                            if (distance.Length < 1 || Math.Pow(distance.Min(), 2) > Math.Pow(entityDescriptor.EntitySize.X, 2) + Math.Pow(entityDescriptor.EntitySize.Y, 2) + Math.Pow(entityDescriptor.EntitySize.Z, 2))
                            {
                                clusteringOK = true;
                                for (int i = 0; i < k; i++)
                                {
                                    List<Coord3D> clusterCentroids = new List<Coord3D>();
                                    for (int j = 0; j < featureCenters.Count; j++)
                                    {
                                        if (labels[j, 0] == i)
                                        {
                                            clusterCentroids.Add(featureCenters[j]);
                                        }
                                    }
                                    if (clusterCentroids.Count > 0)
                                    {
                                        TimeSpan time = features.Time;
                                        result.Add(new Entity(entityDescriptor, time, Util.GetMean(clusterCentroids.ToArray())));
                                    }
                                }
                            }
                            else 
                            {
                                k = k - 1;
                            }
                        }
                    }
                    return result;
                }
            }
            public CrazyFlieConfiguration1()
                : base(featureFunc, new Coord3D(0.6f, 0.6f, 0.6f), new Coord3D(0.05f, 0.05f, 0.05f), null) {}
            
        }
        // The Entity/Object tracked.
        public class Entity
        {
            public Coord3D Position { get; private set; }
            public Coord3D Up { get; private set; }
            public Coord3D Forward { get; private set; }
            public TimeSpan Time { get; private set; }
            public EntityDescriptor Descriptor { get; private set; }

            public Entity(EntityDescriptor descriptor, TimeSpan time, Coord3D position, Coord3D up = new Coord3D(), Coord3D forward = new Coord3D())
            {
                Position = position;
                Up = up;
                Forward = forward;
                Time = time;
                Descriptor = descriptor;
            }
        }
        public class Trajectories
        {
            private List<Trajectory> TrajectoryList = new List<Trajectory>();
            TimeSpan LastFrame = new TimeSpan();

            public void UpdateTrajectories(List<Entity> entities)
            {
                // Assumes entities are of the same time frame.
                // Only update when entities from a different time frame (than that of the last update) arrives. 
                if (LastFrame.CompareTo(entities[0].Time) < 0 && entities.Count > 0)
                {
                    LastFrame = entities[0].Time;
                    foreach (var trajectory in TrajectoryList)
                    {
                        trajectory.UpdateTrajectory(entities);
                    }
                    foreach (var entity in entities)
                    {
                        Trajectory trajectory = new Trajectory(entity.Descriptor);
                        List<Entity> tempEntities = new List<Entity>();
                        tempEntities.Add(entity);
                        trajectory.UpdateTrajectory(tempEntities);
                        TrajectoryList.Add(trajectory);
                    }
                }
            }
            public void Clear()
            {
                TrajectoryList.Clear();
            }
            public List<Trajectory> GetTrajectories()
            {
                return TrajectoryList;
            }
        }
        public class TrajectoryUpdatedEventArgs : EventArgs
        {
            public Entity LatestState;

            public TrajectoryUpdatedEventArgs(Entity latest)
            {
                LatestState = latest;
            }
        }
        // Represents the trajectory of the entity.
        public class Trajectory
        {
            public delegate void TrajectoryUpdeatedEventHandler(object sender, TrajectoryUpdatedEventArgs e);
            public event TrajectoryUpdeatedEventHandler trajectoryUpdated;
            public List<Entity> Entities = new List<Entity>(); // List of entites in the trajectory. In chronological order. (Latest at the back).
            public float MaxVelocity { get; private set; } // Maximum Velocity for an entity to be identified in the same trajectory. In meters per second.
            public float MaxTimeOut { get; private set; } // Maximum amount of time allowed between successful update of Entities (in seconds).
            public Entity LatestEntity = null;
            public EntityDescriptor Descriptor { get; private set; }
            public Trajectory(EntityDescriptor descriptor, float maxVelocity = 2.1f, float maxTimeOut = 0.7f)
            {
                Descriptor = descriptor;
                MaxVelocity = maxVelocity;
                MaxTimeOut = maxTimeOut;
            }
            //Check latest position and descriptor to see if the a specific entity belongs to the trajectory. If so, add to Entities.
            public void UpdateTrajectory(List<Entity> entities)
            {
                foreach (Entity entity in entities)
                {
                    if (Entities.Count == 0)
                    {
                        LatestEntity = entity;
                        Entities.Add(entity);
                        break;
                    }
                    else
                    {
                        Entity lastEntry = Entities[Entities.Count - 1];
                        TimeSpan deltaT = entity.Time.Subtract(lastEntry.Time);
                        if (deltaT.TotalSeconds > 0 && (deltaT.TotalSeconds) < MaxTimeOut)
                        {
                            float velocity = (float)Util.GetDistance(lastEntry.Position, entity.Position) / (float)deltaT.TotalSeconds;
                            if (velocity <= MaxVelocity)
                            {
                                LatestEntity = entity;
                                Entities.Add(entity);
                                if (trajectoryUpdated != null)
                                    trajectoryUpdated(this, new TrajectoryUpdatedEventArgs(entity));
                                break;
                            }
                        }
                    }
                }
                entities.Remove(Entities[Entities.Count - 1]);
            }
            public Entity GetLatestEntity()
            {
                return LatestEntity;
            }
        }
        
        public class FeatureDetectionDescriptor
        {
            public FeatureDetector Detector { get; private set; }
            public BitmapType Frame { get; private set; }
            public MappedTo Mapping { get; private set; }
            public Rect Rect { get; private set; }
            public bool UseCuda { get; private set; }
            public bool GetDescriptors { get; private set; }
            public double Threshold { get; private set; }

            public FeatureDetectionDescriptor(FeatureDetector detector, BitmapType frame, MappedTo mapping, Rect rect, bool useCuda, bool getDescriptors, double threshold = 100)
            {
                Detector = detector;
                Frame = frame;
                Mapping = mapping;
                Rect = rect;
                UseCuda = useCuda;
                GetDescriptors = getDescriptors;
                Threshold = threshold;
            }
        }
        public class CrazyFlieConfiguratuion1Features: FeatureDetectionDescriptor
        {
            public CrazyFlieConfiguratuion1Features(double threshold = 100) : base (FeatureDetector.FAST, BitmapType.Infrared, MappedTo.InfraredCoord, new System.Windows.Rect( 0, 0, 15, 15), true, false, threshold) {}
        }

        private KinectDepthColorImageBuilder ImageBuilder;
        private MetricsCalculator Metrics;
        private Trajectories TrajectoryUpdater = new Trajectories();
        private Features ExtractedFeatures = null;
        private FeatureDetectionDescriptor FeatureDescription = null;
        private EntityDescriptor EntityDescription = null;
        public delegate void ImageFrameHandler(object sender, EventArgs e);
        public event ImageFrameHandler imageFrameUpdated;
        public delegate void TrajectoriesUpdatedHandler(object sender, EventArgs e);
        public event TrajectoriesUpdatedHandler trajectoriesUpdated;
        public delegate void FeaturesDetectedHandler(object sender, EventArgs e);
        public event FeaturesDetectedHandler featuresUpdated;
        Image<Gray, byte> grayFrameImage = null;
        public bool IsTracking {get; private set;}
        
        public FeatureTracker()
        {
            BitmapType[] bmps = {BitmapType.Infrared, BitmapType.Depth, BitmapType.Color, BitmapType.Coord3D};
            ImageBuilder = new KinectDepthColorImageBuilder(bmps);
            Metrics = new MetricsCalculator();
            ImageBuilder.FrameArrived += imageBuilder_frameArrived;
        }
        ~FeatureTracker()
        {
            if (grayFrameImage != null)
                grayFrameImage.Dispose();
        }
        public void GetBitmap(BitmapType bmp, MappedTo mapping, Action<WriteableBitmap> callback)
        {
            ImageBuilder.GetBitmap(bmp, mapping, new Rect(0, 0, 0, 0), callback);
        }
        public TimeSpan GetFrameTime()
        {
            return ImageBuilder.GetFrameTime();
        }
        public void TrackFeatures(FeatureDetectionDescriptor featureDescription, EntityDescriptor entityDescription)
        {
            FeatureDescription = featureDescription;
            EntityDescription = entityDescription;
            ResetTracking();
            IsTracking = true;
        }
        public void ResetTracking()
        {
            TrajectoryUpdater.Clear();
        }
        public void PauseTracking()
        {
            IsTracking = false;
        }
        public void ResumeTracking()
        {
            IsTracking = true;
        }
        public List<Trajectory> GetTrajectories()
        {
            return TrajectoryUpdater.GetTrajectories();
        }
        public Features GetFeatures()
        {
            return ExtractedFeatures;
        }

        void imageBuilder_frameArrived(object sender, EventArgs args)
        {
            if (imageFrameUpdated != null)
            {
                imageFrameUpdated(this, null);
            }
            // Place order for the next tracking frame.
            if (FeatureDescription != null && EntityDescription != null && IsTracking == true)
            {
                GetFeatures(FeatureDescription.Detector, FeatureDescription.Frame, FeatureDescription.Mapping, (features) =>
                {
                    ExtractedFeatures = features;
                    if (featuresUpdated != null)
                        featuresUpdated(this, null);
                    List<FeatureTracker.Entity> entities = EntityDescription.GetEntities(features);
                    Console.Write("Features extracted at time: " + features.Time.TotalSeconds + "/r");
                    TrajectoryUpdater.UpdateTrajectories(entities);
                    if (trajectoriesUpdated != null)
                        trajectoriesUpdated(this, null);
                }, FeatureDescription.Rect, FeatureDescription.UseCuda, FeatureDescription.GetDescriptors, FeatureDescription.Threshold);
            }
        }
        protected void GetFeatures(FeatureDetector detector, BitmapType frame, MappedTo mapping, Action<Features> callback, Rect rect, bool useCuda, bool getDescriptors, double threshold = 100)
        {

            if (frame == BitmapType.Coord3D || frame == BitmapType.Depth || frame == BitmapType.DepthBGRA)
            {
                throw new NotImplementedException();
            }
            Action<Util.WriteableBitmapProxy> cb = async (bmp) =>
            {
                try
                {
                    // Convert feature and frame bitmaps to grayscale.
                    Stopwatch watch1 = Stopwatch.StartNew();
                    if(grayFrameImage == null)
                    {
                        grayFrameImage = new Image<Gray, byte>(bmp.PixelWidth, bmp.PixelHeight);
                    }
                    else if(grayFrameImage.Height != bmp.PixelHeight || grayFrameImage.Width != bmp.PixelWidth)
                    {
                        grayFrameImage.Dispose();
                        grayFrameImage = new Image<Gray, byte>(bmp.PixelWidth, bmp.PixelHeight);
                    }
                    if (frame == BitmapType.Color)
                    {
                        Util.GrayImageFromWriteableBitmap(bmp, ref grayFrameImage, PixelFormats.Bgr32);
                    }
                    else if (frame == BitmapType.Infrared)
                    {
                        Util.GrayImageFromWriteableBitmap(bmp, ref grayFrameImage, PixelFormats.Gray16);
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                    watch1.Stop();
                    Console.Write("Loading of images for GetFeatures took " + watch1.ElapsedMilliseconds + "ms.");

                    KeyPointsAndDescriptors keyPointsAndDescriptors = await GetKeyPointsAndDescriptorsAsync(detector, grayFrameImage, threshold, getDescriptors, useCuda);
                    //grayFrameImage.Dispose();
                    MKeyPoint[] keyPoints = keyPointsAndDescriptors.KeyPoints.ToArray();
                    TimeSpan time = ImageBuilder.GetFrameTime();
                    if (keyPoints.Length > 0)
                    {
                        // Get the rectangle of 3D coordinates surrounding each keypoint.
                        List<Coord3D[]> coords = new List<Coord3D[]>();
                        List<Rect> coordsMappings = new List<Rect>();
                        for (int i = 0; i < keyPoints.Length; i++)
                        {
                            coords.Add(ImageBuilder.GetCoord3DNow(new Rect(keyPoints[i].Point.X + rect.X, keyPoints[i].Point.Y + rect.Y, rect.Width, rect.Height), mapping));
                            coordsMappings.Add(rect);
                        }
                        callback(new Features(frame, mapping, keyPointsAndDescriptors.KeyPoints, coords.ToArray(), coordsMappings.ToArray(), time, keyPointsAndDescriptors.Descriptors));
                    }
                }
                catch (Exception e)
                {
                    Console.Write("Error in GetFeatures: " + e.Message);
                }
            };
            // Make sure ImageBuilder has the mapping for the Coord3D.
            ImageBuilder.GetBitmap(BitmapType.Coord3D, mapping, new Rect(0, 0, 1, 1), (b) => { });
            ImageBuilder.GetBitmapAsync(frame, mapping, new Rect(0, 0, 0, 0), cb);
        }
        protected async Task<KeyPointsAndDescriptors> GetKeyPointsAndDescriptorsAsync(FeatureDetector detectorType, Image<Gray, byte> image, double threshold, bool useDescriptors, bool useCuda)
        {
            return await Task.Run(() =>
            {
                return GetKeyPointsAndDescriptors(detectorType, image, threshold, useDescriptors, useCuda);
            });
        }
        protected KeyPointsAndDescriptors GetKeyPointsAndDescriptors(FeatureDetector detectorType, Image<Gray, byte> image, double threshold, bool useDescriptors = true, bool useCuda = true)
        {
            Stopwatch watch = Stopwatch.StartNew();
            using (VectorOfKeyPoint keyPoints = new VectorOfKeyPoint())
            using (Mat descriptors = new Mat())
            {
                switch (detectorType)
                {
                    case FeatureDetector.FAST:
                        if (CudaInvoke.HasCuda)
                        {
                            using (GpuMat<byte> gpuImage = new GpuMat<byte>(image))
                            //extract features from the object image
                            using (GpuMat gpuKeyPoints = new GpuMat())
                            using (CudaFastFeatureDetector fastCuda = new CudaFastFeatureDetector((int)threshold, true, FastDetector.DetectorType.Type9_16, 50))
                            //extract features from the object image
                            {
                                fastCuda.DetectRaw(gpuImage, keyPoints, null);
                                if (useDescriptors)
                                {
                                    fastCuda.Compute(gpuImage, keyPoints, descriptors);
                                }
                            }
                        }
                        else
                        {
                            using (FastDetector detector = new FastDetector((int)threshold, true, FastDetector.DetectorType.Type9_16))
                            {
                                detector.DetectRaw(image, keyPoints, null);
                                if (descriptors != null)
                                {
                                    detector.Compute(image, keyPoints, descriptors);
                                }
                            }
                        }
                        break;
                    case FeatureDetector.SURF:
                        if (CudaInvoke.HasCuda)
                        {
                            using (CudaSURF surfCuda = new CudaSURF((float)threshold, 4, 2, true, 0.01f, false))
                            using (GpuMat gpuImage = new GpuMat(image))
                            //extract features from the object image
                            using (GpuMat gpuKeyPoints = surfCuda.DetectKeyPointsRaw(gpuImage, null))
                            //using (GpuMat gpuSourceDescriptors = surfCuda.ComputeDescriptorsRaw(gpuSourceImage, null, gpuSourceKeyPoints))
                            {
                                surfCuda.DownloadKeypoints(gpuKeyPoints, keyPoints);
                                if (useDescriptors)
                                {
                                    using (GpuMat gpuMat = surfCuda.ComputeDescriptorsRaw(gpuImage, null, gpuKeyPoints))
                                    {
                                        gpuMat.Download(descriptors);
                                    }
                                }
                            }
                        }
                        else
                        {
                            SURF detector = new SURF(threshold);
                            detector.DetectRaw(image, keyPoints, null);
                            detector.DetectAndCompute(image, null, keyPoints, descriptors, true);
                        }
                        break;
                    default:
                        Debug.Assert(false, "GetKeyPointsAndDescriptors detectorType not found.");
                        break;
                }
                watch.Stop();
                Console.Write("GetKeyPointsAndDescriptors took " + watch.ElapsedMilliseconds + "ms to complete.");
                return new KeyPointsAndDescriptors(keyPoints, descriptors);
            }
        }
        // Builds frames consisting of depth, color and IR images. Note that the depth values are the distance from the plane of the camera
        // in meters.
        //
        // Initialization of DepthColorImageBuilder:
        // Call the Constructor with the frame sources you want.
        //
        // The DepthColorImageBuilder works like a fast food restaurant and work as follows:
        // 1. Call GetBitmap with the BitmapType (Your frame source), MappedTo (The mapping type), Rect (The region of the mapped frame to acquire) and a callback function, to place an order for those data.
        // 2. If the order is already in the list, the latter will be replaced by the new order, the task for the latter will be canceled.
        // 3. The GetBitmap function will wait for the frame to be updated.
        // (Note: The WriteableBitmap will not update until the next frame arrives.)
        // 4. Once the frames arrives, all the WriteableBitmap(s) will be updated and afterwards issue a FrameArrived Event. ImageBuilder will lock and unlock the frame to prevent tearing.
        // 5. The callback will be called once all frames are updated.
        // 6. Orders for the bitmaps are not persistent, so you will need to call GetBitmap again to get the next frame.
        //
        protected internal abstract class DepthColorImageBuilder<C>
        {
            protected enum OrderStatus
            {
                Success,
                Failed
            }
            protected class BufferMetrics
            {
                public int Width { get; private set; }
                public int Height { get; private set; }
                public int BytesPerPixel { get; private set; }
                public PixelFormat Format { get; private set; }

                public BufferMetrics(int width, int height, int bytesPerpixel, PixelFormat format)
                {
                    Width = width;
                    Height = height;
                    BytesPerPixel = bytesPerpixel;
                    Format = format;
                }
            }
            protected class Mapping<C>
            {
                public C [] mapping {get; set;}

                public Mapping ()
                {
                }
                public Mapping( C [] map)
                {
                    mapping = map;
                }
            }
            public delegate void DepthColorImageFrameHandler(object sender, EventArgs e);
            public event DepthColorImageFrameHandler FrameArrived;
            protected BitmapType[] BitmapTypes;
            protected ConcurrentDictionary<string, WriteableBitmap> MappedWholeBitmapFrames = new ConcurrentDictionary<string, WriteableBitmap>(); // The mapped bitmaps themselves (The whole thing). This saves having to initize and allocate on each frame.
            protected ConcurrentDictionary<string, Util.WriteableBitmapProxy> MappedWholeBitmapFrameProxies = new ConcurrentDictionary<string, Util.WriteableBitmapProxy>(); // Proxies for the mapped bitmaps themselves, these values can be accessed from other threads.
            protected ConcurrentDictionary<Tuple<BitmapType, MappedTo, Rect>, List<Action<WriteableBitmap>>> Orders = new ConcurrentDictionary<Tuple<BitmapType, MappedTo, Rect>, List<Action<WriteableBitmap>>>(); // The data to be processed for this frame. Only the regions specified here will be updated on each frame.
            protected ConcurrentDictionary<BitmapType, BufferMetrics> BitmapMetrics = new ConcurrentDictionary<BitmapType, BufferMetrics>(); // Properties of the mapped bitmaps to be used.
            //protected Dictionary<Tuple<BitmapType, MappedTo>, Mapping<C>> Mappings = new Dictionary<Tuple<BitmapType, MappedTo>, Mapping<C>>(); // The corresponding mapping from the raw frame to the mapped frame.
            public TimeSpan FrameTime = TimeSpan.Zero; //relative time of the frame.
            protected abstract BufferMetrics GetBufferMetrics(BitmapType bmp); // Get information regarding the buffers/frames.
            protected abstract BufferMetrics GetBufferMetrics(MappedTo mapping); // Get information regarding the buffers/frames.
            protected abstract void ProcessFrame(); //Once the frame arrives, all orders are process. Including updating the buffers/bitmaps and running the callbacks.
            protected abstract void InitializeSensor();
            protected abstract void CloseSensor();
            protected abstract bool IsSensorActive();
            public Coord3D[] GetCoord3DNow(Rect rect, MappedTo mapping)
            {
                //Please speed up this method!
                //Convert rect to PointF.
                var numberOfPoints = (int)rect.Width * (int)rect.Height;
                var upperLeft = rect.TopLeft;
                System.Drawing.PointF[] points = new System.Drawing.PointF[numberOfPoints];
                for (int i = 0; i < numberOfPoints; i++)
                {
                    points[i].X = (int)rect.TopLeft.X + i % (int)rect.Width;
                    points[i].Y = (int)rect.TopLeft.Y + (int)Math.Floor(i / rect.Width);
                }
                return GetCoord3DNow(points, mapping);
            }
            public abstract Coord3D[] GetCoord3DNow(System.Drawing.PointF[] points, MappedTo mapping);

            // Is the frame processing while ProcessFrame is called?
            protected bool FrameProcessing = false;
            public DepthColorImageBuilder()
            {
            }
            protected void FinishedProcessingFrame()
            {
                FrameArrived(this, null);
            }
            public TimeSpan GetFrameTime() 
            {
                return FrameTime;
            }
            // Clips the inputRect to be within clippingRegion. If inputRect is size zero, then the clipping Region is returned.
            protected Rect ClipRect(Rect inputRect, Rect clippingRegion)
            {
                Rect outRect = inputRect;
                if (inputRect.Width <= 0 || inputRect.Height <= 0)
                {
                    outRect = clippingRegion;
                }
                outRect.Intersect(clippingRegion);
                return outRect;
            }
            // Orders for the specific regions for the bitmap source and mapping. Initializes the specific bitmap if not already done so. 
            // Waits for the frame update to complete, then returns the region on the WriteableBitmap is successful. If unsuccessful, returns null.
            public void GetBitmapAsync(BitmapType bmp, MappedTo mapping, Rect rect, Action<Util.WriteableBitmapProxy> callback)
            {
                GetBitmap(bmp, mapping, rect, new Action<WriteableBitmap>((b) =>
                {
                    //b.Lock();
                    Util.WriteableBitmapProxy proxy = new Util.WriteableBitmapProxy(b);
                    var task = new Task(() => { callback(proxy); });
                    //task.ContinueWith((r) => { Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => { b.Unlock(); })); });
                    task.Start();
                }));
            }
            public void GetBitmap(BitmapType bmp, MappedTo mapping, Rect rect, Action<WriteableBitmap> callback)
            {
                WriteableBitmap result = null;
                if (!IsSensorActive())
                {
                    InitializeSensor();
                }
                result = InitializeMappedWholeBitmap(bmp, mapping);
                if (result != null)
                {
                    Rect wholeRect = new Rect(0, 0, result.PixelWidth, result.PixelHeight);
                    Rect clippedRect = ClipRect(rect, wholeRect);
                    Tuple<BitmapType, MappedTo, Rect> key = new Tuple<BitmapType, MappedTo, Rect>(bmp, mapping, clippedRect);
                    if (Orders.ContainsKey(key))
                    {
                        //Chaining callbacks when order already exists.
                        Orders[key].Add(callback);
                    }
                    else
                    {
                        Orders.TryAdd(key, new List<Action<WriteableBitmap>>());
                        Orders[key].Add(callback);
                    }
                }
            }
            // Initialize bitmaps for editing. Please run on UI thread.
            protected WriteableBitmap InitializeMappedWholeBitmap(BitmapType bmpType, MappedTo mapping)
            {
                WriteableBitmap result = null;
                try
                {
                    if (MappedWholeBitmapFrames.TryGetValue(GetKey(bmpType, mapping), out result))
                    {
                        return result;
                    }
                    else
                    {
                        BufferMetrics met1 = GetBufferMetrics(bmpType);
                        BufferMetrics met2 = null;
                        switch (mapping)
                        {
                            case MappedTo.ColorCoord:
                                met2 = GetBufferMetrics(BitmapType.Color);
                                break;
                            case MappedTo.DepthCoord:
                                met2 = GetBufferMetrics(BitmapType.Depth);
                                break;
                            case MappedTo.InfraredCoord:
                                met2 = GetBufferMetrics(BitmapType.Infrared);
                                break;
                            default:
                                Debug.Assert(false, "InitializePremappedBitmap - unknown mapping type: " + mapping.ToString());
                                break;
                        }
                        result = new WriteableBitmap(met2.Width, met2.Height, 300, 300, met1.Format, null);
                        if (result != null)
                        {
                            MappedWholeBitmapFrames.TryAdd(GetKey(bmpType, mapping), result);
                            MappedWholeBitmapFrameProxies.TryAdd(GetKey(bmpType, mapping), new Util.WriteableBitmapProxy(result));
                        }
                    }
                }
                catch
                {
                    result = null;
                    Debug.WriteLine("Error Initializing bitmap for the specific source.");
                }
                return result;
            }
            // An exception when there is a problem acquiring a frame for the capture device.
            public class FrameNotAcquiredError : Exception 
            {
                public FrameNotAcquiredError(string message) : base(message) {}
            }
        }

        protected internal class KinectDepthColorImageBuilder : DepthColorImageBuilder<Coord3D>
        {
            // References to the Kinect sensor and its accompanying readers.
            KinectSensor Sensor;
            MultiSourceFrameReader FrameReader;
            MultiSourceFrame CurrentMultisourceFrame;
            private Mapping<Coord3D> Co2D, D2Co, Ca2Co, Ca2D;
            private bool Co2DUpd, D2CoUpd, Ca2CoUpd, Ca2DUpd;
            CameraSpacePoint[] camToDepthCache = null;
            CameraSpacePoint[] CamToColCache = null;
            ColorSpacePoint[] ColToDepthCache = null;
            UInt32[] UColToDepthCache = null;

            int DWidth = 512;
            int DHeight = 424;
            int CHeight = 1080;
            int CWidth = 1920;
            // Initialize the Kinect sensors to send the specified frames.
            public KinectDepthColorImageBuilder(BitmapType[] bmps) 
            {
                BitmapTypes = bmps;
                InitializeSensor();
            }
            protected override bool IsSensorActive()
            {
                return KinectSensor.GetDefault().IsOpen;
            }
            protected override void InitializeSensor()
            {
                FrameSourceTypes kinectSources = 0;
                foreach (BitmapType bmp in BitmapTypes)
                {
                    if (bmp == BitmapType.Color)
                    {
                        kinectSources = kinectSources | FrameSourceTypes.Color;
                    }
                    if (bmp == BitmapType.Depth || bmp == BitmapType.Coord3D || bmp == BitmapType.DepthBGRA)
                    {
                        kinectSources = kinectSources | FrameSourceTypes.Depth;
                    }
                    if (bmp == BitmapType.Infrared)
                    {
                        kinectSources = kinectSources | FrameSourceTypes.Infrared;
                    }
                }
                Sensor = KinectSensor.GetDefault();
                FrameReader = Sensor.OpenMultiSourceFrameReader(kinectSources);
                FrameReader.MultiSourceFrameArrived += frameReader_MultiSourceFrameArrived;
                Sensor.Open();
            }
            protected override void CloseSensor()
            {
                FrameReader.MultiSourceFrameArrived -= frameReader_MultiSourceFrameArrived;
                Sensor.Close();
            }
            protected override BufferMetrics GetBufferMetrics(BitmapType bmp)
            {
                MultiSourceFrame mframe = FrameReader.AcquireLatestFrame();
                BufferMetrics result = null;
                if (!BitmapMetrics.TryGetValue(bmp, out result))
                {
                    switch (bmp)
                    {
                        case BitmapType.Color:
                          //  using (ColorFrame cframe = mframe.ColorFrameReference.AcquireFrame())
                          //  {
                          //      if (cframe != null)
                          //      {
                          //          FrameDescription description = cframe.CreateFrameDescription(ColorImageFormat.Bgra);
                          //          result = new BufferMetrics(description.Width, description.Height, (int)description.BytesPerPixel, PixelFormats.Bgr32);
                          //      }
                          //      else
                          //      {
                                    // If one cannot acquire a frame then we use default settings.
                                    result = new BufferMetrics(CWidth, CHeight, PixelFormats.Bgr32.BitsPerPixel / 8, PixelFormats.Bgr32);
                          //      }
                          //  }
                            break;
                        case BitmapType.Depth:
                          //  using (DepthFrame dframe = mframe.DepthFrameReference.AcquireFrame())
                          //  {
                          //      if (dframe != null)
                          //      {
                          //          FrameDescription description = dframe.FrameDescription;
                          //          result = new BufferMetrics(description.Width, description.Height, (int)description.BytesPerPixel, PixelFormats.Gray16); // We change bytes per pixel to 4.
                          //      }
                          //      else
                          //      {
                                    // If one cannot acquire a frame then we use default settings.
                                    result = new BufferMetrics(DWidth, DHeight, PixelFormats.Gray16.BitsPerPixel / 8, PixelFormats.Gray16);
                          //      }
                          //  }
                            break;
                        case BitmapType.DepthBGRA:
                          //  using (DepthFrame dframe = mframe.DepthFrameReference.AcquireFrame())
                          //  {
                          //      if (dframe != null)
                          //      {
                          //          FrameDescription description = dframe.FrameDescription;
                          //          result = new BufferMetrics(description.Width, description.Height, PixelFormats.Bgr32.BitsPerPixel / 8, PixelFormats.Bgr32); // We change bytes per pixel to 4.
                          //      }
                          //      else
                          //      {
                                    // If one cannot acquire a frame then we use default settings.
                                    result = new BufferMetrics(DWidth, DHeight, PixelFormats.Bgr32.BitsPerPixel / 8, PixelFormats.Bgr32);
                          //      }
                          //  }
                            break;
                        case BitmapType.Infrared:
                          //  using (InfraredFrame iframe = mframe.InfraredFrameReference.AcquireFrame())
                          //  {
                          //     if (iframe != null)
                          //      {
                          //          FrameDescription description = iframe.FrameDescription;
                          //          result = new BufferMetrics(description.Width, description.Height, (int)description.BytesPerPixel, PixelFormats.Gray16);
                          //      }
                          //      else
                          //      {
                                    // If one cannot acquire a frame then we use default settings.
                                    result = new BufferMetrics(DWidth, DHeight, PixelFormats.Gray16.BitsPerPixel / 8, PixelFormats.Gray16);
                          //      }
                          //  }
                            break;
                        case BitmapType.Coord3D:
                          //  using (DepthFrame dframe = mframe.DepthFrameReference.AcquireFrame())
                          //  {
                          //      if (dframe != null)
                          //      {
                          //          FrameDescription description = dframe.FrameDescription;
                          //          result = new BufferMetrics(description.Width, description.Height, PixelFormats.Rgb128Float.BitsPerPixel / 8 /* float type times three + 1 float for filling. */, PixelFormats.Rgb128Float);
                          //      }
                          //      else
                          //      {
                                    // If one cannot acquire a frame then we use default settings.
                            result = new BufferMetrics(DWidth, DHeight, PixelFormats.Prgba128Float.BitsPerPixel / 8, PixelFormats.Prgba128Float);
                          //      }
                          //  }
                            break;
                        default:
                            Debug.Assert(false, "GetBufferMetrics for " + bmp.ToString() + "does not exist.");
                            break;
                    }
                    if (result == null)
                    {
                        throw new FrameNotAcquiredError("Cannot get metrics for buffer " + bmp.ToString());
                    }
                    else
                    {
                        BitmapMetrics.TryAdd(bmp, result);
                    }
                }
                return result;
            }
            protected override BufferMetrics GetBufferMetrics(MappedTo mapping)
            {
                BufferMetrics result = null;
                switch (mapping)
                {
                    case MappedTo.ColorCoord:
                        result = GetBufferMetrics(BitmapType.Color);
                        break;
                    case MappedTo.DepthCoord:
                        result = GetBufferMetrics(BitmapType.Depth);
                        break;
                    case MappedTo.InfraredCoord:
                        result = GetBufferMetrics(BitmapType.Depth);
                        break;
                    default:
                        Debug.Assert(false, "GetBufferMetrics: Invalid mapping.");
                        break;
                }
                return result;
            }
            private Mapping<Coord3D> GetMapping(BitmapType bmp, MappedTo mapping)
            {
                switch (bmp)
                {
                    case BitmapType.Color:
                        if (mapping == MappedTo.InfraredCoord || mapping == MappedTo.DepthCoord)
                        {
                            return Co2D;
                        }
                        else
                        {
                            return null;
                        }
                        break;
                    case BitmapType.Coord3D:
                        if (mapping == MappedTo.ColorCoord)
                        {
                            return Ca2Co;
                        }
                        else if (mapping == MappedTo.DepthCoord || mapping == MappedTo.InfraredCoord)
                        {
                            return Ca2D;
                        }
                        else
                        {
                            return null;
                        }
                        break;
                    case BitmapType.Depth:
                        if (mapping == MappedTo.ColorCoord)
                        {
                            return D2Co;
                        }
                        else 
                        {
                            return null;
                        }
                        break;
                    case BitmapType.DepthBGRA:
                        if (mapping == MappedTo.ColorCoord)
                        {
                            return D2Co;
                        }
                        else 
                        {
                            return null;
                        }
                        break;
                    case BitmapType.Infrared:
                        if (mapping == MappedTo.ColorCoord)
                        {
                            return D2Co;
                        }
                        else 
                        {
                            return null;
                        }
                        break;
                    default:
                        Debug.Assert(false, bmp.ToString() + " cannot be processed by GetMapping()");
                        break;
                }
                return null;
            }
            //Initializes mappings for each frame if neccessary.
            protected void InitializeMappings() //Initializes mappings for each frame if neccessary.
            {
                Co2DUpd = false;
                D2CoUpd = false;
                Ca2CoUpd = false;
                Ca2DUpd = false;
                foreach (var order in Orders)
                {
                    switch (order.Key.Item1)
                    {
                        case BitmapType.Color:
                            if (order.Key.Item2 == MappedTo.InfraredCoord || order.Key.Item2 == MappedTo.DepthCoord)
                            {
                                if (Co2DUpd == false)
                                {
                                    if (Co2D == null)
                                    {
                                        Co2D = new Mapping<Coord3D>(new Coord3D[DWidth * DHeight]);
                                    }
                                    GenerateColorToDepthMap(Co2D.mapping);
                                    Co2DUpd = true;
                                }
                            }
                            break;
                        case BitmapType.Depth:
                            if (order.Key.Item2 == MappedTo.ColorCoord)
                            {
                                if (D2CoUpd == false)
                                {
                                    if (D2Co == null)
                                    {
                                        D2Co = new Mapping<Coord3D>(new Coord3D[CWidth * CHeight]);
                                    }
                                    GenerateDepthToColorMap(D2Co.mapping);
                                    D2CoUpd = true;
                                }
                            }
                            break;
                        case BitmapType.DepthBGRA:
                            if (order.Key.Item2 == MappedTo.ColorCoord)
                            {
                                if (Co2DUpd == false)
                                {
                                    if (Co2D == null)
                                    {
                                        Co2D = new Mapping<Coord3D>(new Coord3D[DWidth * DHeight]);
                                    }
                                    GenerateDepthToColorMap(Co2D.mapping);
                                    Co2DUpd = true;
                                }
                            }
                            break;
                        case BitmapType.Infrared:
                            if (order.Key.Item2 == MappedTo.ColorCoord)
                            {
                                if (D2CoUpd == false)
                                {
                                    if (D2Co == null)
                                    {
                                        D2Co = new Mapping<Coord3D>(new Coord3D[CWidth * CHeight]);
                                    }
                                    GenerateDepthToColorMap(D2Co.mapping);
                                    D2CoUpd = true;
                                }
                            }
                            break;
                        case BitmapType.Coord3D:
                            if (order.Key.Item2 == MappedTo.ColorCoord)
                            {
                                if (Ca2CoUpd == false)
                                {
                                    if (Ca2Co == null)
                                    {
                                        Ca2Co = new Mapping<Coord3D>(new Coord3D[CWidth * CHeight]);
                                    }
                                    GenerateCameraToColorMap(Ca2Co.mapping);
                                    Ca2CoUpd = true;
                                }
                            }
                            else if (order.Key.Item2 == MappedTo.DepthCoord || order.Key.Item2 == MappedTo.InfraredCoord)
                            {
                                if (Ca2DUpd == false)
                                {
                                    if (Ca2D == null)
                                    {
                                        Ca2D = new Mapping<Coord3D>(new Coord3D[DWidth * DHeight]);
                                    }
                                    GenerateCameraToDepthMap(Ca2D.mapping);
                                    Ca2DUpd = true;
                                }
                            }
                            break;
                        default:
                            Debug.Assert(false, order.Key.Item1 + " cannot be processed by InitializeMappings()");
                            break;
                    }
                }
            }
            private bool GenerateColorToDepthMap(Coord3D[] map) 
            {
                using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                {
                    if (dframe != null)
                    {

                        if (ColToDepthCache == null)
                            ColToDepthCache = new ColorSpacePoint[DWidth * DHeight];
                        Debug.Assert(map.Length == map.Length, "Error in GenerateColorToDepthMap: map length does not equal to temporary map lenght.");
                        using (KinectBuffer depthBufferData = dframe.LockImageBuffer())
                        {
                            Sensor.CoordinateMapper.MapDepthFrameToColorSpaceUsingIntPtr(depthBufferData.UnderlyingBuffer, depthBufferData.Size, ColToDepthCache);
                        }
                        //Parallel.For(0, map.Length, i =>
                        for(int i = 0; i < map.Length; i++)
                        {
                            map[i].X = ColToDepthCache[i].X;
                            map[i].Y = ColToDepthCache[i].Y;
                        }//);
                        return true;
                    }
                    else
                    {
                        return false;
                        //throw new FrameNotAcquiredError("Depth frame lost.");
                    }
                }
            }
            private bool GenerateDepthToColorMap(Coord3D[] map)
            {
                using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                {
                    if (dframe != null)
                    {

                        DepthSpacePoint[] tempMap = new DepthSpacePoint[CWidth * CHeight];
                        using (KinectBuffer depthBufferData = dframe.LockImageBuffer())
                        {
                            Sensor.CoordinateMapper.MapColorFrameToDepthSpaceUsingIntPtr(depthBufferData.UnderlyingBuffer, depthBufferData.Size, tempMap);
                        }
                        //Parallel.For(0, map.Length, i =>
                        for(int i = 0; i < map.Length; i++)
                            {
                                map[i].X = tempMap[i].X;
                                map[i].Y = tempMap[i].Y;
                            }//);
                        return true;
                    }
                    else 
                    {
                        return false;
                        //throw new FrameNotAcquiredError("Depth frame lost.");
                    }
                }
            }
            private bool GenerateCameraToColorMap(Coord3D[] map)
            {
                using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                {
                    if (dframe != null)
                    {

                        if (CamToColCache == null)
                            CamToColCache = new CameraSpacePoint[CWidth * CHeight];
                        using (KinectBuffer depthBufferData = dframe.LockImageBuffer())
                        {
                            Sensor.CoordinateMapper.MapColorFrameToCameraSpaceUsingIntPtr(depthBufferData.UnderlyingBuffer, depthBufferData.Size, CamToColCache);
                        }
                        //Parallel.For(0, map.Length, i =>
                        for(int i = 0; i < map.Length; i++)
                        {
                            map[i].X = CamToColCache[i].X;
                            map[i].Y = CamToColCache[i].Y;
                            map[i].Z = CamToColCache[i].Z;
                        }//);
                        return true;
                    }
                    else
                    {
                        return false;
                        //throw new FrameNotAcquiredError("Depth frame lost.");
                    }
                }
            }
            private bool GenerateCameraToDepthMap(Coord3D[] map)
            {
                using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                {
                    if (dframe != null)
                    {

                        if( camToDepthCache == null)
                            camToDepthCache = new CameraSpacePoint[DWidth * DHeight];
                        using (KinectBuffer depthBufferData = dframe.LockImageBuffer())
                        {
                            Sensor.CoordinateMapper.MapDepthFrameToCameraSpaceUsingIntPtr(depthBufferData.UnderlyingBuffer, depthBufferData.Size, camToDepthCache);
                        }
                        //Parallel.For(0, map.Length, i =>
                        for(int i = 0; i < map.Length; i++)
                        {
                            map[i].X = camToDepthCache[i].X;
                            map[i].Y = camToDepthCache[i].Y;
                            map[i].Z = camToDepthCache[i].Z;
                        }//);
                        return true;
                    }
                    else
                    {
                        return false;
                        //throw new FrameNotAcquiredError("Depth frame lost.");
                    }
                }
            }

            // Write mapped bitmaps with data from the frame. Assumes the bitmap is lock and ready to be written and orders are proper (and not updating out of bound regions).
            // Note: This method is to be exeuted on the UI thread.
            protected void WriteBitmaps() 
            {
                // Lock all MappedWholeBitmapFrames first.
                foreach (var bmps in  MappedWholeBitmapFrames)
                {
                    bmps.Value.Lock();
                }
                
                // This is a list of FailedOrders, which is a collection of orders that have failed to execute.
               ConcurrentDictionary<Tuple<BitmapType, MappedTo, Rect>, OrderStatus> orderStatus = new ConcurrentDictionary<Tuple<BitmapType, MappedTo, Rect>, OrderStatus>();
               List<Task> tasks = new List<Task>();
               Task task = null;
               foreach (var order in Orders)
               {
                   var bmp = order.Key.Item1;
                   var mapping = order.Key.Item2;
                   var rect = order.Key.Item3;
                   Debug.Assert(MappedWholeBitmapFrames.ContainsKey(GetKey(bmp, mapping)), "MappedWholeBitmapFrame not available during frame updating.");
                   switch (bmp)
                   {
                       case BitmapType.Color:
                           task = new Task(() => { if (ColorToBitmap(mapping, rect)) { orderStatus.TryAdd(order.Key, OrderStatus.Success); } else { orderStatus.TryAdd(order.Key, OrderStatus.Failed); }});
                           task.ContinueWith(t => { orderStatus.TryAdd(order.Key, OrderStatus.Success); }, TaskContinuationOptions.OnlyOnRanToCompletion);
                           task.ContinueWith(t => { orderStatus.TryAdd(order.Key, OrderStatus.Failed); }, TaskContinuationOptions.OnlyOnFaulted);
                           break;
                       case BitmapType.Depth:
                           task = new Task(() => { if (DepthToBitmap(mapping, rect)) { orderStatus.TryAdd(order.Key, OrderStatus.Success); } else { orderStatus.TryAdd(order.Key, OrderStatus.Failed);}});
                           task.ContinueWith(t => { orderStatus.TryAdd(order.Key, OrderStatus.Failed); }, TaskContinuationOptions.OnlyOnFaulted);
                           break;
                       case BitmapType.DepthBGRA:
                           task = new Task(() => { if (DepthBGRAToBitmap(mapping, rect)) { orderStatus.TryAdd(order.Key, OrderStatus.Success); } else { orderStatus.TryAdd(order.Key, OrderStatus.Failed); } });
                           task.ContinueWith(t => { orderStatus.TryAdd(order.Key, OrderStatus.Failed); }, TaskContinuationOptions.OnlyOnFaulted);
                           break;
                       case BitmapType.Infrared:
                           task = new Task(() => { if (IRToBitmap(mapping, rect)) { orderStatus.TryAdd(order.Key, OrderStatus.Success); } else { orderStatus.TryAdd(order.Key, OrderStatus.Failed); } });
                           task.ContinueWith(t => { orderStatus.TryAdd(order.Key, OrderStatus.Failed); }, TaskContinuationOptions.OnlyOnFaulted);
                           break;
                       case BitmapType.Coord3D:
                           task = new Task(() => { if (Coord3DToBitmap(mapping, rect)) { orderStatus.TryAdd(order.Key, OrderStatus.Success); } else { orderStatus.TryAdd(order.Key, OrderStatus.Failed); } });
                           task.ContinueWith(t => { orderStatus.TryAdd(order.Key, OrderStatus.Failed); }, TaskContinuationOptions.OnlyOnFaulted);
                           break;
                   }
                   tasks.Add(task);
               }
               Parallel.ForEach(tasks, t => t.Start());
               try
               {
                   Task.WaitAll(tasks.ToArray());
               }
               catch (AggregateException ae)
               {
                   foreach (var e in ae.InnerExceptions)
                   {
                       // Handle the custom exception. 
                       if (e is FrameNotAcquiredError)
                       {
                           Console.WriteLine(e.Message);
                       }
                       // Rethrow any other exception. 
                       else
                       {
                           throw;
                       }
                   }
               }
                // AddDirty Rect for all successful orders.
               foreach (var order in Orders)
               {
                   OrderStatus status = OrderStatus.Failed;
                   orderStatus.TryGetValue(order.Key, out status);
                   if (status == OrderStatus.Success)
                   {
                       var bmp = order.Key.Item1;
                       var mapping = order.Key.Item2;
                       var rect = order.Key.Item3;
                       MappedWholeBitmapFrames[GetKey(bmp, mapping)].AddDirtyRect(new Int32Rect((int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height));
                   }
               }
                // Unlock all MappedWholeBitmapFrames.
               foreach (var bmps in MappedWholeBitmapFrames)
               {
                   bmps.Value.Unlock();
               }
                // Prepare bitmaps for execution of the callback function. 
                foreach (var order in Orders)
               {
                   var bmp = order.Key.Item1;
                   var mapping = order.Key.Item2;
                   var rect = order.Key.Item3;
                   // all actions will be executed on the UI frame so that the frames can be accessed.
                   OrderStatus status = OrderStatus.Failed;
                   orderStatus.TryGetValue(order.Key, out status);
                   if (status == OrderStatus.Success)
                   {
                       WriteableBitmap bmpFrame = MappedWholeBitmapFrames[GetKey(bmp, mapping)];
                       if (new Rect(0, 0, bmpFrame.PixelWidth, bmpFrame.PixelHeight).Equals(rect))
                       {
                           foreach (var action in order.Value)
                           {
                               action(bmpFrame);
                           }
                       }
                       else
                       {
                           // Generate a smaller WriteableBitmap if only a subset of the frame is needed.
                           WriteableBitmap output = new WriteableBitmap((int)rect.Width, (int)rect.Height, 300, 300, bmpFrame.Format, null);
                           output.Blit(new Rect(0, 0, rect.Width, rect.Height), bmpFrame, rect);
                           foreach (var action in order.Value)
                           {
                               action(output);
                           }
                       }
                   }
               }
            }
            // Writes mapped frame pixels to specific MappedBitmap. Assumes the MappedBitmaps are locked to expose the backbuffer for writing.
            private bool ColorToBitmap(MappedTo mapping, Rect rect)
            {
                Util.WriteableBitmapProxy result = null;
                if (!MappedWholeBitmapFrameProxies.TryGetValue(GetKey(BitmapType.Color, mapping), out result))
                {
                    Debug.Assert(false, "MappedWholeBitmapFrame for type: BitmapType.Color and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                    throw new FrameNotAcquiredError("MappedWholeBitmapFrame for type: BitmapType.Color and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                }
                else
                {
                    if (mapping == MappedTo.ColorCoord)
                    {
                        using (ColorFrame cframe = CurrentMultisourceFrame.ColorFrameReference.AcquireFrame())
                        {
                            if (cframe != null && result != null)
                            {
                                using (KinectBuffer colorBuffer = cframe.LockRawImageBuffer())
                                {
                                    FrameDescription description = cframe.CreateFrameDescription(ColorImageFormat.Bgra);
                                    if (cframe.RawColorImageFormat == ColorImageFormat.Bgra)
                                    {
                                        cframe.CopyRawFrameDataToIntPtr(result.BackBuffer, description.LengthInPixels * description.BytesPerPixel);
                                    }
                                    else
                                    {
                                        // Use an openCV implementation.
                                        Util.YUV422ToBGRA(colorBuffer.UnderlyingBuffer, result.BackBuffer, result.PixelWidth, result.PixelHeight);
                                        //cframe.CopyConvertedFrameDataToIntPtr(result.BackBuffer, description.LengthInPixels * description.BytesPerPixel, ColorImageFormat.Bgra);
                                    }
                                    //cframe.Dispose();
                                }
                                return true;
                            }
                            else
                            {
                                return false;
                                //Console.Write("Color frame lost.");
                                //throw new FrameNotAcquiredError("Color frame lost.");
                            }
                        }
                    }
                    else if (mapping == MappedTo.DepthCoord || mapping == MappedTo.InfraredCoord)
                    {
                        using (ColorFrame cframe = CurrentMultisourceFrame.ColorFrameReference.AcquireFrame())
                        {
                            if (cframe != null && result != null)
                            {
                                using (KinectBuffer colorBuffer = cframe.LockRawImageBuffer())
                                {
                                    FrameDescription description = cframe.CreateFrameDescription(ColorImageFormat.Bgra);
                                    Mapping<Coord3D> map = GetMapping(BitmapType.Color, mapping);
                                    var width = result.PixelWidth;
                                    var height = result.PixelHeight;
                                    var strideInBytes = width * result.BackBufferStride; //stride in number of 16bit pixels.
                                    Debug.Assert(width > 0 && height > 0 && strideInBytes > 0, "Assertion error - IRToBitmap: frame parameters width, height and stride must be more than 0.");
                                    if(UColToDepthCache == null) 
                                        UColToDepthCache = new UInt32[description.Width * description.Height];
                                    UInt32 tempBufferWidth = (UInt32)description.Width;
                                    unsafe
                                    {
                                        UInt32* resultDataPtr = (UInt32*)result.BackBuffer;
                                        fixed (UInt32* pArray = UColToDepthCache)
                                        {
                                            IntPtr tempBufferPtr = new IntPtr((void*)pArray);
                                            //Fill tempBuffer.
                                            if (cframe.RawColorImageFormat == ColorImageFormat.Bgra)
                                            {
                                                cframe.CopyRawFrameDataToIntPtr(tempBufferPtr, (uint)ColToDepthCache.Length * 4);
                                            }
                                            else
                                            {
                                                // Use an openCV implementation.
                                                Util.YUV422ToBGRA(colorBuffer.UnderlyingBuffer, tempBufferPtr, description.Width, description.Height);
                                                //cframe.CopyConvertedFrameDataToIntPtr(tempBufferPtr, (uint)tempBuffer.Length * 4, ColorImageFormat.Bgra);
                                            }
                                        }
                                        //cframe.Dispose();
                                        // Do the mapping.
                                        //Parallel.For(0, Convert.ToInt64(result.PixelHeight * result.PixelWidth), (i) =>
                                        for(int i = 0; i < Convert.ToInt64(result.PixelHeight * result.PixelWidth); i++)
                                        {
                                            if (map.mapping[i].Y >= 0 && map.mapping[i].Y < description.Height && map.mapping[i].X >= 0 && map.mapping[i].X < description.Width)
                                            {
                                                resultDataPtr[i] = UColToDepthCache[(int)(Math.Floor(map.mapping[i].Y) * tempBufferWidth + Math.Floor(map.mapping[i].X))];
                                            }
                                            else
                                            {
                                                resultDataPtr[i] = 0xFFFF0000;
                                            }
                                        }//);
                                    }
                                }
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            private bool DepthToBitmap(MappedTo mapping, Rect rect)
            {
                Util.WriteableBitmapProxy result = null;
                if (!MappedWholeBitmapFrameProxies.TryGetValue(GetKey(BitmapType.Depth, mapping), out result))
                {
                    Debug.Assert(false, "MappedWholeBitmapFrame for type: BitmapType.Depth and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                    throw new FrameNotAcquiredError("MappedWholeBitmapFrame for type: BitmapType.Depth and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                }
                else
                {
                    if (mapping == MappedTo.DepthCoord || mapping == MappedTo.InfraredCoord)
                    {
                        using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                        {
                            if (dframe != null && result != null)
                            {
                                using (KinectBuffer depthBuffer = dframe.LockImageBuffer())
                                {
                                    FrameDescription description = dframe.FrameDescription;
                                    dframe.CopyFrameDataToIntPtr(result.BackBuffer, (uint)(result.PixelHeight * result.BackBufferStride));
                                    //dframe.Dispose();
                                } 
                                return true;

                            }
                            else
                            {
                                return false;
                                //Console.Write("Depth frame lost.");
                                //throw new FrameNotAcquiredError("Depth frame lost.");
                            }
                        }
                    }
                    else if (mapping == MappedTo.ColorCoord)
                    {
                        using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                        {
                            if (dframe != null && result != null)
                            {
                                FrameDescription description = dframe.FrameDescription;
                                Mapping<Coord3D> map = GetMapping(BitmapType.Depth, mapping);
                                var width = result.PixelWidth;
                                var height = result.PixelHeight;
                                Debug.Assert(width > 0 && height > 0, "Assertion error - DepthToBitmap: frame parameters width and height must be more than 0.");
                                UInt32 tempBufferWidth = (UInt32) description.Width;
                                using (KinectBuffer depthBuffer = dframe.LockImageBuffer())
                                {
                                    unsafe
                                    {
                                        UInt16* depthBufferDataPtr = (UInt16*)depthBuffer.UnderlyingBuffer;
                                        UInt16* resultDataPtr = (UInt16*)result.BackBuffer;
                                        // Do the mapping.
                                        //Parallel.For(0, Convert.ToInt64(result.PixelHeight * result.PixelWidth), (i) =>
                                        for(int i = 0; i < Convert.ToInt64(result.PixelHeight * result.PixelWidth); i++)
                                        {
                                            if (map.mapping[i].Y >= 0 && map.mapping[i].Y < description.Height && map.mapping[i].X >= 0 && map.mapping[i].X < description.Width)
                                            {
                                                resultDataPtr[i] = depthBufferDataPtr[(int)(Math.Floor(map.mapping[i].Y) * tempBufferWidth + Math.Floor(map.mapping[i].X))];
                                            }
                                            else
                                            {
                                                resultDataPtr[i] = 0x0000;
                                            }
                                        }//);
                                    }
                                }
                                //dframe.Dispose();
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            private bool DepthBGRAToBitmap(MappedTo mapping, Rect rect)
            {
                Util.WriteableBitmapProxy result = null;
                if (!MappedWholeBitmapFrameProxies.TryGetValue(GetKey(BitmapType.DepthBGRA, mapping), out result))
                {
                    Debug.Assert(false, "MappedWholeBitmapFrame for type: BitmapType.DepthBGRA and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                    throw new FrameNotAcquiredError("MappedWholeBitmapFrame for type: BitmapType.DepthBGRA and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                }
                else
                {
                    if (mapping == MappedTo.DepthCoord || mapping == MappedTo.InfraredCoord)
                    {
                        using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                        {
                            if (dframe != null && result != null)
                            {
                                FrameDescription description = dframe.FrameDescription;
                                var width = result.PixelWidth;
                                var height = result.PixelHeight;
                                Debug.Assert(width > 0 && height > 0, "Assertion error - DepthBGRAToBitmap: frame parameters width and  height must be more than 0.");
                                using (KinectBuffer depthBuffer = dframe.LockImageBuffer())
                                {
                                    unsafe
                                    {
                                        UInt16* depthBufferDataPtr = (UInt16*)depthBuffer.UnderlyingBuffer;
                                        Byte* resultDataPtr = (Byte*)result.BackBuffer;
                                        // Do the mapping.
                                        //Parallel.For(0, Convert.ToInt64(result.PixelHeight * result.PixelWidth), (i) =>
                                        for (int i = 0; i < Convert.ToInt64(result.PixelHeight * result.PixelWidth); i++)
                                        {
                                            UInt16 depth = depthBufferDataPtr[i];
                                            if (depth > 0)
                                            {
                                                Byte intensity = (Byte)(depth & 0x00FF);
                                                resultDataPtr[4 * i] = intensity;
                                                resultDataPtr[4 * i + 1] = intensity;
                                                resultDataPtr[4 * i + 2] = intensity;
                                                resultDataPtr[4 * i + 3] = 0xFF;
                                            }
                                            else
                                            {
                                                resultDataPtr[4 * i] = 0x00;
                                                resultDataPtr[4 * i + 1] = 0x00;
                                                resultDataPtr[4 * i + 2] = 0xFF;
                                                resultDataPtr[4 * i + 3] = 0xFF;
                                            }
                                        }//);
                                    }
                                }
                                return true;
                                //dframe.Dispose();
                            }
                            else
                            {
                                return false;
                                //Console.Write("Depth frame lost.");
                                //throw new FrameNotAcquiredError("Depth frame lost.");
                            }
                        }
                    }
                    else if (mapping == MappedTo.ColorCoord)
                    {
                        using (DepthFrame dframe = CurrentMultisourceFrame.DepthFrameReference.AcquireFrame())
                        {
                            if (dframe != null && result != null)
                            {
                                FrameDescription description = dframe.FrameDescription;
                                Mapping<Coord3D> map = GetMapping(BitmapType.DepthBGRA, mapping);
                                var width = result.PixelWidth;
                                var height = result.PixelHeight;
                                Int32 tempBufferWidth = description.Width;
                                Debug.Assert(width > 0 && height > 0, "Assertion error - DepthBGRAToBitmap: frame parameters width and  height must be more than 0.");
                                using (KinectBuffer depthBuffer = dframe.LockImageBuffer())
                                {
                                    unsafe
                                    {
                                        Int16* depthBufferDataPtr = (Int16*)depthBuffer.UnderlyingBuffer;
                                        Byte* resultDataPtr = (Byte*)result.BackBuffer;
                                        // Do the mapping.
                                        //Parallel.For(0, Convert.ToInt64(result.PixelHeight * result.PixelWidth), (i) =>
                                        for(int i = 0; i < Convert.ToInt64(result.PixelHeight * result.PixelWidth); i++)
                                        {
                                            if (map.mapping[i].Y >= 0 && map.mapping[i].Y < description.Height && map.mapping[i].X >= 0 && map.mapping[i].X < description.Width)
                                            {
                                                Byte intensity = (Byte) (depthBufferDataPtr[(int)(Math.Floor(map.mapping[i].Y) * tempBufferWidth + Math.Floor(map.mapping[i].X))] & 0x00FF); 
                                                resultDataPtr[4 * i] = intensity;
                                                resultDataPtr[4 * i + 1] = intensity;
                                                resultDataPtr[4 * i + 2] = intensity;
                                                resultDataPtr[4 * i + 3] = 0xFF;
                                            }
                                            else 
                                            {
                                                // If depth cannot be mapped, then we display red.
                                                resultDataPtr[4 * i] = 0x00;
                                                resultDataPtr[4 * i + 1] = 0x00;
                                                resultDataPtr[4 * i + 2] = 0xFF;
                                                resultDataPtr[4 * i + 3] = 0xFF;
                                            }
                                        }//);
                                    }
                                }
                                //dframe.Dispose();
                                return true;
                            }
                            else
                            {
                                return false;
                                //Console.Write("Depth frame lost.");
                                //throw new FrameNotAcquiredError("Depth frame lost.");
                            }
                        }
                    }
                }
                return false;
            }
            private bool IRToBitmap(MappedTo mapping, Rect rect)
            {
                Util.WriteableBitmapProxy result = null;
                if (!MappedWholeBitmapFrameProxies.TryGetValue(GetKey(BitmapType.Infrared, mapping), out result))
                {
                    Debug.Assert(false, "MappedWholeBitmapFrame for type: BitmapType.Infrared and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                    throw new FrameNotAcquiredError("MappedWholeBitmapFrame for type: BitmapType.Infrared and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                }
                else
                {
                    if (mapping == MappedTo.DepthCoord || mapping == MappedTo.InfraredCoord)
                    {
                        using (InfraredFrame iframe = CurrentMultisourceFrame.InfraredFrameReference.AcquireFrame())
                        {
                            if (iframe != null && result != null)
                            {
                                using (KinectBuffer depthBuffer = iframe.LockImageBuffer())
                                {
                                    FrameDescription description = iframe.FrameDescription;
                                    iframe.CopyFrameDataToIntPtr(result.BackBuffer, (uint)(result.PixelHeight * result.BackBufferStride));
                                    //iframe.Dispose();
                                }
                                return true;
                            }
                            else
                            {
                                return false;
                                //Console.Write("IR frame lost.");
                                //throw new FrameNotAcquiredError("IR frame lost.");
                            }
                        }
                    }
                    else if (mapping == MappedTo.ColorCoord)
                    {
                        using (InfraredFrame iframe = CurrentMultisourceFrame.InfraredFrameReference.AcquireFrame())
                        {
                            if (iframe != null && result != null)
                            {
                                FrameDescription description = iframe.FrameDescription;
                                Mapping<Coord3D> map = GetMapping(BitmapType.Infrared, mapping);
                                var width = result.PixelWidth;
                                var height = result.PixelHeight;
                                Debug.Assert(width > 0 && height > 0, "Assertion error - IRToBitmap: frame parameters width and height must be more than 0.");
                                Int32 tempBufferWidth = description.Width;
                                using (KinectBuffer irBuffer = iframe.LockImageBuffer())
                                {
                                    unsafe
                                    {
                                        Int16* irBufferDataPtr = (Int16*)irBuffer.UnderlyingBuffer;
                                        Int16* resultDataPtr = (Int16*)result.BackBuffer;
                                        // Do the mapping.
                                        //Parallel.For(0, Convert.ToInt64(result.PixelHeight * result.PixelWidth), (i) =>
                                        for(int i = 0; i < Convert.ToInt64(result.PixelHeight * result.PixelWidth); i++)
                                        {
                                            if (map.mapping[i].Y >= 0 && map.mapping[i].Y < description.Height && map.mapping[i].X >= 0 && map.mapping[i].X < description.Width)
                                            {
                                                resultDataPtr[i] = irBufferDataPtr[(int)(Math.Floor(map.mapping[i].Y) * tempBufferWidth + Math.Floor(map.mapping[i].X))];
                                            }
                                            else
                                            {
                                                resultDataPtr[i] = 0x0000;
                                            }
                                        }//);
                                    }
                                }
                                return true;
                                //iframe.Dispose();
                            }
                        }
                    }
                }
                return false;
            }

            public override Coord3D[] GetCoord3DNow(System.Drawing.PointF[] points, MappedTo mapping)
            {
                Coord3D[] result = null;
                Mapping<Coord3D> currentMapping = null;
                if (mapping == MappedTo.ColorCoord)
                {
//                    if (Ca2CoUpd == false)
//                    {
//                        if (Ca2Co == null)
//                        {
//                            Ca2Co = new Mapping<Coord3D>(new Coord3D[CWidth * CHeight]);
//                        }
                        //GenerateCameraToColorMap(Ca2Co.mapping);
//                        Ca2CoUpd = true;
//                    }
                    currentMapping = Ca2Co;
                }
                else if (mapping == MappedTo.DepthCoord || mapping == MappedTo.InfraredCoord)
                {
//                    if (Ca2DUpd == false)
//                    {
//                        if (Ca2D == null)
//                        {
//                            Ca2D = new Mapping<Coord3D>(new Coord3D[DWidth * DHeight]);
//                        }
//                        GenerateCameraToDepthMap(Ca2D.mapping);
//                        Ca2DUpd = true;
//                    }
                    currentMapping = Ca2D;
                }
                else
                {
                    Debug.Assert(false, "Unsupported mapping for GetCoord3DNow.");
                }
                result = new Coord3D[points.Length];
                BufferMetrics metrics = GetBufferMetrics(mapping);
                for (int i = 0; i < points.Length; i++)
                {
                    int X = (int)Math.Floor(points[i].X);
                    int Y = (int)Math.Floor(points[i].Y);
                    if (X >= 0 && X < metrics.Width
                        && Y >= 0 && Y < metrics.Height)
                    {
                        // Note we have to convert from Kinect coordinates to our world coordinates.
                        result[i].Y = currentMapping.mapping[X + Y * metrics.Width].X;
                        result[i].Z = currentMapping.mapping[X + Y * metrics.Width].Y;
                        result[i].X = currentMapping.mapping[X + Y * metrics.Width].Z;
                    }
                }
                return result;
            }
            private bool Coord3DToBitmap(MappedTo mapping, Rect rect)
            {
                Util.WriteableBitmapProxy result = null;
                if (!MappedWholeBitmapFrameProxies.TryGetValue(GetKey(BitmapType.Coord3D, mapping), out result))
                {
                    Debug.Assert(false, "MappedWholeBitmapFrame for type: BitmapType.Infrared and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                    throw new FrameNotAcquiredError("MappedWholeBitmapFrame for type: BitmapType.Infrared and mapping: " + mapping.ToString() + " cannot be accessed/found.");
                }
                else
                {
                    Mapping<Coord3D> map = GetMapping(BitmapType.Coord3D, mapping);
                    unsafe
                    {
                        Byte* resultDataPtr = (Byte*)result.BackBuffer;
                        //IntPtr resultDataPtr = result.BackBuffer;
                        for (long a = 0; a < rect.Width * rect.Height; a++ )
                        {
                            long i = (long)(rect.X + rect.Y * result.PixelWidth + (a % rect.Width) + Math.Floor(a / rect.Width));
                                Byte[] y = BitConverter.GetBytes( map.mapping[i].X);
                                Byte[] z = BitConverter.GetBytes( map.mapping[i].Y);
                                Byte[] x = BitConverter.GetBytes( map.mapping[i].Z);
                                resultDataPtr[16 * i] = x[0];
                                resultDataPtr[16 * i + 1] = x[1];
                                resultDataPtr[16 * i + 2] = x[2];
                                resultDataPtr[16 * i + 3] = x[3];
                                resultDataPtr[16 * i + 4] = y[0];
                                resultDataPtr[16 * i + 5] = y[1];
                                resultDataPtr[16 * i + 6] = y[2];
                                resultDataPtr[16 * i + 7] = y[3];
                                resultDataPtr[16 * i + 8] = z[0];
                                resultDataPtr[16 * i + 9] = z[1];
                                resultDataPtr[16 * i + 10] = z[2];
                                resultDataPtr[16 * i + 11] = z[3]; 
                                resultDataPtr[16 * i + 12] = 0x00;
                                resultDataPtr[16 * i + 13] = 0x00;
                                resultDataPtr[16 * i + 14] = 0x00;
                                resultDataPtr[16 * i + 15] = 0x00; 
                        }
                            // Do the mapping.
                            //Parallel.For(0, map.mapping.LongLength, (i) =>
                            //for (long i = 0; i < map.mapping.LongLength; i++)
                            //{
                                //Marshal.StructureToPtr(map.mapping[i], resultDataPtr, false);
                                //resultDataPtr += Marshal.SizeOf(typeof(Coord3D));
                            //}//);
                    }
                    return true;
                }
            }
            //Once the frame arrives, all orders are process. Including updating the buffers/bitmaps and running the callbacks.
            protected override void ProcessFrame()
            {
                try
                {
                    FrameProcessing = true;
                    InitializeMappings();
                    WriteBitmaps();
                }
                catch (FrameNotAcquiredError e)
                {
                    Console.Write(e.Message);
                }
                finally
                {
                    FrameProcessing = false;
                    Orders.Clear();
                    base.FinishedProcessingFrame();
                }
            }

            void  frameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs args)
            {
                CurrentMultisourceFrame = args.FrameReference.AcquireFrame();
                FrameTime = CurrentMultisourceFrame.DepthFrameReference.RelativeTime;
                if (CurrentMultisourceFrame != null && FrameProcessing == false)
                {
                     ProcessFrame();
                }
            }
        }
        // Calculates positions, velocities and rotations of tracked features (in meters, and degrees). The coordinate system is similar to 
        // the skeleton space of kinect (right handed with Y the up direction according to the accelerometer and Z pointing away from the 
        // depth sensor).
        protected internal class MetricsCalculator
        {
            public MetricsCalculator()
            {
            }
        }

    }
}