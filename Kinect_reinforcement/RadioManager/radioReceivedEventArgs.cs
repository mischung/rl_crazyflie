﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.Connection
{
    public class radioReceivedEventArgs<re> : EventArgs
    {
        public re Data { get; private set; } // The data received.
        public long Latency { get; private set; } //Round trip latency in ms.
        public bool Success { get; private set; } //Is the command successfully acknowledged.
        public radioReceivedEventArgs(re data, long latency, bool success)
        {
            Data = data;
            Latency = latency;
            Success = success;
        }
    }

}
