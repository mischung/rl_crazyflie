﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrazyflieDotNet.Crazyflie.TransferProtocol;

namespace Kinect_reinforcement.Connection
{
    public class LowLevelCommanderCommand
    {
        public LowLvlPacketPayload payload {get; private set;}
        public LowLvlPacketHeader header { get; private set; }
        public LowLevelCommanderCommand(byte motor1, byte motor2, byte motor3, byte motor4)
        {
            payload = new LowLvlPacketPayload(new byte[] { 0x01, motor1, motor2, motor3, motor4 });
            header = new LowLvlPacketHeader(Channel.Channel0);
        }
        public LowLevelCommanderCommand()
        {
            payload = new LowLvlPacketPayload(new byte[] { 0x00 });
            header = new LowLvlPacketHeader(Channel.Channel0);
        }
    }
}
