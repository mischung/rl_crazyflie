﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Timers;
using CrazyflieDotNet.Crazyradio.Driver;
using CrazyflieDotNet.Crazyflie.TransferProtocol;
using CrazyflieDotNet;


namespace Kinect_reinforcement.Connection
{
    public class RadioManager
    {
        public delegate void RadioReceivedHandler(object sender, EventArgs e);
        public event RadioReceivedHandler radioReceived;
        public delegate void RadioConnectedHandler(object sender, EventArgs e);
        public event RadioConnectedHandler radioConnected;
        public delegate void RadioDisconnectedHandler(object sender, EventArgs e);
        public event RadioDisconnectedHandler radioDisconnected;
        public delegate void RadioErrorHandler(object sender, EventArgs e);
        public event RadioErrorHandler radioError;

        private CrazyradioMessenger CrazyRadioMessenger = null;
        private bool _isConnected = false;
        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
            private set
            {
                _isConnected = value;
                // Turn the check connection timer on when connected.
                this.ConnectionTimer.Enabled = value;
                if (value)
                {
                    if (radioConnected != null)
                        radioConnected(this, null);
                }
                else
                {
                    if (radioDisconnected != null)
                        radioDisconnected(this, null);
                }
            }
        }
        private int TimeOut;
        private const int MinTimeOut = 10;
        private int FailedPings = 0;
        private System.Timers.Timer ConnectionTimer = new System.Timers.Timer(2000);

        public RadioManager(int timeOutPings = MinTimeOut) // number of pings before timeout.
        {
            IsConnected = false;
            TimeOut = Math.Max(timeOutPings, MinTimeOut);
            ConnectionTimer.AutoReset = true;
            ConnectionTimer.Elapsed += new ElapsedEventHandler(CheckConnection);
        }
        public bool Connect()
        {
            try
            {
                var crazyradioDriver = CrazyradioDriver.GetCrazyradios().First();
                crazyradioDriver.Open();
                var scanResults = crazyradioDriver.ScanChannels(RadioChannel.Channel0, RadioChannel.Channel125);
                if (scanResults.Any())
                {
                    var firstScanResult = scanResults.First();

                    var dataRateWithCrazyflie = firstScanResult.DataRate;
                    var channelWithCrazyflie = firstScanResult.Channels.First();

                    crazyradioDriver.DataRate = dataRateWithCrazyflie;
                    crazyradioDriver.Channel = channelWithCrazyflie;
                    CrazyRadioMessenger = new CrazyradioMessenger(crazyradioDriver);
                    IsConnected = true;
                }
                else
                {
                    crazyradioDriver.Close();
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                Console.Write("Connection Error", e);
                if (radioError != null)
                    radioError(this, null);
                IsConnected = false;
                return false;
            }
            return true;
        }
        public void Disconnect()
        {
            try
            {
                CrazyRadioMessenger = null;
                var crazyradioDriver = CrazyradioDriver.GetCrazyradios().First();
                crazyradioDriver.Close();
            }
            finally
            {
                IsConnected = false;
                ConnectionTimer.Enabled = false;
            }
        }
        public Task<bool> ConnectAsync()
        {
            return Task.Run<bool>(() =>
                {
                    return Connect();
                });
        }
        public Task DisconnectAsync()
        {
            return Task.Run(() =>
            {
                Disconnect();
            });
        }
        public Task TransmitAsync(LowLevelCommanderCommand command, bool persistent = false)
        {
            return Task.Run(() => { Transmit(command, persistent); });
        }
        public void CheckConnection(object sender, ElapsedEventArgs e)
        {
            if (CrazyRadioMessenger != null && IsConnected)
            {
                try
                {
                    var ackPacket = CrazyRadioMessenger.SendMessage(new PingPacket());
                    FailedPings = 0;
                }
                catch
                {
                    if (FailedPings >= TimeOut)
                    {
                        FailedPings = 0;
                        IsConnected = false;
                    }
                    else
                    {
                        FailedPings++;
                    }

                }
            }
            else
            {
                IsConnected = false;
            }
        }
        public void Transmit(LowLevelCommanderCommand command, bool persistent = false, int retries = 10)
        {
            if (CrazyRadioMessenger != null && IsConnected)
            {
                Stopwatch timer = Stopwatch.StartNew();
                try
                {
                    var ackPacket = CrazyRadioMessenger.SendMessage(new LowLvlPacket(command.header, command.payload));

                    if (ackPacket.Payload != null && ackPacket.Payload.GetBytes().Length >= 7)
                    {
                        var ackPacketBytes = ackPacket.Payload.GetBytes();
                        if (radioReceived != null)
                            radioReceived(this, new radioReceivedEventArgs<LowLevelCommanderDataBundle>(new LowLevelCommanderDataBundle(ackPacketBytes), timer.ElapsedMilliseconds, true));
                    }
                    else
                    {
                        if (!persistent)
                        {
                            if (radioReceived != null)
                                radioReceived(this, new radioReceivedEventArgs<LowLevelCommanderDataBundle>(null, timer.ElapsedMilliseconds, false));
                        }
                        else
                        {
                            if (retries > 0)
                            {
                                Thread.Sleep(10);
                                Transmit(command, true, retries - 1);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.Write("Transmission Error", e);
                    if (!persistent)
                    {
                        if (radioReceived != null)
                            radioReceived(this, new radioReceivedEventArgs<LowLevelCommanderDataBundle>(null, timer.ElapsedMilliseconds, false));
                    }
                    else
                    {
                        Transmit(command, true);
                    }
                }
                finally
                {
                    timer.Stop();
                }
            }
            else
            {
                if (radioReceived != null)
                    radioReceived(this, new radioReceivedEventArgs<LowLevelCommanderDataBundle>(null, 0, false));
            }
        }
        ~RadioManager()
        {
            if(IsConnected)
            {
                Disconnect();
            }
        }

    }
}
