﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using Emgu.CV.Structure;
using Emgu.CV;
using Feature_Tracking;



namespace Kinect_reinforcement
{

    public static class Util
    {
        public class WriteableBitmapProxy
        {
            public IntPtr BackBuffer { get; private set; }
            public int PixelWidth { get; private set; }
            public int PixelHeight { get; private set; }
            public int BackBufferStride { get; private set; }

            public WriteableBitmapProxy(WriteableBitmap bmp)
            {
                BackBuffer = bmp.BackBuffer;
                PixelWidth = bmp.PixelWidth;
                PixelHeight = bmp.PixelHeight;
                BackBufferStride = bmp.BackBufferStride;
            }
        }
        public static Bitmap BitmapFromWriteableBitmap(WriteableBitmap writeBmp)
        {
            Stopwatch watch = Stopwatch.StartNew();
            Bitmap bmp;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)writeBmp));
                enc.Save(outStream);
                bmp = new Bitmap(outStream);
            }
            watch.Stop();
            Console.Write("BitmapFromWriteableBitmap took " + watch.ElapsedMilliseconds + "ms to complete.");
            return bmp;
        }
        public static Image<Gray, byte> GrayImageFromWriteableBitmap(WriteableBitmap writeBmp, PixelFormat format)
        {
            byte[] buffer = new byte[writeBmp.BackBufferStride * writeBmp.PixelHeight];
            int height = writeBmp.PixelHeight;
            int width = writeBmp.PixelWidth;
            writeBmp.CopyPixels(buffer, writeBmp.BackBufferStride, 0);
            if (format == PixelFormats.Gray16)
            {
                Image<Gray, Int16> bmp = new Image<Gray, Int16>(width, height);
                bmp.Bytes = buffer;
                return bmp.Convert<Gray, byte>();
            }
            else if (format == PixelFormats.Bgr32)
            {
                Image<Bgra, byte> bmp = new Image<Bgra, byte>(width, height);
                bmp.Bytes = buffer;
                return bmp.Convert<Gray, byte>();
            }
            else if (format == PixelFormats.Bgr24)
            {
                Image<Bgr, byte> bmp = new Image<Bgr, byte>(width, height);
                bmp.Bytes = buffer;
                return bmp.Convert<Gray, byte>();
            }
            else if (format == PixelFormats.Gray8)
            {
                Image<Gray, byte> bmp = new Image<Gray, byte>(width, height);
                bmp.Bytes = buffer;
                return bmp;
            }
            else
            {
                Debug.Assert(false, "GrayImageFromWriteableBitmapAsync: PixelFormat not implemented.");
                throw new NotImplementedException();
            }
        }
        public static Bitmap BitmapFromImageSource(ImageSource writeBmp)
        {
            Stopwatch watch = Stopwatch.StartNew();
            Bitmap bmp;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)writeBmp));
                enc.Save(outStream);
                bmp = new Bitmap(outStream);
            }
            watch.Stop();
            Console.Write("BitmapFromWriteableBitmap took " + watch.ElapsedMilliseconds + "ms to complete.");
            return bmp;
        }
        public static ImageSource ImageSourceFromBitmap(Bitmap bmp)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                                bmp.GetHbitmap(),
                                IntPtr.Zero,
                                System.Windows.Int32Rect.Empty,
                                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }
        //Gets the mean position and normal from a set of 3D points. Assumes the points all lie on a plane. Returns true with meanPosition and normal if successful.
        public static bool GetPositionAndNormal(Coord3D[] points, out Coord3D meanPosition, out Coord3D normal)
        {
            List<float[]> inputValues = new List<float[]>();
            for (int i = 0; i < points.Length; i++)
            {
                if (!float.IsInfinity(points[i].X) && !float.IsInfinity(points[i].Y) && !float.IsInfinity(points[i].Z)
                            && !float.IsNaN(points[i].X) && !float.IsNaN(points[i].Y) && !float.IsNaN(points[i].Z))
                {
                    inputValues.Add(new float[] { points[i].X, points[i].Y, points[i].Z });
                }
            }
            if (inputValues.Count < 3)
            {
                meanPosition = new Coord3D();
                normal = new Coord3D();
                return false;
            }
            float[,] inputValues2DMat = new float[inputValues.Count, 3];
            for (int i = 0; i < inputValues.Count; i++)
            {
                inputValues2DMat[i, 0] = inputValues[i][0];
                inputValues2DMat[i, 1] = inputValues[i][1];
                inputValues2DMat[i, 2] = inputValues[i][2];
            }
            using (Matrix<float> data = new Matrix<float>(inputValues2DMat))
            using (Mat mean = new Mat())
            //Matrix<Double> eigenValues = new Matrix<Double>(1, 3);
            using (Matrix<float> eigenVectors = new Matrix<float>(3, 3))
            {
                CvInvoke.PCACompute(data, mean, eigenVectors);
                using (Matrix<float> normalVector = new Matrix<float>(1, 3))
                {
                    eigenVectors.GetRow(0).CopyTo(normalVector);
                    CvInvoke.Normalize(normalVector, normalVector);
                    float[] tmp = new float[3];
                    mean.CopyTo(tmp);
                    meanPosition = new Coord3D(tmp[0], tmp[1], tmp[2]);
                    normal = new Coord3D(normalVector[0, 0], normalVector[0, 1], normalVector[0, 2]);
                }
            }
            return true;
        }
        public static void GetMeanStdev(Coord3D[] input, out Coord3D mean, out Coord3D stdev)
        {
            MCvScalar m = new MCvScalar();
            MCvScalar std = new MCvScalar();
            float[,] coord = new float[input.Length, 3];
            //float[,] coord = new float[input.LongLength, 3];
            for (long i = 0; i < input.LongLength; i++)
            {
                coord[i, 0] = input[i].X;
                coord[i, 1] = input[i].Y;
                coord[i, 2] = input[i].Z;
            }
            using (Matrix<float> mcoord = new Matrix<float>(coord))
            {
                CvInvoke.MeanStdDev(mcoord.Reshape(3, input.Length), ref m, ref std);
                mean = new Coord3D((float)m.V0, (float)m.V1, (float)m.V2);
                stdev = new Coord3D((float)std.V0, (float)std.V1, (float)std.V2);
            }
        }
        // Shamelessly copied.
        public static bool IsNumber(this object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }
        // Utility to return field information as a string.
        public static string DisplayValues<T>(string varName, T variable, string units) where T : struct
        {
            StringBuilder sb = new StringBuilder(varName + ": (");
            IList<FieldInfo> properties = typeof(T).GetFields();
            foreach (var property in properties)
            {
                if (Util.IsNumber(property.GetValue(variable)))
                {
                    sb.Append(" ").Append(String.Format("{0:0.000}", property.GetValue(variable))).Append(units).Append(",");
                }
                else
                {
                    sb.Append(" ").Append(property.GetValue(variable)).Append(units).Append(",");
                }
            }
            sb.Remove(sb.Length - 1, 1).Append(")").AppendLine();
            return sb.ToString();
        }
        public static Double GetDistance(Coord3D d1, Coord3D d2)
        {
            return Math.Sqrt(Math.Pow(d1.X - d2.X, 2) + Math.Pow(d1.Y - d2.Y, 2) + Math.Pow(d1.Z - d2.Z, 2));
        }
        public static Coord3D GetMean(Coord3D[] coords)
        {
            Coord3D mean, stdev;
            GetMeanStdev(coords, out mean, out stdev);
            return mean;
         /*   float[] fx = new float[coords.Length];
            float[] fy = new float[coords.Length];
            float[] fz = new float[coords.Length];
            for (int k = 0; k < coords.Length; k++)
            {
                fx[k] = coords[k].X;
                fy[k] = coords[k].Y;
                fz[k] = coords[k].Z;
            }
            return new Coord3D(fx.Average(), fy.Average(), fz.Average()); */
        }
    }
}
