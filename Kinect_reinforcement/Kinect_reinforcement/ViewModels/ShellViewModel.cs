﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Kinect_reinforcement.Feature_Tracking;
using Kinect_reinforcement.Connection;


namespace Kinect_reinforcement.ViewModels
{
    
    public class ShellViewModel : Conductor<object>.Collection.OneActive  
    {
        private FeatureTracker _tracker;
        private RadioManager _radio;

        public ShellViewModel(FeatureTracker tracker, RadioManager radio )
        {
            _tracker = tracker;
            _radio = radio;
        }

        protected override void OnInitialize()
        {
            ShowSamplePage();
            base.OnInitialize();
        }
        public void ShowSamplePage()
        {
            ActivateItem(new OfflineLearningPageViewModel() { DisplayName = "Offline Learning" });
            ActivateItem(new LearningPageViewModel(_radio, _tracker) { DisplayName = "Learning" });
            ActivateItem(new RadioConnectionPageViewModel(_radio) { DisplayName = "Radio" });
            ActivateItem(new TrackingPageViewModel(_tracker) { DisplayName = "Tracking" });
            ActivateItem(new SamplePageViewModel(_tracker) { DisplayName = "Sample" });
        }


    }
}
