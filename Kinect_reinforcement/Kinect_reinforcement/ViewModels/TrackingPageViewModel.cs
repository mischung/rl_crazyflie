﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Data;
using System.Windows.Threading;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;
using Caliburn.Micro;
using Kinect_reinforcement.Feature_Tracking;
using Emgu.CV.Structure;



namespace Kinect_reinforcement.ViewModels
{
    class TrackingPageViewModel : Screen
    {
        private FeatureTracker _tracker;
        TimeSpan lastTime = TimeSpan.Zero;
        WriteableBitmap trackingFrame;
        BindableCollection<ImageSource> sourceImages = new BindableCollection<ImageSource>();
        BindableCollection<String> keyPointPositions = new BindableCollection<String>();
        String fps;
        BindableCollection<Coord3D> trackingMean = new BindableCollection<Coord3D>();
        BindableCollection<Coord3D> trackingStdev = new BindableCollection<Coord3D>();
        BindableCollection<string> trackingMeanText = new BindableCollection<string>();
        BindableCollection<string> trackingStdevText = new BindableCollection<string>();
        MKeyPoint[] keyPoints = null;
        int threshold = 100;
        int frameNumber = 0; //Counter to display bitmaps alternatively.
        //Export centerTrackingPositions with their mean and standard deviation to file in csv format with header information.
        public void ExportTrackingData()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                StringBuilder sb = new StringBuilder();
                //Add headers.
                IList<FieldInfo> properties = typeof(Coord3D).GetFields();
                sb.Append("Trajectory:").Append(",");
                foreach (var propertyInfo in properties)
                {
                    sb.Append("Center Position:" + propertyInfo.Name).Append(",");
                }
                sb.Append("Time (ms)").Append(",");
                foreach (var propertyInfo in properties)
                {
                    sb.Append("Mean Center Position:" + propertyInfo.Name).Append(",");
                }
                foreach (var propertyInfo in properties)
                {
                    sb.Append("Stdev Center Position:" + propertyInfo.Name).Append(",");
                }
                //Remove last comman and add new line.
                sb.Remove(sb.Length - 1, 1).AppendLine();
                List<FeatureTracker.Trajectory> trajectoryList = _tracker.GetTrajectories();
                for (var j = 0; j < trajectoryList.Count; j++)
                {
                    Coord3D[] position = trajectoryList[j].Entities.ConvertAll((t) => { return t.Position; }).ToArray();
                    TimeSpan[] time = trajectoryList[j].Entities.ConvertAll((t) => { return t.Time; }).ToArray();
                    for (int i = 0; i < position.Length; i++)
                    {
                        sb.Append(j.ToString()).Append(",");
                        foreach (var propertyInfo in properties)
                        {
                            sb.Append(propertyInfo.GetValue(position[i]).ToString()).Append(",");
                        }
                        sb.Append(time[i].TotalMilliseconds).Append(",");
                        sb.Remove(sb.Length - 1, 1).AppendLine();
                    }
                    Coord3D mean, stdev;
                    Util.GetMeanStdev(position, out mean, out stdev);
                    // Add mean and stdev to last row.
                    sb.Append(j.ToString()).Append(",,,,,"); // skip the center position columns.
                    foreach (var propertyInfo in properties)
                    {
                        sb.Append(propertyInfo.GetValue(mean).ToString()).Append(",");
                    }
                    foreach (var propertyInfo in properties)
                    {
                        sb.Append(propertyInfo.GetValue(stdev).ToString()).Append(",");
                    }
                    sb.Remove(sb.Length - 1, 1).AppendLine();
                }
                File.WriteAllText(saveFileDialog.FileName, sb.ToString());
            }

        }
        public void ResetTracking()
        {
            _tracker.ResetTracking();
            TrackingStdevText.Clear();
            TrackingMeanText.Clear();
        }
        public BindableCollection<string> TrackingMeanText
        {
            get { return trackingMeanText; }
            private set
            {
                trackingMeanText = value;
                NotifyOfPropertyChange(() => TrackingMeanText);
            }
        }
        public BindableCollection<string> TrackingStdevText
        {
            get { return trackingStdevText; }
            private set
            {
                trackingStdevText = value;
                NotifyOfPropertyChange(() => TrackingStdevText);
            }
        }
        public BindableCollection<Coord3D> TrackingMean
        {
            get { return trackingMean; }
            set
            {
                trackingMean = value;
                TrackingMeanText.Clear();
                foreach (var mean in TrackingMean)
                {
                    TrackingMeanText.Add(Util.DisplayValues<Coord3D>("Mean", mean, "m"));
                }
                NotifyOfPropertyChange(() => TrackingMean);
            }
        }
        public BindableCollection<Coord3D> TrackingStdev
        {
            get { return trackingStdev; }
            set
            {
                trackingStdev = value;
                TrackingStdevText.Clear();
                foreach (var std in TrackingStdev)
                {
                    TrackingStdevText.Add(Util.DisplayValues<Coord3D>("Stdev", std, "m"));
                }
                NotifyOfPropertyChange(() => TrackingStdev);
            }
        }
        public int Threshold
        {
            get { return threshold; }
            set
            {
                threshold = value;
                _tracker.TrackFeatures(new FeatureTracker.CrazyFlieConfiguratuion1Features(Threshold), new FeatureTracker.CrazyFlieConfiguration1());
                NotifyOfPropertyChange(() => Threshold);
            }
        }
        public BindableCollection<String> KeyPointPositions
        {
            get { return keyPointPositions; }
            set
            {
                keyPointPositions = value;
                NotifyOfPropertyChange(() => KeyPointPositions);
            }
        }
        public String Fps
        {
            get { return fps; }
            set
            {
                fps = value;
                NotifyOfPropertyChange(() => Fps);
            }
        }
        public BindableCollection<ImageSource> SourceImages
        {
            get { return sourceImages; }
            set
            {
                sourceImages = value;
                NotifyOfPropertyChange(() => SourceImages);
            }
        }
        public WriteableBitmap TrackingFrame
        {
            get { return trackingFrame; }
            set
            {
                trackingFrame = value;
                NotifyOfPropertyChange(() => TrackingFrame);
            }
        }
        public TrackingPageViewModel(FeatureTracker tracker)
        {
            _tracker = tracker;
        }

        protected override void OnActivate()
        {
            _tracker.imageFrameUpdated += tracker_imageFrameUpdated;
            _tracker.trajectoriesUpdated += _tracker_trajectoriesUpdated;
            _tracker.featuresUpdated += _tracker_featuresUpdated;
            _tracker.TrackFeatures(new FeatureTracker.CrazyFlieConfiguratuion1Features(Threshold), new FeatureTracker.CrazyFlieConfiguration1());
            base.OnActivate();
        }
        void _tracker_featuresUpdated(object sender, EventArgs e)
        {
            try
            {
                FeatureTracker.Features features = _tracker.GetFeatures();
                if (features != null)
                {
                    Coord3D[][] coords = features.PointClouds;
                    TimeSpan time = features.Time;
                    keyPoints = features.KeyPoints.ToArray();
                    KeyPointPositions.Clear();
                    for (int i = 0; i < coords.Length; i++)
                    {
                        Coord3D mean, normal;
                        if (Util.GetPositionAndNormal(coords[i], out mean, out normal))
                        {
                            Coord3D centroid = new Coord3D(mean.X + normal.X * (float)0.036, mean.Y + normal.Y * (float)0.036, mean.Z + normal.Z * (float)0.036);
                            KeyPointPositions.Add(Util.DisplayValues<Coord3D>("Mean(PCA)", mean, "m") + Util.DisplayValues<Coord3D>("Center", centroid, "m"));
                            // Check if distance from TrackingMean is outside of tolerance.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write("Error update Page: ", ex.Message);
            }
        }
        void _tracker_trajectoriesUpdated(object sender, EventArgs e)
        {
            List<FeatureTracker.Trajectory> trajectoryList = _tracker.GetTrajectories();
            BindableCollection<Coord3D> tMean = new BindableCollection<Coord3D>();
            BindableCollection<Coord3D> tStdev = new BindableCollection<Coord3D>();
            foreach (var trajectory in trajectoryList)
            {
                //Only show long trajectories.
                if (trajectory.Entities.Count > 100)
                {
                    Feature_Tracking.Coord3D[] positions = trajectory.Entities.ConvertAll((t) => { return t.Position; }).ToArray();
                    Coord3D m, s;
                    Util.GetMeanStdev(positions, out m, out s);
                    tMean.Add(m);
                    tStdev.Add(s);
                }
            }
            if (tMean.Count > 0)
            {
                TrackingMean = tMean;
                TrackingStdev = tStdev;
            }
        }
        protected override void OnInitialize()
        {
            base.OnInitialize();
        }
        protected override void OnDeactivate(bool close)
        {
            _tracker.imageFrameUpdated -= tracker_imageFrameUpdated;
            _tracker.trajectoriesUpdated -= _tracker_trajectoriesUpdated;
            _tracker.featuresUpdated -= _tracker_featuresUpdated;
            _tracker.PauseTracking();
            base.OnDeactivate(close);
        }

        void tracker_imageFrameUpdated(object sender, EventArgs e)
        {
            //Fps = "FPS: " + ((int)(1000 / (_tracker.GetFrameTime().Milliseconds - lastTime.Milliseconds + 1))).ToString();
            lastTime = _tracker.GetFrameTime();
            _tracker.GetBitmap(BitmapType.Color, MappedTo.InfraredCoord, (B) => 
            { 
                if (TrackingFrame == null || TrackingFrame.PixelHeight != B.PixelHeight || TrackingFrame.PixelWidth != B.PixelWidth || TrackingFrame.Format != B.Format)
                {
                    TrackingFrame = B.Clone();
                }
                else
                {
                    TrackingFrame.Blit(new System.Windows.Rect(0, 0, TrackingFrame.PixelWidth, TrackingFrame.PixelHeight), B, new System.Windows.Rect(0, 0, TrackingFrame.PixelWidth, TrackingFrame.PixelHeight));
                }
            });
            if (keyPoints != null && trackingFrame != null)
            {
                System.Windows.Media.Color color = System.Windows.Media.Color.FromArgb(0xFF, 0x00, 0x00, 0xff);
                foreach (var keypoint in keyPoints)
                {
                    Rectangle rect = new Rectangle((int)keypoint.Point.X - 3, (int)keypoint.Point.Y - 3, 7, 7);
                    ((WriteableBitmap)trackingFrame).DrawLine(rect.Left, rect.Top, rect.Right, rect.Bottom, color);
                    ((WriteableBitmap)trackingFrame).DrawLine(rect.Right, rect.Top, rect.Left, rect.Bottom, color);
                }
            }
            keyPoints = null;
        }
    }
}
