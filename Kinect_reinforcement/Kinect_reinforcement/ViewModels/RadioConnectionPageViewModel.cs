﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using CrazyflieDotNet.Crazyradio.Driver;
using CrazyflieDotNet;
using Kinect_reinforcement.Connection;
using System.Threading;
using System.Windows;
using System.Diagnostics;

namespace Kinect_reinforcement.ViewModels
{
    class RadioConnectionPageViewModel : Screen
    {
        private bool _connected = false;
        private RadioManager _radio = null;
        private BindableCollection<LowLevelCommanderDataBundle> _dataList = new BindableCollection<LowLevelCommanderDataBundle>();
        private BindableCollection<long> _latency = new BindableCollection<long>();
        private string _toggleConnectionText = "Connect";
        private byte _motor1 = 0;
        private byte _motor2 = 0;
        private byte _motor3 = 0;
        private byte _motor4 = 0;
        private string _avg, _stdev, _min, _max;
        private bool _canControlMotor = false;

        public void UpdateLatency()
        {
            var avg = _latency.Average();
            double sum = _latency.Sum(d => Math.Pow(d - avg, 2));
            //Put it all together      
            double stdev = Math.Sqrt((sum) / (_latency.Count() - 1));
            double max = _latency.Max();
            double min = _latency.Min();
            Avg = String.Format("{0:0.00}", avg);
            Stdev = String.Format("{0:0.00}", stdev);
            Max = String.Format("{0:0.00}", max);
            Min = String.Format("{0:0.00}", min);
    
        }
        public string Avg
        {
            get { return _avg; }
            set
            {
                _avg = value;
                NotifyOfPropertyChange(() => Avg);
            }
        }
        public string Stdev
        {
            get { return _stdev; }
            set
            {
                _stdev = value;
                NotifyOfPropertyChange(() => Stdev);
            }
        }
        public string Min
        {
            get { return _min; }
            set
            {
                _min = value;
                NotifyOfPropertyChange(() => Min);
            }
        }
        public string Max
        {
            get { return _max; }
            set
            {
                _max = value;
                NotifyOfPropertyChange(() => Max);
            }
        }
        public bool CanControlMotor
        {
            get { return _canControlMotor; }
            set
            {
                _canControlMotor = value;
                NotifyOfPropertyChange(() => CanControlMotor);
            }
        }
        public byte Motor1
        {
            get { return _motor1; }
            set
            {
                _motor1 = value;
                NotifyOfPropertyChange(() => Motor1);
            }
        }
        public byte Motor2
        {
            get { return _motor2; }
            set
            {
                _motor2 = value;
                NotifyOfPropertyChange(() => Motor2);
            }
        }
        public byte Motor3
        {
            get { return _motor3; }
            set
            {
                _motor3 = value;
                NotifyOfPropertyChange(() => Motor3);
            }
        }
        public byte Motor4
        {
            get { return _motor4; }
            set
            {
                _motor4 = value;
                NotifyOfPropertyChange(() => Motor4);
            }
        }
        public string ToggleConnectionText
        {
            get { return _toggleConnectionText; }
            set
            {
                _toggleConnectionText = value;
                NotifyOfPropertyChange(() => ToggleConnectionText);
            }
        }
        public bool Connected
        {
            get { return _connected; }
            set
            {
                _connected = value;
                NotifyOfPropertyChange(() => Connected);
                if (value)
                {
                    ToggleConnectionText = "Disconnect";
                }
                else
                {
                    ToggleConnectionText = "Connect";
                }
            }
        }
        public BindableCollection<LowLevelCommanderDataBundle> DataList
        {
            get { return _dataList; }
            set
            {
                _dataList = value;
                NotifyOfPropertyChange(() => DataList);
                if (Connected)
                {
                    _radio.TransmitAsync(new LowLevelCommanderCommand(Motor1, Motor2, Motor3, Motor4), true);
                }
            }
        }
        public RadioConnectionPageViewModel(RadioManager radio)
        {
                _radio = radio;
        }
        protected override void OnActivate()
        {
            _radio.radioConnected += _radio_radioConnected;
            _radio.radioDisconnected += _radio_radioDisconnected;
            _radio.radioReceived += _radio_radioReceived;
            _radio.radioError += _radio_radioError;
            base.OnActivate();
        }
        protected override void OnInitialize()
        {
            base.OnInitialize();
        }

        void _radio_radioError(object sender, EventArgs e)
        {
            MessageBox.Show("Connection failed.");
        }
        protected override void OnDeactivate(bool close)
        {
            if (Connected)
            {
                ToggleConnection();
            }
            _radio.radioConnected -= _radio_radioConnected;
            _radio.radioDisconnected -= _radio_radioDisconnected;
            _radio.radioReceived -= _radio_radioReceived;
            _radio.radioError -= _radio_radioError;
            base.OnDeactivate(close);
        }
        void _radio_radioReceived(object sender, EventArgs e)
        {
            if (((radioReceivedEventArgs<LowLevelCommanderDataBundle>)e).Success)
            {
                LowLevelCommanderDataBundle data = ((radioReceivedEventArgs<LowLevelCommanderDataBundle>)e).Data;
                DataList.Add(data);
                _latency.Add(((radioReceivedEventArgs<LowLevelCommanderDataBundle>)e).Latency);
                UpdateLatency();
                Task.Run(() =>
                    {
                        int waitTime = (int)Math.Max(0, 5 - ((radioReceivedEventArgs<LowLevelCommanderDataBundle>)e).Latency);
                        Thread.Sleep(waitTime);
                        if (CanControlMotor)
                        {
                            _radio.TransmitAsync(new LowLevelCommanderCommand(Motor1, Motor2, Motor3, Motor4), true);
                        }
                        else
                        {
                            _radio.TransmitAsync(new LowLevelCommanderCommand(), true);
                        }
                    });
                NotifyOfPropertyChange(() => DataList);
            }
        }
        void _radio_radioDisconnected(object sender, EventArgs e)
        {
            Connected = false;
        }
        void _radio_radioConnected(object sender, EventArgs e)
        {
            Connected = true;
            _radio.TransmitAsync(new LowLevelCommanderCommand(), true);
        }
        public void ToggleConnection()
        {
            if (Connected)
            {
                _radio.DisconnectAsync();
            }
            else
            {
                _radio.ConnectAsync();
            }
        }
        public void Reset()
        {
            DataList.Clear();
            NotifyOfPropertyChange(() => DataList);
            _latency.Clear();
            Min = "";
            Max = "";
            Avg = "";
            Stdev = "";
        }
    }
}
