﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Media;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using Caliburn.Micro;
using Kinect_reinforcement.Commander;
using Kinect_reinforcement.ReinforcementLearner;
using Kinect_reinforcement.Connection;
using Kinect_reinforcement.Feature_Tracking;
using Emgu.CV.Structure;


namespace Kinect_reinforcement.ViewModels
{
    public class InputKeyValue : PropertyChangedBase 
    {
        private string _key;
        private float _value;
        public string Key {
            get
            {
                return _key;
            }
            set 
            {
                if (_key == value)
                    return;
                _key = value;
                NotifyOfPropertyChange(() => Key);
                }
        }
        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value == value)
                    return;
                _value = value;
                NotifyOfPropertyChange(() => Value);
            }
        }
    }

    class LearningPageViewModel : Screen
    {
        int frameCount = 0;
        bool _isLearning = false;
        bool _prepareLearning = false;
        RadioManager _radio;
        FeatureTracker _tracker;
        ILearningCommander _commander;
        IReinforcementLearningAlgorithm _learningAlgorithm = null;
        IReinforcementLearner _learner = null;
        string _script;
        TrackingScriptingRules _rules =  new TrackingScriptingRules();
        String _log = "";
        MKeyPoint[] keyPoints = null;
        float _threshold = 100;
        WriteableBitmap _trackingFrame;
        private string _selectedAlgorithm;
        BindableCollection<InputKeyValue> _inputList = new BindableCollection<InputKeyValue>();
        private InputKeyValue _selectedInput;
        public ILearningCommander Commander
        {
            get
            {
                return _commander;
            }
            set
            {
                if (_commander != null)
                {
                    _commander.learningStart -= _commander_learningStart;
                    _commander.learningReset -= _commander_learningReset;
                    _commander.learningError -= _commander_learningError;
                }
                _commander = value;
                NotifyOfPropertyChange(() => IsLearning);
                NotifyOfPropertyChange(() => CanStartLearning);
                NotifyOfPropertyChange(() => CanRestartLearning);
                NotifyOfPropertyChange(() => CanLoadState);
                NotifyOfPropertyChange(() => CanSaveState);
                if (_commander != null)
                {
                    _commander.learningStart += _commander_learningStart;
                    _commander.learningReset += _commander_learningReset;
                    _commander.learningError += _commander_learningError;
                }

            }
        }
        public bool IsLearning
        {
            get
            {
                return _isLearning;
            }
            set
            {
                _isLearning = value;
                NotifyOfPropertyChange(() => IsLearning);
                NotifyOfPropertyChange(() => CanStartLearning);
                NotifyOfPropertyChange(() => CanRestartLearning);
                NotifyOfPropertyChange(() => CanLoadState);
                NotifyOfPropertyChange(() => CanSaveState);

            }
        }
        public bool PrepareLearning
        {
            get
            {
                return _prepareLearning;
            }
            set
            {
                _prepareLearning = value;
                NotifyOfPropertyChange(() => PrepareLearning);
                NotifyOfPropertyChange(() => CanStartLearning);
                NotifyOfPropertyChange(() => CanRestartLearning);
                NotifyOfPropertyChange(() => CanLoadState);
                NotifyOfPropertyChange(() => CanSaveState);

            }
        }

        public BindableCollection<string> Algorithms
        {
            get
            {
                // silly example of the collection to bind to
                return new BindableCollection<string>(
                                 new string[] { "Dummy", "Launch (CMA-NeuroES)", "Flight Recorder", "Launch (CMA-NeuroES) with feed forward neural network simulation" });
            }
        }
        public string SelectedAlgorithm
        {
            get { return _selectedAlgorithm; }
            set
            {
                _selectedAlgorithm = value;
                NotifyOfPropertyChange(() => SelectedAlgorithm);
                InputList.Clear();
                _learner = null;
                if (_selectedAlgorithm == "Dummy")
                {
                    _learningAlgorithm = new DummyLearningAlgorithm();
                    foreach(var item in new string(_learningAlgorithm.GetInputMapping()).Split(new char[]{','}))
                    {
                        InputKeyValue inputItem = new InputKeyValue();
                        inputItem.Key = item;
                        inputItem.Value = 0.0f;
                        InputList.Add(inputItem);
                    }
                    _learner = new CrazyflieOffsiteReinforcementLearner<IReinforcementLearningAlgorithm>(_learningAlgorithm, _radio);
                    Commander = new CrazyflieLearningCommander();
                    Commander.CreateLearningSession(_tracker, _rules, _learner);
                }
                else if (_selectedAlgorithm == "Launch (CMA-NeuroES)")
                {
                    //Make random initial state.
                    DateTime timeSeed = DateTime.Now;
                    Random rnd = new Random((int)timeSeed.Ticks);
                    int numWeights = (int)SimpleRecurrentNetwork.GetNumberOfWeights(4, 5, 4);
                    byte[] weights = new byte[numWeights * sizeof(float)];
                    for (int i = 0; i < numWeights; i++)
                        BitConverter.GetBytes((float)(rnd.NextDouble() - 0.5) * 2).CopyTo(weights, i * sizeof(float));
                    _learningAlgorithm = new CMANeuroES("LAUNCHHEIGHT,ROLL,PITCH,VBATT".ToCharArray(), "MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(), 5, true, 0.5f, 0.1f, 0, 3, weights,
                            new float[,] { { -1f, 1f }, { -180f, 180f }, { -180f, 180f }, { -0.8f, 0.8f } }, new float[,] { { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } });
                    InputKeyValue inputItem = new InputKeyValue();
                    inputItem.Key = "LAUNCHHEIGHT";
                    inputItem.Value = 0.4f;
                    InputList.Add(inputItem);
                    _learner = new CrazyflieOffsiteReinforcementLearner<IReinforcementLearningAlgorithm>(_learningAlgorithm, _radio);
                    Commander = new CrazyflieLearningCommander();
                    Commander.CreateLearningSession(_tracker, _rules, _learner);
                }
                else if (_selectedAlgorithm == "Flight Recorder")
                {
                    _learningAlgorithm = new DummyLearningAlgorithm();
                    foreach (var item in new string(_learningAlgorithm.GetOutputMapping()).Split(new char[] { ',' }))
                    {
                        InputKeyValue inputItem = new InputKeyValue();
                        inputItem.Key = item;
                        inputItem.Value = 0.0f;
                        InputList.Add(inputItem);
                    }
                    _learner = new CrazyflieOffsiteApprenticeLearner<IReinforcementLearningAlgorithm>(_learningAlgorithm, _radio);
                    Commander = new CrazyflieLearningCommander();
                    Commander.CreateLearningSession(_tracker, _rules, _learner);
                }
                else if (_selectedAlgorithm == "Launch (CMA-NeuroES) with feed forward neural network simulation")
                {
                    //Make random initial state.
                    DateTime timeSeed = DateTime.Now;
                    Random rnd = new Random((int)timeSeed.Ticks);
                    int numWeights = (int)SimpleRecurrentNetwork.GetNumberOfWeights(4, 4, 4);
                    byte[] weights = new byte[numWeights * sizeof(float)];
                    for (int i = 0; i < numWeights; i++)
                        BitConverter.GetBytes((float)(rnd.NextDouble() - 0.5) / 50f).CopyTo(weights, i * sizeof(float));
                    _learningAlgorithm = new CMANeuroES("LAUNCHHEIGHT,ROLL,PITCH,VBATTERYC".ToCharArray(), "MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(), 4, true, 5f, 0.5f, 200, 3, weights,
                            new float[,] { { -1f, 1f }, { -180f, 180f }, { -180f, 180f }, { -0.8f, 0.8f } }, new float[,] { { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } });
                    InputKeyValue inputItem = new InputKeyValue();
                    inputItem.Key = "LAUNCHHEIGHT";
                    inputItem.Value = 0.8f;
                    InputList.Add(inputItem);
                    _learner = new SimulationReinforcementLearner<IReinforcementLearningAlgorithm>(_learningAlgorithm);
                    byte[] loadBytes = null;
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    if (openFileDialog.ShowDialog() == true)
                    {
                        loadBytes = File.ReadAllBytes(openFileDialog.FileName);
                    }
                    if (loadBytes != null) {
                        SimplePropagator simulator = new SimplePropagator("ROLL,PITCH,ROLLRATE,PITCHRATE,YAWRATE,VXb,VYb,VZb,ACCX,ACCY,ACCZ,LAND,VBATTERYC,MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(),
                            "ROLL,PITCH,ROLLRATE,PITCHRATE,YAWRATE,VXb,VYb,VZb,ACCX,ACCY,ACCZ".ToCharArray(), new int[] { 29 }, false, loadBytes, false,
                            new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -1000f, 1000f }, { -1000f, 1000f },
                        { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -1f, 1f }, { -0.8f, 0.8f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } },
                            new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -1000f, 1000f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f } });
                        Commander = new NeuralNetworkSimulationReinforcementLearner(simulator, 0.001f,
                                        new Dictionary<string, float>
                                        {
                                            {"ROLL", 0f},
                                            {"PITCH", 0f},
                                            {"YAW", 0f},
                                            {"ROLLRATE", 0f},
                                            {"PITCHRATE", 0f},
                                            {"YAWRATE", 0f},
                                            {"VXb", 0f},
                                            {"VYb", 0f},
                                            {"VZb", 0f},
                                            {"ACCX", 0f},
                                            {"ACCY", 0f},
                                            {"ACCZ", 0f},
                                            {"LAND", -1f},
                                            {"VBATTERYC", 0.8f},
                                            {"MOTOR1", 0f},
                                            {"MOTOR2", 0f},
                                            {"MOTOR3", 0f},
                                            {"MOTOR4", 0f},
                                            {"POSX", 0f},
                                            {"POSY", 0f},
                                            {"POSZ", 0f},
                                            {"FTIME", 0f}
                                        }, 0.03f, 1000, 5e5f);
                        Commander.CreateLearningSession(null, _rules, _learner);
                    }
                }
                NotifyOfPropertyChange(() => InputList);
                NotifyOfPropertyChange(() => CanStartLearning);
                NotifyOfPropertyChange(() => CanLoadState);
                NotifyOfPropertyChange(() => CanSaveState);
            }
        }
        public InputKeyValue SelectedInput
        {
            get
            {
                return _selectedInput;
            }
            set
            {
                _selectedInput = value;
                NotifyOfPropertyChange(() => SelectedInput);
            }
        }
        public float Threshold
        {
            get
            {
                return _threshold;
            }
            set
            {
                _threshold = value;
                NotifyOfPropertyChange(() => Threshold);
            }
        }
        public string Script
        {
            get
            {
                return _script;
            }
            set
            {
                _script = value;
                NotifyOfPropertyChange(() => Script);
                NotifyOfPropertyChange(() => CanStartLearning);
            }
        }
        public string Log
        {
            get
            {
                return _log;
            }
            set
            {
                _log = value;
                NotifyOfPropertyChange(() => Log);
            }
        }
        public WriteableBitmap TrackingFrame
        {
            get { return _trackingFrame; }
            set
            {
                _trackingFrame = value;
                NotifyOfPropertyChange(() => TrackingFrame);
            }
        }
        public BindableCollection<InputKeyValue> InputList
        {
            get
            {
                return _inputList;
            }
            set
            {
                _inputList = value;
                NotifyOfPropertyChange(() => InputList);
            }
        }
        public LearningPageViewModel(RadioManager radio, FeatureTracker tracker)
        {
            _radio = radio;
            _tracker = tracker;
        }
        protected override void OnActivate()
        {
            SelectedAlgorithm = "";
            _tracker.imageFrameUpdated += _tracker_imageFrameUpdated;
            _tracker.featuresUpdated += _tracker_featuresUpdated;
            Script = "function Propagate () \n Log('Please insert Propagate function \\n') \n end \n";
            base.OnActivate();
        }
        protected override void OnInitialize()
        {
            base.OnInitialize();
        }

        void _commander_learningError(object sender, EventArgs e)
        {
            MessageBox.Show("Learning start failed.");
            PrepareLearning = false;
        }

        void _commander_learningReset(object sender, EventArgs e)
        {
            IsLearning = false;
            PrepareLearning = false;
        }

        void _commander_learningStart(object sender, EventArgs e)
        {
            IsLearning = true;
        }

        void _tracker_featuresUpdated(object sender, EventArgs e)
        {
            try
            {
                FeatureTracker.Features features = _tracker.GetFeatures();
                if (features != null)
                {
                    Coord3D[][] coords = features.PointClouds;
                    TimeSpan time = features.Time;
                    keyPoints = features.KeyPoints.ToArray();
                    for (int i = 0; i < coords.Length; i++)
                    {
                        Coord3D mean, normal;
                        if (Util.GetPositionAndNormal(coords[i], out mean, out normal))
                        {
                            Coord3D centroid = new Coord3D(mean.X + normal.X * (float)0.036, mean.Y + normal.Y * (float)0.036, mean.Z + normal.Z * (float)0.036);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write("Error update Page: ", ex.Message);
            }
        }

        void _tracker_imageFrameUpdated(object sender, EventArgs e)
        {
            if (frameCount == 0)
            {
                _tracker.GetBitmap(BitmapType.Color, MappedTo.InfraredCoord, (B) =>
                {
                    if (TrackingFrame == null || TrackingFrame.PixelHeight != B.PixelHeight || TrackingFrame.PixelWidth != B.PixelWidth || TrackingFrame.Format != B.Format)
                    {
                        TrackingFrame = B.Clone();
                    }
                    else
                    {
                        TrackingFrame.Blit(new System.Windows.Rect(0, 0, TrackingFrame.PixelWidth, TrackingFrame.PixelHeight), B, new System.Windows.Rect(0, 0, TrackingFrame.PixelWidth, TrackingFrame.PixelHeight));
                    }
                });
                if (keyPoints != null && _trackingFrame != null)
                {
                    System.Windows.Media.Color color = System.Windows.Media.Color.FromArgb(0xFF, 0x00, 0x00, 0xff);
                    foreach (var keypoint in keyPoints)
                    {
                        Rectangle rect = new Rectangle((int)keypoint.Point.X - 3, (int)keypoint.Point.Y - 3, 7, 7);
                        ((WriteableBitmap)_trackingFrame).DrawLine(rect.Left, rect.Top, rect.Right, rect.Bottom, color);
                        ((WriteableBitmap)_trackingFrame).DrawLine(rect.Right, rect.Top, rect.Left, rect.Bottom, color);
                    }
                }
                keyPoints = null;
            }
            frameCount++;
            frameCount = frameCount % 1;
            Log = _rules.Log;

        }
        protected override void OnDeactivate(bool close)
        {
            _tracker.imageFrameUpdated -= _tracker_imageFrameUpdated;
            _tracker.featuresUpdated -= _tracker_featuresUpdated;
            if (Commander != null)
                Commander.Terminate();
            Commander = null;
            SelectedAlgorithm = "";
            base.OnDeactivate(close);
        }
        public void StartLearning()
        {
            PrepareLearning = true;
            _rules.ClearLog();
            if (_rules.LoadScript(Script))
            {
                _commander.InitializeLearning(Parse(InputList), Threshold);
            }
            else
            {
                PrepareLearning = false;
            }
        }
        public void RestartLearning()
        {
            _commander.ResetEpisode(0);
        }
        public void LoadState()
        {
            if (_learningAlgorithm != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    byte[] loadBytes = File.ReadAllBytes(openFileDialog.FileName);
                    _learningAlgorithm.SetState(loadBytes);
                }
            }
        }
        public void SaveState()
        {
            if (_learningAlgorithm != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true)
                {
                    byte[] saveBytes = _learningAlgorithm.GetState();
                    File.WriteAllBytes(saveFileDialog.FileName, saveBytes);
                }
            }
        }
        public void SaveSLog()
        {
            if (_learningAlgorithm != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true)
                {
                    string saveString = _rules.SLog;
                    File.WriteAllText(saveFileDialog.FileName, saveString);
                }
            }
        }
        public bool CanStartLearning
        {
            get
            {
                return (_learningAlgorithm != null) && (Script != null) && (!IsLearning) && (!PrepareLearning) && (Commander != null);
            }
        }
        public bool CanRestartLearning
        {
            get
            {
                return Commander != null && Commander.IsLearning;
            }
        }
        public bool CanLoadState
        {
            get
            {
                return (_learningAlgorithm != null) && (!IsLearning) && (Commander != null);
            }
        }
        public bool CanSaveState
        {
            get
            {
                return (_learningAlgorithm != null) && (!IsLearning) && (Commander != null);
            }
        }
        private Dictionary<string, float> Parse(BindableCollection<InputKeyValue> list)
        {
            Dictionary<string, float> result = new Dictionary<string, float>();

            foreach (var item in list)
            {
                if (item.Key != "" && Util.IsNumber(item.Value))
                {
                    result.Add(item.Key, item.Value);
                }
            }
            return result;
        }
    }
}
