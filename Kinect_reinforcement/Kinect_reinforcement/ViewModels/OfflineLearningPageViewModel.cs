﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Diagnostics;
using Microsoft.Win32;
using System.Windows;
using Caliburn.Micro;
using Kinect_reinforcement.ReinforcementLearner;
using Kinect_reinforcement.Feature_Tracking;

namespace Kinect_reinforcement.ViewModels
{
    public class OfflineLearningPageViewModel : Screen
    {

        string _errorData;
        string _quadraticError;
        ISupervisedLearningAlgorithm _learner;
        private string _selectedAlgorithm;
        bool _isProcessing = false;
        int _numHidden = 29;

        public bool IsProcessing
        {
            get
            {
                return _isProcessing;
            }
            set
            {
                _isProcessing = value;
                NotifyOfPropertyChange(() => IsProcessing);
                NotifyOfPropertyChange(() => CanLearn);
                NotifyOfPropertyChange(() => CanLoadState);
                NotifyOfPropertyChange(() => CanSaveState);
                NotifyOfPropertyChange(() => CanAnalyzeLearning);
            }
        }
        public string QuadraticError
        {
            get
            {
                return _quadraticError;
            }
            set
            {
                _quadraticError = value;
                NotifyOfPropertyChange(() => QuadraticError);
            }
        }
        public string ErrorData
        {
            get
            {
                return _errorData;
            }
            set
            {
                _errorData = value;
                NotifyOfPropertyChange(() => ErrorData);
            }
        }
        public BindableCollection<string> Algorithms
        {
            get
            {
                return new BindableCollection<string>(
                                 new string[] { "Flight state action learner", "Flight state action learner(Air)", "Flight model learner", "Flight model learner(Air)" });
            }
        }
        public string SelectedAlgorithm
        {
            get { return _selectedAlgorithm; }
            set
            {
                _selectedAlgorithm = value;
                NotifyOfPropertyChange(() => SelectedAlgorithm);
                _learner = null;
                if (_selectedAlgorithm == "Flight model learner")
                {
                    _learner = new KFoldCrossValidationNN("ROLL,PITCH,YAWRATE,VXb,VYb,VZb,LAND,VBATTERYC,ROLLPRIME,PITCHPRIME,YAWRATEPRIME,VXbPRIME,VYbPRIME,VZbPRIME".ToCharArray(), "MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(),
                        new int[] { _numHidden }, false, 0.01f, 5.0f, null, true, new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -1f, 1f }, { -0.8f, 0.8f },
                            { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f } },
                        new float[,] { { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } });
                }
                if (_selectedAlgorithm == "Flight model learner(Air)")
                {
                    _learner = new KFoldCrossValidationNN("ROLL,PITCH,YAWRATE,VXb,VYb,VZb,VBATTERYC,ROLLPRIME,PITCHPRIME,YAWRATEPRIME,VXbPRIME,VYbPRIME,VZbPRIME".ToCharArray(), "MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(),
                        new int[] { _numHidden }, false, 0.01f, 5.0f, null, true, new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -0.8f, 0.8f },
                            { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }},
                        new float[,] { { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } });
                }
                if (_selectedAlgorithm == "Flight state action learner")
                {
                    _learner = new KFoldCrossValidationNN("ROLL,PITCH,ROLLRATE,PITCHRATE,YAWRATE,VXb,VYb,VZb,ACCX,ACCY,ACCZ,LAND,VBATTERYC,MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(),
                        "ROLLPRIME,PITCHPRIME,ROLLRATEPRIME,PITCHRATEPRIME,YAWRATEPRIME,VXbPRIME,VYbPRIME,VZbPRIME,ACCXPRIME,ACCYPRIME,ACCZPRIME".ToCharArray(),
                        new int[] { _numHidden }, false, 0.1f, 5.0f, null, false, new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -1000f, 1000f }, { -1000f, 1000f },
                        { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -1f, 1f }, { -0.8f, 0.8f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } },
                        new float[,] {  { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -1000f, 1000f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }});
                }
                if (_selectedAlgorithm == "Flight state action learner(Air)")
                {
                    _learner = new KFoldCrossValidationNN("ROLL,PITCH,ROLLRATE,PITCHRATE,YAWRATE,VXb,VYb,VZb,ACCX,ACCY,ACCZ,VBATTERYC,MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray(),
                        "ROLLPRIME,PITCHPRIME,ROLLRATEPRIME,PITCHRATEPRIME,YAWRATEPRIME,VXbPRIME,VYbPRIME,VZbPRIME,ACCXPRIME,ACCYPRIME,ACCZPRIME".ToCharArray(),
                        new int[] { _numHidden }, false, 0.1f, 5.0f, null, false, new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -1000f, 1000f }, { -1000f, 1000f },
                        { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -0.8f, 0.8f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f }, { 0f, 256f } },
                        new float[,] { { -180f, 180f }, { -180f, 180f }, { -1000f, 1000f }, { -1000f, 1000f }, { -1000f, 1000f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f }, { -2f, 2f } });
                }
                NotifyOfPropertyChange(() => CanLearn);
                NotifyOfPropertyChange(() => CanLoadState);
                NotifyOfPropertyChange(() => CanSaveState);
                NotifyOfPropertyChange(() => CanAnalyzeLearning);
                 
            }
        }


        public OfflineLearningPageViewModel()
        {

        }
        private byte[][] Parse(string[] text)
        {
            try
            {
                string[] learnerHeaders = (new string(_learner.GetInputMapping()) + ',' + new string(_learner.GetOutputMapping())).Split(new char[] { ',' });
                //Assume the first line is the header.
                string[] textHeaders = text[0].Split(new char[] { ',' });

                //Make a map of headers so that each column of the text can be mapped to the learner headers.
                int[] learnerTextHeaderMap = new int[learnerHeaders.Length];
                for (int i = 0; i < learnerTextHeaderMap.Length; i++)
                {
                    int index = -1;
                    for (int j = 0; j < textHeaders.Length; j++)
                    {
                        if (learnerHeaders[i] == textHeaders[j])
                            index = j;
                    }
                    learnerTextHeaderMap[i] = index;
                }

                //Allocate the output and parse the data.
                byte[][] result = new byte[text.Length - 1][];
                for (int i = 0; i < result.GetLength(0); i++)
                {
                    string[] textDataRow = text[i + 1].Split(new char[] { ',' });
                    result[i] = new byte[(_learner.GetNumberOfInputs() + _learner.GetNumberOfOutputs()) * sizeof(float)];
                    for (int j = 0; j < _learner.GetNumberOfInputs() + _learner.GetNumberOfOutputs(); j++)
                    {
                        if (learnerTextHeaderMap[j] == -1)
                        {
                            BitConverter.GetBytes(0.0f).CopyTo(result[i], j * sizeof(float));
                        }
                        else
                        {
                            float value = Convert.ToSingle(textDataRow[learnerTextHeaderMap[j]]);
                            if (!Util.IsNumber(value))
                            {
                                value = 0.0f;
                            }
                            BitConverter.GetBytes(value).CopyTo(result[i], j * sizeof(float));
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Parsing failed: -" + ex.Message);
                return null;
            }
        }
        public void Learn()
        {
            if (_learner != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        NotifyOfPropertyChange(() => CanLearn);
                        NotifyOfPropertyChange(() => CanLoadState);
                        NotifyOfPropertyChange(() => CanSaveState);
                        NotifyOfPropertyChange(() => CanAnalyzeLearning);
                        IsProcessing = true;
                        string[] loadBytes = File.ReadAllLines(openFileDialog.FileName);
                        StringBuilder sb = new StringBuilder();
                        //Parse result back float, along with headers.
                        byte[] result2 = _learner.BatchLearnGetQuadraticError(Parse(loadBytes));
                        float error2;
                        error2 = BitConverter.ToSingle(result2, 0);
                        QuadraticError = error2.ToString();
                        sb.AppendLine("Quadratic Error: " + error2);
//                        byte[][] result = _learner.BatchLearn(Parse(loadBytes));
//                        sb.Append(_learner.GetOutputMapping());
//                        int headersLength = new string(_learner.GetOutputMapping()).Split(new char[] { ',' }).Length;
//                        for (int i = 0; i < result.GetLength(0); i++)
//                        {
//                            sb.AppendLine();
//                            float[] error = new float[headersLength];
//                            for (int j = 0; j < headersLength; j++)
//                            {
//                                error[j] = BitConverter.ToSingle(result[i], j * sizeof(float));
//                                sb.Append(error[j]).Append(",");
//                            }
//                            sb.Remove(sb.Length - 1, 1);
//                        }
                        ErrorData = sb.ToString();
                        

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Learning failed: -" + ex.Message);
                    }
                    IsProcessing = false;
                    NotifyOfPropertyChange(() => CanLearn);
                    NotifyOfPropertyChange(() => CanLoadState);
                    NotifyOfPropertyChange(() => CanSaveState);
                    NotifyOfPropertyChange(() => CanAnalyzeLearning);
                }
            }
        }
        public void AnalyzeLearning()
        {
            if (_learner != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        IsProcessing = true;
                        NotifyOfPropertyChange(() => CanLearn);
                        NotifyOfPropertyChange(() => CanLoadState);
                        NotifyOfPropertyChange(() => CanSaveState);
                        NotifyOfPropertyChange(() => CanAnalyzeLearning);
                        ErrorData = "Processing....";
                        string[] loadBytes = File.ReadAllLines(openFileDialog.FileName);
                        StringBuilder sb = new StringBuilder();
                        //Parse result back float, along with headers.
                        byte[][] parsedBytes = Parse(loadBytes);
                        for (int b = 1; b < 41; b++)
                        {
                            _numHidden = b;
                            sb.AppendLine("Hidden Units: " + _numHidden);
                            ErrorData = sb.ToString();
                            Debug.WriteLine("Analyzing with " + b + " hidden units.");
                            for (int a = 0; a < 10; a++)
                            {
                                SelectedAlgorithm = SelectedAlgorithm; //Trigger the reinitiation of learner.
                                byte[] result = _learner.BatchLearnGetQuadraticError(parsedBytes, false);
                                float error;
                                error = BitConverter.ToSingle(result, 0);
                                QuadraticError = error.ToString();
                                sb.AppendLine("Quadratic Error (Random weights): " + error);
                                result = _learner.BatchLearnGetQuadraticError(parsedBytes, true);
                                error = BitConverter.ToSingle(result, 0);
                                QuadraticError = error.ToString();
                                sb.AppendLine("Quadratic Error: " + error);
                                ErrorData = sb.ToString();
                                _learner = null;
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                                Debug.WriteLine("Running......");
                            }
                        }
                        ErrorData = sb.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Learning failed: -" + ex.Message);
                    }
                    IsProcessing = false;
                    NotifyOfPropertyChange(() => CanLearn);
                    NotifyOfPropertyChange(() => CanLoadState);
                    NotifyOfPropertyChange(() => CanSaveState);
                    NotifyOfPropertyChange(() => CanAnalyzeLearning);
                }
            }

        }
        public void SaveState()
        {
            if (_learner != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true)
                {
                    byte[] saveBytes = _learner.GetState();
                    File.WriteAllBytes(saveFileDialog.FileName, saveBytes);
                }
            }
        }
        public void LoadState()
        {
            if (_learner != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    byte[] loadBytes = File.ReadAllBytes(openFileDialog.FileName);
                    _learner.SetState(loadBytes);
                }
            }

        }
        public bool CanLoadState
        {
            get
            {
                return _selectedAlgorithm != null && !IsProcessing;
            }
        }
        public bool CanSaveState
        {
            get
            {
                return _selectedAlgorithm != null && !IsProcessing;
            }
        }
        public bool CanLearn
        {
            get
            {
                return _selectedAlgorithm != null && !IsProcessing;
            }
        }
        public bool CanAnalyzeLearning
        {
            get
            {
                return _selectedAlgorithm != null && !IsProcessing;
            }
        }

    }
}
