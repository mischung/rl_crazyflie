﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Kinect_reinforcement.Feature_Tracking;
using System.Windows.Media;

namespace Kinect_reinforcement.ViewModels
{

    class SamplePageViewModel : Screen 
    {
        private FeatureTracker _tracker;// = new FeatureTracker();
        TimeSpan lastTime = TimeSpan.Zero;
        int frameNumber = 0; //Counter to display bitmaps alternatively.
        ImageSource colorImage, irImage, depthImage, irImageUnmapped;
        String fps;

        public SamplePageViewModel(FeatureTracker tracker)
        {
            _tracker = tracker;
        }

        public String Fps
        {
            get { return fps; }
            set
            {
                fps = value;
                NotifyOfPropertyChange(() => Fps);
            }
        }

        public ImageSource ColorImage
        {
            get { return colorImage; }
            set { 
                colorImage = value;
                NotifyOfPropertyChange(() => ColorImage);
            }
        }
        public ImageSource IrImage
        {
            get { return irImage; }
            set
            {
                irImage = value;
                NotifyOfPropertyChange(() => IrImage);
            }
        }
        public ImageSource DepthImage
        {
            get { return depthImage; }
            set
            {
                depthImage = value;
                NotifyOfPropertyChange(() => DepthImage);
            }
        }
        public ImageSource IrImageUnmapped
        {
            get { return irImageUnmapped; }
            set
            {
                irImageUnmapped = value;
                NotifyOfPropertyChange(() => IrImageUnmapped);
            }
        }
        
        protected override void OnActivate()
        {
            _tracker.imageFrameUpdated += tracker_imageFrameUpdated;
            CompositionTarget.Rendering += CompositionTarget_Rendering;
            base.OnActivate();
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
        }

        protected override void OnDeactivate(bool close)
        {
            _tracker.imageFrameUpdated -= tracker_imageFrameUpdated;
            CompositionTarget.Rendering -= CompositionTarget_Rendering;
            base.OnDeactivate(close);
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
        }

        void tracker_imageFrameUpdated(object sender, EventArgs e)
        {
            frameNumber = (frameNumber + 1) % 4;
            Fps = "FPS: " + ((int)(1000 / (_tracker.GetFrameTime().Milliseconds - lastTime.Milliseconds + 1))).ToString();
            lastTime = _tracker.GetFrameTime();
            // We alternate images between frames.
            // switch (frameNumber)
            // {
            //     case 0:
            _tracker.GetBitmap(BitmapType.Color, MappedTo.ColorCoord, (B) => { ColorImage = B; });
            //         break;
            //     case 1:
            _tracker.GetBitmap(BitmapType.Infrared, MappedTo.InfraredCoord, (B) => { IrImage = B; });
            //         break;
            //     case 2:
            //_tracker.GetBitmap(FeatureTracker.BitmapType.DepthBGRA, FeatureTracker.MappedTo.DepthCoord, (B) => { DepthImage = B; });
            //         break;
            //     case 3:
           // _tracker.GetBitmap(FeatureTracker.BitmapType.Infrared, FeatureTracker.MappedTo.InfraredCoord, (B) => { IrImageUnmapped = B; });
            //         break;
            // }

        }

    }
}
