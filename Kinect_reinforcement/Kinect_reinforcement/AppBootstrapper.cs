﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Caliburn.Micro;
using Kinect_reinforcement.ViewModels;
using Kinect_reinforcement.Feature_Tracking;
using Kinect_reinforcement.Connection;

namespace Kinect_reinforcement
{
    public class AppBootstrapper : BootstrapperBase
    {
        private SimpleContainer _container = new SimpleContainer();

        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;
            throw new InvalidOperationException("Could not locate any instance.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        public AppBootstrapper()
        {
            Initialize();
        }
        
        protected override void Configure()
        {
            _container.Instance<IWindowManager>(new WindowManager());
            _container.PerRequest<ShellViewModel>();
            _container.RegisterSingleton(typeof(FeatureTracker), null, typeof(FeatureTracker));
            _container.RegisterSingleton(typeof(RadioManager), null, typeof(RadioManager));
        }
         
        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void OnUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Debug.Write("Unhandled exception: " + e.Exception.Message);
            base.OnUnhandledException(sender, e);
        }
    }
}
