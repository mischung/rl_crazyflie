﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Feature_Tracking;
using System.Threading;
using System.Globalization;

namespace Kinect_reinforcement
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FeatureTracker tracker = new FeatureTracker();
        TimeSpan lastTime = TimeSpan.Zero;
        int frameNumber = 0; //Counter to display bitmaps alternatively.

        public MainWindow()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us"); //Set debugging to English.
            InitializeComponent();
            this.Loaded += windowLoaded;
        }

        void windowLoaded(object sender, RoutedEventArgs e)
        {
            tracker.imageFrameUpdated += tracker_imageFrameUpdated;
            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            // We alternate images between frames.
            switch (frameNumber)
            {
               case 0:
                   tracker.GetBitmap(FeatureTracker.BitmapType.Color, FeatureTracker.MappedTo.ColorCoord, (B) => { colorImage.Source = B;});
                   break;
               case 1:
                   tracker.GetBitmap(FeatureTracker.BitmapType.Infrared, FeatureTracker.MappedTo.ColorCoord, (B) => { irImage.Source = B;});
                   break;
               case 2:
                   tracker.GetBitmap(FeatureTracker.BitmapType.DepthBGRA, FeatureTracker.MappedTo.DepthCoord, (B) => { depthImage.Source = B; });
                   break;
               case 3:
                   tracker.GetBitmap(FeatureTracker.BitmapType.Infrared, FeatureTracker.MappedTo.InfraredCoord, (B) => { irImageUnmapped.Source = B; });
                   break;
            }
        }

        void tracker_imageFrameUpdated(object sender, EventArgs e)
        {
            frameNumber = (frameNumber + 1 ) % 4;
            fps.Text = "FPS: " + ((int)(1000 / (tracker.GetFrameTime().Milliseconds - lastTime.Milliseconds + 1))).ToString();
            lastTime = tracker.GetFrameTime();
        }
    }
}
