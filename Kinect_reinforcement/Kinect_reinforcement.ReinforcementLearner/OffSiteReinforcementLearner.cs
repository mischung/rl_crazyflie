﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Timers;


namespace Kinect_reinforcement.ReinforcementLearner
{
    public class OffSiteReinforcementLearner<al> : IReinforcementLearner where al : IReinforcementLearningAlgorithm
    {
        float CurrentReward = 0;
        protected bool StartLearning = false;
        al Algorithm;
        long PropagationInterval = 5; // in ms.
        bool AutomaticPropagation = true;
        protected ConcurrentDictionary<string, float> AlgorithmInputStates = new ConcurrentDictionary<string, float>();
        protected ConcurrentDictionary<string, float> AlgorithmOutputStates = new ConcurrentDictionary<string, float>();
        private Dictionary<string, UInt32> InputMapping = new Dictionary<string, UInt32>();
        private Dictionary<string, UInt32> OutputMapping = new Dictionary<string, UInt32>();
        private byte[] ConvertInputsResultsCache = null;

        public event LearningResetHandler learningReset;
        public event LearningStartHandler learningStart;

        public Dictionary<string, float> GetOutput()
        {
            return AlgorithmOutputStates.ToDictionary(entry => entry.Key,
                                                       entry => entry.Value);
        }
        public Dictionary<string, float> GetInput()
        {
            return AlgorithmInputStates.ToDictionary(entry => entry.Key,
                                                       entry => entry.Value);
        }
        public void ResetLearnerState()
        {
            Algorithm.ResetState();
        }
        public byte[] GetState()
        {
            return Algorithm.GetState();
        }
        public void LoadState(byte[] state)
        {
            Algorithm.SetState(state);
        }
        public void Input(Dictionary<string, float> input)
        {
            foreach (var element in input)
            {
                // if (InputMapping.ContainsKey(element.Key))
                AlgorithmInputStates[element.Key.ToUpperInvariant()] = element.Value;
            }
        }
        public void InitializeLearning(Dictionary<string, float> parameters)
        {
            PreInitiationTasks();
            Input(parameters);
            CurrentReward = 0;
            StartLearning = true;
            if (AutomaticPropagation)
                LearningPropagationTask();
            if (learningStart != null)
                learningStart(this, null);
        }

        public void ResetLearning(float val) //Reset learning. Learner will wait for another round of InitializeLearning from the commander.
        {
            CurrentReward = val;
            Algorithm.ResetEpisode(ConvertValues(CurrentReward));
            StartLearning = false;
            if (learningReset != null && StartLearning == true)
                learningReset(this, null);
            PostResetTasks();
        }
        public void ResetLearning() 
        {
            ResetLearning(CurrentReward);
        }
        public void Reward(float val)
        {
            CurrentReward = val;
        }
        public void ResetLearnerStates() //Reset all states inside learner.
        {
            Algorithm.ResetState();
        }

        public OffSiteReinforcementLearner(al algorithm, bool automaticPropagation = true, double propagationInterval = 10)
        {
            Algorithm = algorithm;
            PropagationInterval = (long)propagationInterval;
            AutomaticPropagation = automaticPropagation;
            Algorithm.ResetState();
            GetAlgorithmMappings();
        }
        async void LearningPropagationTask ()
        {
            Stopwatch timer;
            while (StartLearning)
            {
                try
                {
                    timer = Stopwatch.StartNew();
                    LearningPropagate();
                    timer.Stop();
                    await Task.Delay((int)Math.Max(0, PropagationInterval - timer.ElapsedMilliseconds));
                    if (timer.ElapsedMilliseconds > PropagationInterval)
                        Debug.Write("Propagation time exceeded: " + timer.ElapsedMilliseconds + "ms");
                }
                catch (Exception ex)
                {
                    Debug.Write("Reinfocement Learning error: " + ex.Message);
                }
            }
        }
        public void LearningPropagate()
        {
            Debug.Assert(!AutomaticPropagation, "Reinforcement learner: Manual propagation triggered while automatic propagation is on. This will yield inaccurate results.");
            if (StartLearning)
            {
                Algorithm.Input(ConvertInputs());
                byte[] rawOutput = Algorithm.Propagate(ConvertValues(CurrentReward));
                ConvertOutputs(rawOutput);
                CurrentReward = 0;
                PostPropagateTasks();
            }
        }
        protected virtual void PreInitiationTasks()
        {

        }
        protected virtual void PostPropagateTasks()
        {

        }
        protected virtual void PostResetTasks()
        {

        }
        void GetAlgorithmMappings()
        {
            string[] outResult = new string(Algorithm.GetOutputMapping()).Split(new char[] { ',' });
            for(int i = 0; i < outResult.Length; i++)
            {
                OutputMapping.Add(outResult[i], (UInt32)i);
            }
            string[] inResult = new string(Algorithm.GetInputMapping()).Split(new char[] { ',' });
            for (int i = 0; i < inResult.Length; i++)
            {
                InputMapping.Add(inResult[i], (UInt32)i);
            }
        }
        byte[] ConvertInputs()
        {
            int valueSize = GetAlgorithmValueSize();
            if (ConvertInputsResultsCache == null)
                ConvertInputsResultsCache = new byte[Algorithm.GetNumberOfInputs() * valueSize];
            foreach (var input in AlgorithmInputStates)
            {
                if (InputMapping.ContainsKey(input.Key))
                {
                    int stateIndex = (int)InputMapping[input.Key];
                    byte[] valueInBytes = ConvertValues(input.Value);
                    valueInBytes.CopyTo(ConvertInputsResultsCache, stateIndex);
                    Debug.Assert((stateIndex + valueSize) <= valueSize * Algorithm.GetNumberOfInputs(), "ConvertInputs error: outside of input byte array range.");
                }
            }
            return ConvertInputsResultsCache;
        }
        int GetAlgorithmValueSize()
        {
            int valueSize = 0;
            ValueTypes valType = Algorithm.GetValueType();
            switch (valType)
            {
                case ValueTypes.BYTE:
                    valueSize = sizeof(byte);
                    break;
                case ValueTypes.FLOAT:
                    valueSize = sizeof(float);
                    break;
                case ValueTypes.INT16:
                    valueSize = sizeof(Int16);
                    break;
                case ValueTypes.UINT16:
                    valueSize = sizeof(UInt16);
                    break;
                case ValueTypes.INT32:
                    valueSize = sizeof(Int32);
                    break;
                case ValueTypes.UINT32:
                    valueSize = sizeof(UInt32);
                    break;
                default:
                    Debug.Assert(false, "ConvertOutputs error:");
                    valueSize = 0;
                    break;
            }
            return valueSize;
        }
        void ConvertOutputs(byte[] rawOutputs)
        {
            int valueSize = GetAlgorithmValueSize();
            foreach (var output in OutputMapping)
            {
                Debug.Assert(((output.Value + 1) * valueSize) <= rawOutputs.Length, "ConvertOutputs: rawOutput size smaller than output mappaing range.");
                AlgorithmOutputStates[output.Key] = (float)ConvertValues(rawOutputs, (int) output.Value * valueSize);
            }
        }
        float ConvertValues(byte[] values, int index)
        {
            ValueTypes valType = Algorithm.GetValueType();
            float result;
            switch (valType)
            {
                case ValueTypes.BYTE:
                    result = (float)values[index];
                    break;
                case ValueTypes.FLOAT:
                    result = (float)BitConverter.ToSingle(values, index);
                    break;
                case ValueTypes.INT16:
                    result = (float)BitConverter.ToInt16(values, index);
                    break;
                case ValueTypes.UINT16:
                    result = (float)BitConverter.ToUInt16(values, index);
                    break;
                case ValueTypes.INT32:
                    result = (float)BitConverter.ToInt32(values, index);
                    break;
                case ValueTypes.UINT32:
                    result = (float)BitConverter.ToUInt32(values, index);
                    break;
                default:
                    Debug.Assert(false, "ConvertValues error:");
                    result = 0;
                    break;
            }
            return result;
        }
        byte[] ConvertValues(float val)
        {
            ValueTypes valType = Algorithm.GetValueType();
            byte[] result = null;
            switch (valType)
            {
                case ValueTypes.BYTE:
                    result = new byte[] { (byte)val };
                    break;
                case ValueTypes.FLOAT:
                    result = BitConverter.GetBytes((float)val);
                    break;
                case ValueTypes.INT16:
                    result = BitConverter.GetBytes((Int16)val);
                    break;
                case ValueTypes.UINT16:
                    result = BitConverter.GetBytes((UInt16)val);
                    break;
                case ValueTypes.INT32:
                    result = BitConverter.GetBytes((Int32)val);
                    break;
                case ValueTypes.UINT32:
                    result = BitConverter.GetBytes((UInt32)val);
                    break;
                default:
                    Debug.Assert(false, "ConvertValues error:");
                    result = new Byte[] { };
                    break;
            }
            return result;
        }
        ~OffSiteReinforcementLearner()
        {
            StartLearning = false;
        }
    }
}
