﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public class LearningEventArgs : EventArgs
    {
        IReinforcementLearner Learner;

        public LearningEventArgs(IReinforcementLearner learner)
        {
            Learner = learner;
        }

    }
}
