﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Kinect_reinforcement.Connection;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public class CrazyflieOffsiteApprenticeLearner<al> : CrazyflieOffsiteReinforcementLearner<al> where al : IReinforcementLearningAlgorithm
    {
        protected override void PostPropagateTasks()
        {
            RadioConnection.TransmitAsync(new LowLevelCommanderCommand(), true);
        }
        protected override void PostResetTasks()
        {
        }
        public CrazyflieOffsiteApprenticeLearner(al algorithm, RadioManager radio) : base(algorithm, radio)
        {
        }
    }
}
