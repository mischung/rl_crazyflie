﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    // This interface is implemented on the agent for on site learning, so the interface is made compatible with C. 
    public interface ISupervisedLearningAlgorithm : IPropagator
    {

        byte[][] BatchLearn(byte[][] data); //Learn from a batch of data (input + outpus), the algorithm will split them into training and test sets automatically. Returns a set of output errors for the test set.
        byte[] BatchLearnGetQuadraticError(byte[][] data, bool learn = true);
        void Learn(byte[] data); //Learn from a batch of data (input + outpus), the algorithm will split them into training and test sets automatically. Returns a set of output errors for the test set.
        byte[] GetState();

    }
}
