﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public enum ValueTypes
    {
        BYTE = 0,
        INT16 = 1,
        UINT16 = 2,
        INT32 = 3,
        UINT32 = 4,
        FLOAT = 5,
    }

    public interface IPropagator
    {
        UInt32 GetNumberOfInputs(); // Get number of values for the input. Can be used to calculate the size of the byte array for the input.
        UInt32 GetNumberOfOutputs(); // Get number of values for the input. Can be used to calculate the size of the byte array for the outputs.
        UInt32 GetStateSize(); // Get the size of the state in bytes.
        ValueTypes GetValueType(); // Get the value type of the input and output parameters. Assumes all input and outputs uses the same value type.
        char[] GetInputMapping(); // Get a null terminated string of the input names separated by commas, order by how the values are representaed in the input byte array.
        char[] GetOutputMapping(); // Get a null terminated string of the output names separated by commas, order by how the values are representaed in the output byte array.
        void SetState(byte[] state);
        void ResetState();
        byte[] Propagate(byte[] input); //Propagate from a set of inputs.
        byte[] GetInputRange();
        byte[] GetOutputRange();

    }
}
