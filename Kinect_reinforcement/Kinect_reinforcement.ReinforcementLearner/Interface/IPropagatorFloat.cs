﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public interface IPropagatorFloat: IPropagator
    {
        void SetStateF(float[] state);
        float[] PropagateF(float[] input); //Propagate from a set of inputs.
        float[,] GetInputRangeF();
        float[,] GetOutputRangeF();
        float LinearTransformValueWithClamp(float[] range, float value, bool reverse);
    }
}
