﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public delegate void LearningResetHandler(object sender, LearningEventArgs e);
    public delegate void LearningStartHandler(object sender, LearningEventArgs e);
    // This is equivilent to the learner task in on site learning. 
    // It relays the inputs and outputs from the object and algorithm and propagates the algorithm in a timely fashion.
    public interface IReinforcementLearner
    {

        event LearningResetHandler learningReset; // This event indicates a learner requested reset. This can be triggered by problems with the agent or learning algorithm.
        event LearningStartHandler learningStart; // This event is trigger when the agent and training algorithm is ready.
        void Input(Dictionary<string, float> input);
        void InitializeLearning(Dictionary<string, float> parameters);
        void ResetLearning(float val); //Reset learning. Learner will wait for another round of InitializeLearning from the commander.
        void Reward(float val);
        Dictionary<string, float> GetOutput();
        Dictionary<string, float> GetInput();
        void ResetLearnerState(); //Reset all state inside learner.
        byte[] GetState();
        void LoadState(byte[] state);
        void LearningPropagate(); //Use for manual propagation of he learning task.
    }
}
