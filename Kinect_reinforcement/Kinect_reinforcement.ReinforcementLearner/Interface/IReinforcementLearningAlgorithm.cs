﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{

    // This interface is implemented on the agent for on site learning, so the interface is made compatible with C. 
    public interface IReinforcementLearningAlgorithm : IPropagator
    {
        new byte[] Propagate(byte[] reward); //Propagate a learning algorithm with intermediate reward as parameter. Returns output parameters.
        byte[] ResetEpisode(byte[] reward); //Reset a learning Episode. Returns the output parameters.
        byte[] GetState();
        void Input(byte[] input);
        void InputByIndex(byte[] input, UInt32 index);


    }
}
