﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public interface INeuralNetwork<t> 
    {
        t[] Propagate(t[] input);
        void BackPropagate(t[] input, t[] output);
        t[] GetError(t[] input, t[] output);
        t[] GetWeights();
        UInt32 GetNumberOfWeights();
        void Reset();
    }
}
