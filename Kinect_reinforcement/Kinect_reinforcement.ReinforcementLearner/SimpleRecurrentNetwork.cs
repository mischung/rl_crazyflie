﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Emgu.CV.Structure;
using Emgu.CV;
using Emgu.CV.Cuda;


namespace Kinect_reinforcement.ReinforcementLearner
{
    public class SimpleRecurrentNetwork : INeuralNetwork<float>
    {
        private float[] Weights;
        private Matrix<float> WeightMatrix = null;
        private Matrix<float> InputMatrix = null;
        private Matrix<float> OutputMatrix = null;
        private int TotalWeights;
        private int NumOutput;
        private int NumInput;
        private int NumHidden;
        private bool GatedOutput = true;

        // Recurrent network with bias vectors and recurrent linkage on each non-input layer. Limited to one hidden layer only.
        public SimpleRecurrentNetwork(int numInputs, int numHidden, int numOutput, float[] weights = null, bool gatedOutput = true)
        {
            NumInput = numInputs;
            NumOutput = numOutput;
            NumHidden = numHidden;
            GatedOutput = gatedOutput;
            TotalWeights = (numHidden + numOutput) * (numHidden + numOutput + numInputs) + (numHidden + numOutput);
            InputMatrix = new Matrix<float>(1, numHidden + numOutput + numInputs + 1); //Input from this iteration plus hidden and output signal from last iteration and bias.
            OutputMatrix = new Matrix<float>(1, numHidden + numOutput);
            if (weights == null)
            {
                Weights = new float[TotalWeights];
                DateTime timeSeed = DateTime.Now;
                Random rnd = new Random(timeSeed.Millisecond + timeSeed.Second * 1000);
                for (int i = 0; i < TotalWeights; i++)
                {
                    Weights[i] = (float)(rnd.NextDouble() - 0.5) / 10; //-0.1 to 0.1.
                }
            }
            else
            {
                Debug.Assert(weights.Length == TotalWeights, "SetWeights: Invalid weight length. Please use GetNumberOfWeights to get the proper length of the weights.");
                Weights = weights;
                //int index = 0;
                //Array.Copy(weights, index, InputWeights, 0, InputWeights.Length);
                //index += InputWeights.Length;
                //Array.Copy(weights, index, HiddenWeights, 0, HiddenWeights.Length);
                //index += HiddenWeights.Length;
                //Array.Copy(weights, index, OutputWeights, 0, OutputWeights.Length);
            }
            unsafe
            {
                fixed (float* ptr = Weights)
                {
                    IntPtr intPtr = new IntPtr((void*)ptr);
                    WeightMatrix = new Matrix<float>(NumInput + NumHidden + NumOutput + 1, NumHidden + NumOutput, 1, intPtr, 0); //Again,input consist of Input from this iteration plus hidden and output signal from last iteration and bias.
                }
            }
            Reset();
        }
        public static UInt32 GetNumberOfWeights(int numInputs, int numHidden, int numOutput)
        {
            //Again,input consist of Input from this iteration plus hidden and output signal from last iteration and bias.
            return (UInt32)((numHidden + numOutput) * (numHidden + numOutput + numInputs) + (numHidden + numOutput));
        }
        public UInt32 GetNumberOfWeights()
        {
            return (UInt32)TotalWeights;
        }
        public float[] Propagate(float[] input)
        {
            float[] result = new float[NumOutput];
            if (WeightMatrix != null)
            {
                Debug.Assert(input.Length == NumInput, "Propagate: Invalid input length.");
                //Build matrices.
                int index = 0;
                for (int i = 0; i < input.Length; i++)
                    InputMatrix[0, i + index] = input[i];
                index += input.Length;
                for (int i = 0; i < OutputMatrix.Cols - 1; i++)
                    InputMatrix[0, i + index] = OutputMatrix[0, i];
                InputMatrix[0, OutputMatrix.Cols - 1] = 1f; //For calculation of bias vector.
                OutputMatrix = InputMatrix * WeightMatrix;
                // Apply tanh formula.
                for (int i = 0; i < OutputMatrix.Cols; i++)
                {
                    if (GatedOutput)
                    {
                        float alpha = OutputMatrix[0, i];
                        OutputMatrix[0, i] = (float)Math.Tanh(alpha);
                    }
                }
                for (int i = 0; i < NumOutput; i++)
                    result[i] = OutputMatrix[0, i + NumHidden];
            }
            return result;
        }
        public float[] GetWeights()
        {
            return Weights;
        }
        public void Reset()
        {
            OutputMatrix.SetZero();
        }
        public void BackPropagate(float[] input, float[] output)
        {
            throw new NotImplementedException();
        }
        public float[] GetError(float[] input, float[] output)
        {
            throw new NotImplementedException();
        }

        private void ClearRecurrentState(ref float[] values)
        {
            values.Initialize();
        }

        ~SimpleRecurrentNetwork()
        {
            if (WeightMatrix != null)
                WeightMatrix.Dispose();
            if (InputMatrix != null)
                InputMatrix.Dispose();
            if (OutputMatrix != null)
                OutputMatrix.Dispose();
        }
    }
}
