﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public class SimplePropagator : IPropagatorFloat
    {
        protected char[] InputMap;
        protected char[] OutputMap;
        protected UInt32 NumInput;
        protected UInt32 NumOutput;
        protected int[] NumHidden;
        protected bool Recurrent;
        protected bool GatedOutput;
        protected float[] InputData = null;
        protected float[] OutputData = null;
        protected float[,] InputRange = null;
        protected float[,] OutputRange = null;
        protected INeuralNetwork<float> Network;

        public UInt32 GetNumberOfInputs()
        {
            return NumInput;
        }
        public UInt32 GetNumberOfOutputs()
        {
            return NumOutput;
        }
        public UInt32 GetStateSize()
        {
            return Network.GetNumberOfWeights();
        }
        public ValueTypes GetValueType()
        {
            return ValueTypes.FLOAT;
        }
        public char[] GetInputMapping()
        {
            return InputMap;
        }
        public char[] GetOutputMapping()
        {
            return OutputMap;
        }
        public byte[] GetInputRange()
        {
            byte[] result = new byte[NumInput * sizeof(float) * 2];
            int index = 0;
            float[,] ranges = GetInputRangeF();
            for (int i = 0; i < ranges.GetLength(0); i++ )
            {
                BitConverter.GetBytes(ranges[i, 0]).CopyTo(result, index * sizeof(float));
                index++;
                BitConverter.GetBytes(ranges[i, 1]).CopyTo(result, index * sizeof(float));
                index++;
            }
            return result;
        }
        public byte[] GetOutputRange()
        {
            byte[] result = new byte[NumInput * sizeof(float) * 2];
            int index = 0;
            float[,] ranges = GetOutputRangeF();
            for (int i = 0; i < ranges.GetLength(0); i++)
            {
                BitConverter.GetBytes(ranges[i, 0]).CopyTo(result, index * sizeof(float));
                index++;
                BitConverter.GetBytes(ranges[i, 1]).CopyTo(result, index * sizeof(float));
                index++;
            }
            return result;
        }
        public float[,] GetInputRangeF()
        {
            return InputRange;
        }
        public float[,] GetOutputRangeF()
        {
            return OutputRange;
        }
        public virtual void SetStateF(float[] state)
        {
            Network = new SimpleFeedForwardNetwork((int)NumInput, NumHidden, (int)NumOutput, state, GatedOutput, 0f);
        }
        public virtual void SetState(byte[] state)
        {
            float[] floatState = new float[state.Length / sizeof(float)];
            Debug.Assert(floatState.Length == Network.GetNumberOfWeights(), "SetState: state array has wrong lenght.");
            for (int i = 0; i < floatState.Length; i++)
                floatState[i] = BitConverter.ToSingle(state, i * sizeof(float));
            SetStateF(floatState);
        }
        public virtual void ResetState()
        {
            //Restart with initial state.
            Network = new SimpleFeedForwardNetwork((int)NumInput, NumHidden, (int)NumOutput, null, GatedOutput);
        }
        protected float[] Input(byte[] input)
        {
            Debug.Assert(input.Length / sizeof(float) == NumInput, "Input: input byte array are of wrong length.");
            if (InputData == null)
                InputData = new float[NumInput];
            for (int i = 0; i < NumInput; i++)
                InputData[i] = LinearTransformValueWithClamp(new float[]{InputRange[i, 0], InputRange[i, 1]}, BitConverter.ToSingle(input, i * sizeof(float)), false);
            return InputData;
        }
        protected float[] InputByIndex(byte[] input, UInt32 index)
        {
            Debug.Assert(index < NumInput, "Input: input index outside of input length.");
            Debug.Assert(input.Length <= sizeof(float), "Input: input was the wrong size.");
            if (InputData == null)
                InputData = new float[NumInput];
            InputData[index] = LinearTransformValueWithClamp(new float[]{InputRange[index, 0], InputRange[index, 1]}, BitConverter.ToSingle(input, 0), false);
            return InputData;
        }
        public float[] PropagateF(float[] input)
        {
            return Network.Propagate(input);
        }
        public byte[] Propagate(byte[] input)
        {
            byte[] result = new byte[NumOutput * sizeof(float)];
            float[] resultF = Network.Propagate(Input(input));
            for (int i = 0; i < NumOutput; i++)
            {
                BitConverter.GetBytes(LinearTransformValueWithClamp(new float[]{OutputRange[i, 0], OutputRange[i, 1]}, resultF[i], true)).CopyTo(result, i * sizeof(float));
            }
            return result;
        }
        private float LinearTransformValue(float[] range, float value, bool reverse)
        {
            float mean = range.Average();
            float scaling = Math.Abs(range[0] - mean);
            if (reverse)
            {
                return value * scaling + mean;
            }
            else
            {
                return (value - mean) / scaling;
            }
        }
        public float LinearTransformValueWithClamp(float[] range, float value, bool reverse)
        {
            float result = LinearTransformValue(range, value, reverse);
            if (range[0] > range[1])
            {
                result = Math.Max(Math.Min(range[0], result), range[1]);
            }
            else
            {
                result = Math.Max(Math.Min(range[1], result), range[0]);
            }
            return result;
        }
        public SimplePropagator(char[] inputs, char[] outputs, int[] numberOfHiddenNeurons, bool recurrent, byte[] initialState = null, bool gatedOutput = false, float[,] inputRange = null, float[,] outputRange = null, float learningRate = 0.1f)
        {
            InputMap = (char[])inputs.Clone();
            OutputMap = (char[])outputs.Clone();
            NumInput = (UInt32)new string(InputMap).Split(new char[]{','}).Length;
            NumOutput = (UInt32)new string(OutputMap).Split(new char[]{','}).Length;
            NumHidden = numberOfHiddenNeurons;
            Recurrent = recurrent;
            GatedOutput = gatedOutput;
            Debug.Assert(inputRange == null || (inputRange.GetLength(0) == NumInput && inputRange.GetLength(1) == 2), "InputRange array has wrong length.");
            Debug.Assert(outputRange == null || (outputRange.GetLength(0) == NumOutput && outputRange.GetLength(1) == 2), "OutputRange array has wrong length.");
            for (int i = 0; i < inputRange.GetLength(0); i++)
                Debug.Assert(inputRange[i,0] != inputRange[i,1], "InputRange array has a range error.");
            for (int i = 0; i < outputRange.GetLength(0); i++)
                Debug.Assert(outputRange[i, 0] != outputRange[i, 1], "OutputRange array has a range error.");

                if (inputRange == null)
                {
                    InputRange = new float[NumInput, 2];
                    for (int i = 0; i < NumInput; i++)
                    {
                        InputRange[i, 0] = -1f;
                        InputRange[i, 1] = 1f;
                    }
                }
                else
                {
                    InputRange = inputRange;
                }
            if (outputRange == null)
            {
                OutputRange = new float[NumOutput, 2];
                for (int i = 0; i < NumOutput; i++)
                {
                    OutputRange[i, 0] = -1f;
                    OutputRange[i, 1] = 1f;
                }
            }
            else
            {
                OutputRange = outputRange;
            }
            //Setup Neural Network
            if (!Recurrent)
            {

                if (initialState != null)
                {
                    float[] state = new float[initialState.Length / sizeof(float)];
                    for (int i = 0; i < state.Length; i++)
                        state[i] = BitConverter.ToSingle(initialState, i * sizeof(float));
                    Network = new SimpleFeedForwardNetwork((int)NumInput, NumHidden, (int)NumOutput, state, GatedOutput, learningRate);
                }
                else
                {
                    Network = new SimpleFeedForwardNetwork((int)NumInput, NumHidden, (int)NumOutput, null, GatedOutput, learningRate);
                }
            }
            else
            {
                   Network = new SimpleRecurrentNetwork((int)NumInput, NumHidden[0], (int)NumOutput, null, GatedOutput);
            }

        }
    }
}
