﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Emgu.CV.Structure;
using Emgu.CV;

namespace Kinect_reinforcement.ReinforcementLearner
{
    // Simple feed forward network with at least one hidden layer and tanh gates on the hidden, output neurons and bias vectors on each non-input layer. 
    class SimpleFeedForwardNetwork : INeuralNetwork<float>
    {
        int NumInput, NumOutput, TotalWeights;
        int[] NumHidden;
        float LearningRate;
        bool GatedOutput;
        Matrix<float>[] WeightMatrices;
        Matrix<float>[] ResultsCache; //Cache matrices to store propagation result.
        Matrix<float>[] ActivationCache; //Cache matrices to store propagation result.
        Matrix<float>[] ErrorCache; //Cache matrices to store error results for backpropagation.
        Matrix<float>[] Bias; // Vectors of bias matricies for non-input layers.

        public float[] Propagate(float[] input)
        {
            Debug.Assert(input.Length == NumInput, "Propagate: Invalid input length.");
            if (ResultsCache == null)
            {
                ResultsCache = new Matrix<float>[WeightMatrices.Length];
                ActivationCache = new Matrix<float>[WeightMatrices.Length];
                for (int i = 0; i < ResultsCache.Length; i++)
                {
                    ResultsCache[i] = new Matrix<float>(1, WeightMatrices[i].Cols);
                    ActivationCache[i] = new Matrix<float>(1, WeightMatrices[i].Cols);
                }
            }
            using (Matrix<float> init = new Matrix<float>(input))
                for (int i = 0; i < WeightMatrices.Length; i++)
                {
                    if (i == 0)
                    {
                        ResultsCache[i] = init.Transpose().Mul(WeightMatrices[i]).Add(Bias[i]);
                    }
                    else
                    {
                        ResultsCache[i] = ActivationCache[i - 1].Mul(WeightMatrices[i]).Add(Bias[i]);
                    }
                    //Output neurons.
                    if (!GatedOutput && i == WeightMatrices.Length - 1)
                    {
                        for (int j = 0; j < ResultsCache[i].Cols; j++)
                            ActivationCache[i][0, j] = ResultsCache[i][0, j];
                    }
                    else
                    {
                        for (int j = 0; j < ResultsCache[i].Cols; j++)
                            ActivationCache[i][0, j] = (float)Math.Tanh(ResultsCache[i][0, j]);
                    }
                }
            float[] result = new float[NumOutput];
            for (int i = 0; i < result.Length; i++)
                result[i] = ActivationCache[ResultsCache.Length - 1][0, i];
            return result;
        }
        public float[] GetWeights()
        {
            float[] result = new float[TotalWeights];
            //Return the weight of each weight matrices, one row at a time.
            int weightIndex = 0;
            // Fill in each weight matrix, one row at a time.
            for (int i = 0; i < WeightMatrices.Length; i++)
                for (int j = 0; j < WeightMatrices[i].Rows; j++)
                    for (int k = 0; k < WeightMatrices[i].Cols; k++)
                    {
                        result[weightIndex] = WeightMatrices[i][j, k];
                        weightIndex++;
                    }
            //Fill in bias.
            for (int i = 0; i < Bias.Length; i++)
                for (int j = 0; j < Bias[i].Cols; j++)
                {
                    result[weightIndex] = Bias[i][0, j];
                    weightIndex++;
                }
            return result;
        }
        public UInt32 GetNumberOfWeights()
        {
            return (UInt32)TotalWeights;
        }
        public float[] GetError(float[] input, float[] output)
        {
            Debug.Assert(NumOutput == output.Length, "SimpleFeedForwardNetwork:GetError - Output has the wrong length.");
            Debug.Assert(NumInput == input.Length, "SimpleFeedForwardNetwork:GetError - Input has the wrong length.");
            float[] networkOutput = Propagate(input);
            for (int i = 0; i < networkOutput.Length; i++)
                networkOutput[i] = networkOutput[i] - output[i];
            return networkOutput;
        }
        public void BackPropagate(float[] input, float[] output)
        {
            Debug.Assert(NumOutput == output.Length, "SimpleFeedForwardNetwork:BackPropagate - Output has the wrong length.");
            Debug.Assert(NumInput == input.Length, "SimpleFeedForwardNetwork:BackPropagate - Input has the wrong length.");
            if (ErrorCache == null)
            {
                // Use diagonal matrices for added convenience.
                ErrorCache = new Matrix<float>[WeightMatrices.Length];
                for (int i = 0; i < WeightMatrices.Length; i++)
                    ErrorCache[i] = new Matrix<float>(WeightMatrices[i].Cols, WeightMatrices[i].Cols);
            }
            float[] networkOutput = Propagate(input);
            // Calculate 1-tanh(output)^2 for output results, store diagonized in the errorcache. If output is not gated. Just 1.0.
            if (GatedOutput)
            {
                for (int j = 0; j < ResultsCache[ResultsCache.Length - 1].Cols; j++)
                        ErrorCache[ResultsCache.Length - 1][j, j] = (float)(1 - Math.Pow(Math.Tanh(ResultsCache[ResultsCache.Length - 1][0, j]), 2));
            }
            else
            {
                for (int j = 0; j < ResultsCache[ResultsCache.Length - 1].Cols; j++)
                    ErrorCache[ResultsCache.Length - 1][j, j] = 1;
            }
            // Calculate 1-tanh(output)^2 for all results, store diagonized in the errorcache.
            for (int i = 0; i < ResultsCache.Length - 1; i++)
                for (int j = 0; j < ResultsCache[i].Cols; j++)
                    ErrorCache[i][j, j] = (float)(1 - Math.Pow(Math.Tanh(ResultsCache[i][0, j]), 2));
            // Calculate error at output neurons.
            for (int i = 0; i < NumOutput; i++)
                ErrorCache[ErrorCache.Length - 1][i, i] *= (networkOutput[i] - output[i]);
            // Calculate error at hidden neurons.
            for (int i = 0; i < ErrorCache.Length - 1; i++)
                using (Matrix<float> tempVec = new Matrix<float>(1, ErrorCache[ErrorCache.Length - 2 - i].Rows))
                {
                    ErrorCache[ErrorCache.Length - 1 - i].Mul(WeightMatrices[WeightMatrices.Length - 1 - i].Transpose())
                        .Mul(ErrorCache[ErrorCache.Length - 2 - i])
                        .Reduce(tempVec, Emgu.CV.CvEnum.ReduceDimension.SingleRow, Emgu.CV.CvEnum.ReduceType.ReduceSum);
                    for (int j = 0; j < tempVec.Cols; j++)
                        ErrorCache[ErrorCache.Length - 2 - i][j, j] = tempVec[0, j];
                    WeightMatrices[WeightMatrices.Length - 1 - i] = WeightMatrices[WeightMatrices.Length - 1 - i]
                        .Sub(ActivationCache[ErrorCache.Length - 2 - i].Transpose().Mul(ErrorCache[ErrorCache.Length - 1 - i].GetDiag()
                        .Transpose().Mul(LearningRate)));
                    Bias[Bias.Length - i - 1] = Bias[Bias.Length - i - 1].Sub(ErrorCache[Bias.Length - i - 1].GetDiag().Transpose().Mul(LearningRate));
                }
            //Update weights for the input to hidden weights.
            using (Matrix<float> inVector = new Matrix<float>(input))
            {
                WeightMatrices[0] = WeightMatrices[0]
                    .Sub(inVector.Mul(ErrorCache[0].GetDiag().Transpose().Mul(LearningRate)));
                Bias[0] = Bias[0].Sub(ErrorCache[0].GetDiag().Transpose().Mul(LearningRate));
            }
        }
        public void Reset()
        {
            throw new NotSupportedException("SimpleFeedForwardNetwork has no internal temporal state to reset.");
        }
        public static UInt32 GetNumberOfWeights(int numInputs, int[] numHidden, int numOutput)
        {
            Debug.Assert(numInputs > 0 && numOutput > 0, "SimpleFeedForwardNetwork.GetNumberOfWeights: There must be one or more input or output neurons.");
            Debug.Assert(numHidden.Length > 0, "SimpleFeedForwardNetwork.GetNumberOfWeights: There must be at least one layer of hidden neurons.");
            for (int i = 0; i < numHidden.Length; i++)
            {
                Debug.Assert(numHidden[i] > 0, "SimpleFeedForwardNetwork.GetNumberOfWeights: Each hidden layer must contain at least one neuron.");
            }
            int result = 0;
            // Initialize weight matrices. Calculate total weights.
            result = numInputs * numHidden[0];
            for (int i = 0; i < numHidden.Length - 1; i++)
            {
                result += numHidden[i] * numHidden[i + 1]; //Weights 
                result += numHidden[i]; //plus bias.
            }
            result += numHidden[numHidden.Length - 1] * numOutput; //Weights 
            result += numHidden[numHidden.Length - 1] + numOutput;//plus bias.

            return (UInt32)result;
        }
        public SimpleFeedForwardNetwork(int numInputs, int[] numHidden, int numOutput, float[] weights = null, bool gatedOutput = false, float learningRate = 0.1f)
        {
            Debug.Assert(learningRate > 0 && learningRate <= 1, "Learning rate must be within range (0, 1].");
            Debug.Assert(numInputs > 0 && numOutput > 0, "SimpleFeedForwardNetwork: There must be one or more input or output neurons.");
            Debug.Assert(numHidden.Length > 0, "SimpleFeedForwardNetwork: There must be at least one layer of hidden neurons.");
            for (int i = 0; i < numHidden.Length; i++)
            {
                Debug.Assert(numHidden[i] > 0, "SimpleFeedForwardNetwork: Each hidden layer must contain at least one neuron.");
            }
            NumInput = numInputs;
            NumOutput = numOutput;
            NumHidden = numHidden;
            LearningRate = learningRate;
            GatedOutput = gatedOutput;
            // Initialize weight matrices. Calculate total weights.
            WeightMatrices = new Matrix<float>[NumHidden.Length + 1];
            WeightMatrices[0] = new Matrix<float>(NumInput, NumHidden[0]);
            TotalWeights = NumInput * NumHidden[0];
            Bias = new Matrix<float>[NumHidden.Length + 1];
            for (int i = 0; i < NumHidden.Length - 1; i++)
            {
                WeightMatrices[i + 1] = new Matrix<float>(NumHidden[i], NumHidden[i + 1]);
                Bias[i] = new Matrix<float>(1, NumHidden[i]); // Bias array starts in the first hidden layer.
                TotalWeights += NumHidden[i] * NumHidden[i + 1]; //Weights
                TotalWeights += NumHidden[i];// plus bias.
            }
            WeightMatrices[NumHidden.Length] = new Matrix<float>(NumHidden[NumHidden.Length - 1], NumOutput);
            Bias[NumHidden.Length - 1] = new Matrix<float>(1, NumHidden[NumHidden.Length - 1]); //Bias last hidden layer.
            Bias[NumHidden.Length] = new Matrix<float>(1, NumOutput); //Bias for output.
            TotalWeights += NumHidden[NumHidden.Length - 1] * NumOutput; //Weights 
            TotalWeights += NumHidden[NumHidden.Length - 1] + NumOutput;//plus bias.

            //Randomly generate weights if no weights are given.
            if (weights == null)
            {
                //Make random initial state.
                DateTime timeSeed = DateTime.Now;
                Random rnd = new Random(timeSeed.Millisecond + timeSeed.Second * 1000);
                for (int i = 0; i < WeightMatrices.Length; i++)
                    for (int j = 0; j < WeightMatrices[i].Rows; j++)
                        for(int k = 0; k <WeightMatrices[i].Cols; k++)
                            WeightMatrices[i][j, k] = (float)(rnd.NextDouble() - 0.5); //-0.5 to 0.5
                //Random generate bias.
                for (int i = 0; i < Bias.Length; i++)
                    for (int j = 0; j < Bias[i].Cols; j++)
                        Bias[i][0, j] = (float)(rnd.NextDouble() - 0.5); //-0.5 to 0.5
            }
            else
            {
                Debug.Assert(weights.Length == TotalWeights, "SimpleFeedForwardNetwork: The length of given weights does not equal to the total wieght of the network.");
                int weightIndex = 0;
                // Fill in each weight matrix, one row at a time.
                for (int i = 0; i < WeightMatrices.Length; i++)
                    for (int j = 0; j < WeightMatrices[i].Rows; j++)
                        for (int k = 0; k < WeightMatrices[i].Cols; k++)
                        {
                            WeightMatrices[i][j, k] = weights[weightIndex];
                            weightIndex++;
                        }
                //Fill in bias.
                for (int i = 0; i < Bias.Length; i++)
                    for (int j = 0; j < Bias[i].Cols; j++)
                    {
                        Bias[i][0, j] = weights[weightIndex];
                        weightIndex++;
                    }
            }
        }
        ~SimpleFeedForwardNetwork()
        {
            //Dispose all weight matrices.
            for (int i = 0; i < WeightMatrices.Length; i++)
            {
                if (WeightMatrices[i] != null)
                    WeightMatrices[i].Dispose();
            }
            //Dispose all cache matrices.
            if (ResultsCache != null)
            {
                for (int i = 0; i < ResultsCache.Length; i++)
                {
                    if (ResultsCache[i] != null)
                        ResultsCache[i].Dispose();
                }

            }
            //Dispose all cache matrices.
            if (ActivationCache != null)
            {
                for (int i = 0; i < ActivationCache.Length; i++)
                {
                    if (ActivationCache[i] != null)
                        ActivationCache[i].Dispose();
                }

            }
            if (ErrorCache != null)
            {
                for (int i = 0; i < ErrorCache.Length; i++)
                {
                    if (ErrorCache[i] != null)
                        ErrorCache[i].Dispose();
                }

            }
            if (Bias != null)
            {
                for (int i = 0; i < Bias.Length; i++)
                {
                    if (Bias[i] != null)
                        Bias[i].Dispose();
                }

            }
        }

    }
}
