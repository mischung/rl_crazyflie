﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV;
using CMAESWrapper;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public class CMANeuroES : SimplePropagator, IReinforcementLearningAlgorithm
    {
        private int NumTrials;
        private int NumRepeats;
        private int RepeatIndex = 0;
        private float SigmaMin;
        private float SigmaZero;
        private float[,] Populations;
        private int PopulationIndex;
        private float[,] Performance = null;
        private byte[] LastOutput = null;
        private float[] ThetaInitial = null;
        private float[] HighestPerformanceState = null;
        private float HighestPerformance;
        private int Generation = 0;
        private CMA_ES Wrapper = null;
              
        /*
        private Matrix<float> M;
        private Matrix<float> PSigma, PC;
        private float Sigma;
        private Matrix<float> Mut;
        private Matrix<float> C;
        private Matrix<float> RndNorm; //Matrix of random numbers to generate offsprings.
        private Matrix<float> InvSqrtC;
        private Matrix<float> B;
        private Matrix<float> D;
        
        private int Mu; // number of parents/points for recombination
        private float MuEff; // variance-effectiveness of sum w_i x_i
        private Matrix<float> Weights; //Weigth for recombination.
        private float Cc; //time constant for cumulation for C
        private float CSigma; //t-const for cumulation for sigma control
        private float CConv; //learning rate for rank-one update of C
        private float Damps; //damping for sigma 
        private float ChiN;
        */


        new public byte[] Propagate(byte[] reward)
        {
            Debug.Assert(reward.Length == sizeof(float), "CMANNeuroES.Propagate only accepts a floating point reward value as input.");
            byte[] result = new byte[NumOutput * sizeof(float)];
            float[] resultF = Network.Propagate(InputData);
            for (int i = 0; i < NumOutput; i++)
            {
                BitConverter.GetBytes(LinearTransformValueWithClamp(new float[] { OutputRange[i, 0], OutputRange[i, 1] }, resultF[i], true)).CopyTo(result, i * sizeof(float));
            }
            LastOutput = result;
            Performance[PopulationIndex, RepeatIndex] += BitConverter.ToSingle(reward, 0);
            return LastOutput;
        }
        public byte[] ResetEpisode(byte[] reward)
        {
            try
            {
                Performance[PopulationIndex, RepeatIndex] += BitConverter.ToSingle(reward, 0);
                EvaluateNext();
            }
            catch (Exception ex)
            {
                Debug.Write("ResetEpisode: error resetting episode." + ex.Message);
            }
            return LastOutput;
        }
        public byte[] GetState()
        {
            // return the highest performance state so far...
            if (HighestPerformanceState == null)
            {
                HighestPerformanceState = new float[Populations.GetLength(1)];
                for (int i = 0; i < Populations.GetLength(1); i++)
                    HighestPerformanceState[i] = Populations[0, i];
                HighestPerformance = GetMeanPerformance(0);
            }
            byte[] result = new byte[Populations.GetLength(1) * sizeof(float)];
            for (int i = 0; i < Populations.GetLength(1); i++)
            {
                BitConverter.GetBytes(HighestPerformanceState[i]).CopyTo(result, i * sizeof(float));
            }
            return result;
        }
        new public void SetState(byte[] state)
        {
            //Sets the initial thetaInitial.
            if (ThetaInitial == null)
                ThetaInitial = new float[state.Length / sizeof(float)];
            Debug.Assert(state.Length / sizeof(float) == Network.GetNumberOfWeights(), "CMANeuroES.SetState: state array has wrong lenght.");
            for (int i = 0; i < ThetaInitial.Length; i++)
            {
                ThetaInitial[i] = BitConverter.ToSingle(state, i * sizeof(float));
            }
        }
        new public void ResetState()
        {
            //Restart with initial state.
            HighestPerformanceState = null;
            Generation = 0;
            PopulationIndex = 0;
            RepeatIndex = 0;
            Performance = new float[NumTrials, NumRepeats];
            MakeOffsprings();
            LoadNetwork(PopulationIndex, Recurrent);

        }
        new public void Input(byte[] input)
        {
            Debug.Assert(input.Length / sizeof(float) == NumInput, "Input: input byte array are of wrong length.");
            if (InputData == null)
                InputData = new float[NumInput];
            for (int i = 0; i < NumInput; i++)
                InputData[i] = LinearTransformValueWithClamp(new float[] { InputRange[i, 0], InputRange[i, 1] }, BitConverter.ToSingle(input, i * sizeof(float)), false);
        }
        new public void InputByIndex(byte[] input, UInt32 index)
        {
            Debug.Assert(index < NumInput, "Input: input index outside of input length.");
            Debug.Assert(input.Length <= sizeof(float), "Input: input was the wrong size.");
            if (InputData == null)
                InputData = new float[NumInput];
            InputData[index] = LinearTransformValueWithClamp(new float[] { InputRange[index, 0], InputRange[index, 1] }, BitConverter.ToSingle(input, 0), false);

        }

        public CMANeuroES(char[] inputs, char[] outputs, int numberOfHiddenNeurons, bool recurrent, float sigmaZero, float sigmaMin,  int numberOfTrials = 0, int numRepeats = 3, byte[] initialState = null, float[,] inputRange = null, float[,] outputRange = null, bool gatedOutput = true)
            : base(inputs, outputs, new int[]{numberOfHiddenNeurons}, recurrent, initialState, gatedOutput, inputRange, outputRange, 0f)
        {
            Wrapper = new CMA_ES();
            SigmaZero = sigmaZero;
            SigmaMin = sigmaMin;
            NumRepeats = numRepeats;
            //Setup Neural Network
            if (Recurrent)
            {
                if (initialState != null)
                {
                    SetState(initialState);
                }
                else
                {
                    //Make random initial state.
                    DateTime timeSeed = DateTime.Now;
                    Random rnd = new Random(timeSeed.Millisecond + timeSeed.Second * 1000);
                    if (ThetaInitial == null)
                        ThetaInitial = new float[(int)SimpleRecurrentNetwork.GetNumberOfWeights((int)NumInput, (int)NumHidden[0], (int)NumOutput)];
                    for (int i = 0; i < (int)SimpleRecurrentNetwork.GetNumberOfWeights((int)NumInput, (int)NumHidden[0], (int)NumOutput); i++)
                    {
                        ThetaInitial[i] = (float)(rnd.NextDouble() - 0.5) ; //-0.5 to 0.5.
                    }
                }
            }
            else
            {
                throw new NotImplementedException("Feed forward network is not yet implemented for CMA NeuroES.");
            }
            if (numberOfTrials == 0)
            {
                NumTrials = (int) Math.Max((4 + Math.Floor(3 * Math.Log(ThetaInitial.Length))), 5);
            }
            else
            {
                NumTrials = numberOfTrials;
            }
            Performance = new float[NumTrials, NumRepeats];


            /*
            C = new Matrix<float>(ThetaInitial.Length, ThetaInitial.Length);
            B = new Matrix<float>(ThetaInitial.Length, ThetaInitial.Length);
            D = new Matrix<float>(ThetaInitial.Length, ThetaInitial.Length);
            InvSqrtC = new Matrix<float>(ThetaInitial.Length, ThetaInitial.Length);
            PC = new Matrix<float>(1, ThetaInitial.Length);
            PSigma = new Matrix<float>(1, ThetaInitial.Length);
            Mu = (int)Math.Floor(NumTrials / 2.0);
            Mut = new Matrix<float>(Mu, ThetaInitial.Length);
            Weights = new Matrix<float>(1, Mu);
            for (int i = 0; i < Weights.Cols; i++)
                Weights[0,i] = (float)( Math.Log(Mu + 1) - Math.Log(i + 1));
            Weights = Weights.Mul(1 / Weights.Sum);
            for (int i = 0; i < Weights.Cols; i++)
                MuEff += (float)Math.Pow(Weights[0, i], 2);
            MuEff = (float)1.0 / MuEff;
            Cc = (float)4.0 / (ThetaInitial.Length + 4);  
            CSigma = (MuEff + 2) / (ThetaInitial.Length + MuEff + 3);
            CConv = (float)(2 / (Math.Pow(ThetaInitial.Length + Math.Sqrt(2), 2)));
            Damps = (float)(1 + 2 * Math.Max(0, Math.Sqrt((MuEff - 1) / (ThetaInitial.Length + 1))) + CSigma);
            ChiN = (float)(Math.Sqrt(ThetaInitial.Length) * (1 - 1 / (4 * ThetaInitial.Length) + 1 / (21 * Math.Pow(ThetaInitial.Length, 2)))); 
            PopulationIndex = 0;
            Populations = new float[NumTrials, ThetaInitial.Length];
            if (RndNorm == null)
                RndNorm = new Matrix<float>(10000, 10000); 
            CvInvoke.Randn(RndNorm, new MCvScalar(0), new MCvScalar(1)); 
            */


            float[] stdev = new float[ThetaInitial.Length];
            for (int i = 0; i < stdev.Length; i++)
                stdev[i] = sigmaZero;
            Wrapper.Init(ThetaInitial.Length, NumTrials, 1e-10f, 1e6f, ThetaInitial, stdev);
            ResetState();

        }
        private float GetMeanPerformance(int index)
        {
            float result = 0f;
            for (int i = 0; i < NumRepeats; i++)
                result += Performance[index, i];
            return result / NumRepeats;
        }
        private void EvaluateNext()
        {
            if (HighestPerformanceState == null && RepeatIndex == NumRepeats - 1)
            {
                HighestPerformanceState = new float[Populations.GetLength(1)];
                for (int i = 0; i < Populations.GetLength(1); i++)
                    HighestPerformanceState[i] = Populations[PopulationIndex, i];
                HighestPerformance = GetMeanPerformance(PopulationIndex);
            }
            else
            {
                if (GetMeanPerformance(PopulationIndex) > HighestPerformance && RepeatIndex == NumRepeats - 1)
                {
                    for (int j = 0; j < Populations.GetLength(1); j++)
                        HighestPerformanceState[j] = Populations[PopulationIndex, j];
                    HighestPerformance = GetMeanPerformance(PopulationIndex);
                }
            }
            // Check if PopulationIndex is at the last one. If so we recalculate the other variables and make more offspirings.
            if (PopulationIndex == NumTrials - 1 && RepeatIndex == NumRepeats - 1)
            {
                PopulationIndex = 0;
                RepeatIndex = 0;
                Generation++;
                MakeOffsprings();
                Performance = new float[NumTrials, NumRepeats];
                LoadNetwork(PopulationIndex, Recurrent);
            }
            else
            {
                //Else we evaluate more candidates.
                if (RepeatIndex == NumRepeats - 1)
                {
                    RepeatIndex = 0;
                    PopulationIndex++;
                }
                else
                {
                    RepeatIndex++;
                }
                LoadNetwork(PopulationIndex, Recurrent);
            }
        }
        private void MakeOffsprings()
        {
            if (Generation == 0)
            {
                /*
                if (M != null)
                     M.Dispose();
                 M = new Matrix<float>(ThetaInitial);
                 M = M.Transpose();
                 PSigma.SetZero();
                 PC.SetZero();
                 Sigma = SigmaZero;
                 C.SetIdentity();
                 D.SetIdentity();
                 B.SetIdentity();
                 InvSqrtC.SetIdentity();
                 //CvInvoke.Eigen(C, D, B);
                 */

                Populations = Wrapper.SamplePopulation();
            }
            else
            {
                float[] averages = new float[NumTrials];
                for (int i = 0; i < Performance.GetLength(0); i++)
                    averages[i] = GetMeanPerformance(i);
                int[] keys = new int[Populations.GetLength(0)];
                for (int i = 0; i < keys.Length; i++)
                    keys[i] = i;
                // Sort in descending order.
                Array.Sort(averages, keys);
                Array.Reverse(keys);
                Double average = averages.Average();
                Debug.WriteLine("Generation: " + Generation + ", Average: " + average + ", Stdev:" + Math.Sqrt(averages.Select(val => (val - average) * (val - average)).Sum() / averages.Length) + ", Max:" + averages[averages.Length - 1]
                    + ", Min:" + averages[0]);

                
                /*
                using (Matrix<float> oldM = new Matrix<float>(1, ThetaInitial.Length))
                {
                    M.CopyTo(oldM);
                    M.SetZero();
                    for (int i = 0; i < Mu; i++)
                        for (int j = 0; j < ThetaInitial.Length; j++)
                        {
                            Mut[i, j] = Populations[keys[i], j];
                            M[0, j] += Weights[0, i] * Mut[i, j];
                        }
                    using (Matrix<float> invD = new Matrix<float>(D.Rows, D.Cols, 1))
                    {
                        CvInvoke.Invert(D, invD, Emgu.CV.CvEnum.DecompMethod.LU);
                        B.Mul(invD).Mul(B.Transpose()).CopyTo(InvSqrtC);
                    }
                    PSigma = PSigma.Mul(1 - CSigma).Add(InvSqrtC.Mul(M.Sub(oldM).Mul(1 / Sigma).Transpose()).Transpose().Mul((Math.Sqrt(CSigma * (2 - CSigma) * MuEff))));
                    PC = PC.Mul(1 - Cc).Add(M.Sub(oldM).Mul(1 / Sigma)).Mul(Math.Sqrt(Cc * (2 - Cc) * MuEff));
                    // Assumes MuConv = MuEff.
                    using (Matrix<float> rankMuUpdate = new Matrix<float>(C.Rows, C.Cols))
                    {
                        rankMuUpdate.SetZero();
                        for (int i = 0; i < Mu; i++)
                        using (Matrix<float> z = new Matrix<float>(1, ThetaInitial.Length))
                        {
                            Mut.GetRow(i).Sub(oldM).Mul(1 / Sigma).CopyTo(z);
                            rankMuUpdate.Add(z.Transpose().Mul(z).Mul(Weights[0, i])).CopyTo(rankMuUpdate);
                        }
                        C = C.Mul(1 - CConv).Add(PC.Transpose().Mul(PC).Mul(1 / MuEff).Add(rankMuUpdate.Mul(1 - 1 / MuEff)).Mul(CConv));
                        using (Matrix<float> W = new Matrix<float>(B.Rows, 1))
                        using (Matrix<float> Vt = new Matrix<float>(B.Rows, B.Cols))
                        {
                            CvInvoke.SVDecomp(C, W, B, Vt, Emgu.CV.CvEnum.SvdFlag.Default);
                            for (int i = 0; i < W.Rows; i++)
                                D[i, i] = W[i, 0];
                        }
                    }
                    Sigma = (float)(Sigma * Math.Exp(CSigma / Damps * (PSigma.Norm / ChiN - 1)));
                }
            }
            Sigma = Math.Max(Sigma, SigmaMin);
            // Generate the off springs according to M, Sigma and C.
            DateTime timeSeed = DateTime.Now;
            Random rnd = new Random((int)timeSeed.Ticks);
            using (Matrix<float> StdDev = new Matrix<float>(C.Rows, 1))
            {
                CvInvoke.Sqrt(D.GetDiag(), StdDev);
                for (int i = 0; i < NumTrials; i++)
                {
                    using (Matrix<float> sig = new Matrix<float>(C.Rows, 1))
                    {
                        B.Mul(StdDev).CopyTo(sig);

                        for (int j = 0; j < ThetaInitial.Length; j++)
                        {
                            Populations[i, j] = M[0, j] + Sigma * sig[j, 0] * RndNorm[(int)Math.Floor(rnd.NextDouble() * (RndNorm.Rows - 1)), (int)Math.Floor(rnd.NextDouble()  * (RndNorm.Cols - 1))];
                        }
                    }
                }
            */
                float[] fitness = new float[averages.Length];
                for (int i = 0; i < fitness.Length; i++)
                    fitness[i] = 1f / averages[i];
                Wrapper.UpdateDistribution(fitness);
                if (!Wrapper.TestForTermination())
                {
                    Populations = Wrapper.SamplePopulation();
                }
                else
                {
                    throw new Exception("CMA-ES terminated.");
                }

            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        private void LoadNetwork(int index, bool recurrent)
        {
            Network = null;
            if (recurrent)
            {
                float[] result = new float[Populations.GetLength(1)];
                for (int i = 0; i < Populations.GetLength(1); i++)
                    result[i] = Populations[index, i];
                Network = new SimpleRecurrentNetwork((int)NumInput, NumHidden[0], (int)NumOutput, result);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        ~CMANeuroES()
        {
            /*
            if (C != null)
                C.Dispose();
            if (InvSqrtC != null)
                InvSqrtC.Dispose();
            if (M != null)
                M.Dispose();
            if (PSigma != null)
                PSigma.Dispose();
            if (PC != null)
                PC.Dispose();
            if (RndNorm != null)
                RndNorm.Dispose();
            if (B != null)
                B.Dispose();
            if (D != null)
                D.Dispose();
            if (Mut != null)
                Mut.Dispose();
            if (Weights != null)
                Weights.Dispose();
             */
        }
    }
}
