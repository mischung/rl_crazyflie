﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public class DummyLearningAlgorithm : IReinforcementLearningAlgorithm
    {
        byte[] m = new byte[4];

        public UInt32 GetNumberOfInputs()
        {
            return 4;
        }
        public UInt32 GetNumberOfOutputs()
        {
            return 4;
        }
        public UInt32 GetStateSize()
        {
            return 0;
        }
        public byte[] GetInputRange()
        {
            throw new NotImplementedException();
        }
        public byte[] GetOutputRange()
        {
            throw new NotImplementedException();
        }
        public ValueTypes GetValueType()
        {
            return ValueTypes.BYTE;
        }
        public char[] GetInputMapping()
        {
            return "M1,M2,M3,M4".ToCharArray();
        }
        public char[] GetOutputMapping()
        {
            return "MOTOR1,MOTOR2,MOTOR3,MOTOR4".ToCharArray();
        }
        public void Input(byte[] input)
        {
            m[0] = input[0];
            m[1] = input[1];
            m[2] = input[2];
            m[3] = input[3];
        }
        public void InputByIndex(byte[] input, UInt32 index)
        {
            m[index] = input[0];
        }
        public byte[] Propagate(byte[] reward)
        {
           /* if (reward[0] > 0)
            {
                m[0] = reward[0];
                m[1] = reward[0];
                m[2] = reward[0];
                m[3] = reward[0];
            }*/
            return m;
        }
        public byte[] ResetEpisode(byte[] reward)
        {
            m[0] = reward[0];
            m[1] = reward[0];
            m[2] = reward[0];
            m[3] = reward[0];
            return m;
        }
        public byte[] GetState()
        {
            return null;
        }
        public void SetState(byte[] state)
        {

        }
        public void ResetState()
        {

        }
    }
}
