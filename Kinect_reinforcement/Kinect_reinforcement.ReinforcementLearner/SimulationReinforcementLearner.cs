﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.ReinforcementLearner
{
    public class SimulationReinforcementLearner<al> : OffSiteReinforcementLearner<al> where al : IReinforcementLearningAlgorithm
    {

        public SimulationReinforcementLearner(al algorithm) : base(algorithm, false, 0f)
        {
        }
        protected override void PreInitiationTasks()
        {
        }
        protected override void PostPropagateTasks()
        {
            //Block task here.
        }
        protected override void PostResetTasks()
        {
        }
        ~SimulationReinforcementLearner()
        {
        }

    }
}
