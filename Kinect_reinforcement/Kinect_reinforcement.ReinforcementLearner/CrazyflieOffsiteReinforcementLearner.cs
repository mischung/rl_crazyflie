﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Kinect_reinforcement.Connection;
using System.Reflection;
using System.Diagnostics;

namespace Kinect_reinforcement.ReinforcementLearner
{ 
    public class CrazyflieOffsiteReinforcementLearner<al> : OffSiteReinforcementLearner<al> where al : IReinforcementLearningAlgorithm
    {
        protected RadioManager RadioConnection;
        protected IList<PropertyInfo> lowLevelCommanderDataBundleProperties = null;
        protected Dictionary<string, float> lowLevelCommanderDataBundleData = new Dictionary<string, float>();

        public CrazyflieOffsiteReinforcementLearner(al algorithm, RadioManager radio) : base(algorithm)
        {
            RadioConnection = radio;
            RadioConnection.radioDisconnected += radioConnection_radioDisconnected;
            RadioConnection.radioError += radioConnection_radioError;
            RadioConnection.radioReceived += radioConnection_radioReceived;
        }
        protected void radioConnection_radioError(object sender, EventArgs e)
        {
            if (StartLearning)
            {
                ResetLearning();
            }
        }
        protected void radioConnection_radioDisconnected(object sender, EventArgs e)
        {
            if (StartLearning)
            {
                ResetLearning();
            }
        }
        protected void radioConnection_radioReceived(object sender, EventArgs e)
        {
            LowLevelCommanderDataBundle data = ((radioReceivedEventArgs<LowLevelCommanderDataBundle>)e).Data;
            bool success = ((radioReceivedEventArgs<LowLevelCommanderDataBundle>)e).Success;
            if (success && StartLearning)
            {
                lowLevelCommanderDataBundleProperties = typeof(LowLevelCommanderDataBundle).GetProperties();
                foreach (var property in lowLevelCommanderDataBundleProperties)
                {
                    lowLevelCommanderDataBundleData[property.Name.ToUpperInvariant()] = Convert.ToSingle(property.GetValue(data));
                }
                Input(new Dictionary<string, float>(lowLevelCommanderDataBundleData));
            }
        }
        protected override void PreInitiationTasks()
        {
            //Block so that learningStart event does not trigger before radio connection.
            if (!RadioConnection.IsConnected) 
                RadioConnection.Connect();
        }
        protected override void PostPropagateTasks()
        {
            byte motor1 = Convert.ToByte(Math.Max(0, Math.Min(255, AlgorithmOutputStates["MOTOR1"])));
            byte motor2 = Convert.ToByte(Math.Max(0, Math.Min(255, AlgorithmOutputStates["MOTOR2"])));
            byte motor3 = Convert.ToByte(Math.Max(0, Math.Min(255, AlgorithmOutputStates["MOTOR3"])));
            byte motor4 = Convert.ToByte(Math.Max(0, Math.Min(255, AlgorithmOutputStates["MOTOR4"])));
            RadioConnection.TransmitAsync(new LowLevelCommanderCommand(motor1, motor2, motor3, motor4), true);
        }
        protected override void PostResetTasks()
        {
            try
            {
                Task.Run(() => {
                    RadioConnection.TransmitAsync(new LowLevelCommanderCommand(0, 0, 0, 0), false);
                    Thread.Sleep(100);
                    RadioConnection.TransmitAsync(new LowLevelCommanderCommand(0, 0, 0, 0), false);
                });
            }
            catch (Exception ex)
            {
                Debug.Write("PostPropagateTasks: error shutting down crazyflie motors!." + ex.Message);
            }
        }
        ~CrazyflieOffsiteReinforcementLearner()
        {
            RadioConnection.Disconnect();
        }
    }
}
