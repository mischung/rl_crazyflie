﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Kinect_reinforcement.ReinforcementLearner
{
    // Class to implement K-fold cross validation using a neural network.
    public class KFoldCrossValidationNN : SimplePropagator, ISupervisedLearningAlgorithm
    {
        private float LearningRate;
        private float K;

        protected void InputAndOutput(byte[] data, out float[] input, out float[] output)
        {
            Debug.Assert(data.Length / sizeof(float) == NumInput + NumOutput, "InputAndOutput: data byte array are of wrong length.");
            if (InputData == null)
                InputData = new float[NumInput];
            if (OutputData == null)
                OutputData = new float[NumOutput];
            int dataIndex = 0;
            for (int i = 0; i < NumInput; i++)
            {
                InputData[i] = LinearTransformValueWithClamp(new float[] { InputRange[i, 0], InputRange[i, 1] }, BitConverter.ToSingle(data, dataIndex * sizeof(float)), false);
                dataIndex++;
            }
            for (int i = 0; i < NumOutput; i++)
            {
                OutputData[i] = LinearTransformValueWithClamp(new float[] { OutputRange[i, 0], OutputRange[i, 1] }, BitConverter.ToSingle(data, dataIndex * sizeof(float)), false); 
                dataIndex++;
            }
            input = InputData;
            output = OutputData;
        }
        public byte[][] BatchLearn(byte[][] data)
        {
            // Randomly choose learning and testing sets.
            int numSamples = data.GetLength(0);
            int numK = (int)Math.Ceiling(numSamples / K);
            int[] keys = new int[numSamples];
            for (int i = 0; i < keys.Length; i++)
                keys[i] = i;
            //Make random initial state.
            DateTime timeSeed = DateTime.Now;
            Random rnd = new Random(timeSeed.Millisecond + timeSeed.Second * 1000);
            int[] randomKeys = keys.OrderBy(x => rnd.Next()).ToArray();
            float[,] errors = new float[numK, NumOutput];
            // Learn all sets..
            for (int i = 0; i < numSamples - numK; i++)
            {
                    Learn(data[randomKeys[i]]);
            }
            // Test some sets.
            for (int i = 0; i < numK; i++)
            {
                float[] input = null;
                float[] output = null;
                InputAndOutput(data[randomKeys[numSamples - i - 1]], out input, out output);
                float[] networkOutput = Network.Propagate(input);
                for (int j = 0; j < networkOutput.Length; j++)
                    errors[i, j] = output[j] - networkOutput[j];
            }
            byte[][] result = new byte[numK][];
            for (int i = 0; i < result.GetLength(0); i++)
            {
                result[i] = new byte[NumOutput * sizeof(float)];
                for (int j = 0; j < errors.GetLength(1); j++)
                {
                    BitConverter.GetBytes(LinearTransformValueWithClamp(new float[] { OutputRange[i, 0], OutputRange[i, 1] }, errors[i, j], true)).CopyTo(result[i], j * sizeof(float));
                }
            }
            return result;
        }
        public byte[] BatchLearnGetQuadraticError(byte[][] data, bool learn = true)
        {
            // Randomly choose learning and testing sets.
            int numSamples = data.GetLength(0);
            int numK = (int)Math.Ceiling(numSamples / K);
            int[] keys = new int[numSamples];
            for (int i = 0; i < keys.Length; i++)
                keys[i] = i;
            //Make random initial state.
            DateTime timeSeed = DateTime.Now;
            Random rnd = new Random(timeSeed.Millisecond + timeSeed.Second * 1000);
            int[] randomKeys = keys.OrderBy(x => rnd.Next()).ToArray();
            // Learn all sets..
            for (int i = 0; i < numSamples - numK; i++)
            {
                if (learn == true)
                    Learn(data[randomKeys[i]]);
            }
            float sqrError = 0;
            // Test some sets.
            for (int i = 0; i < numK; i++)
            {
                float[] input = null;
                float[] output = null;
                InputAndOutput(data[randomKeys[numSamples - i - 1]], out input, out output);
                float[] error = Network.GetError(input, output);
                for (int j = 0; j < error.Length; j++)
                    sqrError += (float)Math.Pow(error[j], 2);
            }
            sqrError = sqrError / numK;
            byte[] result = new byte[sizeof(float)];
            BitConverter.GetBytes(sqrError).CopyTo(result, 0);
            return result;
        }
        public void Learn(byte[] data)
        {
            float[] output = null;
            float[] input = null;
            InputAndOutput(data, out input, out output);
            Network.BackPropagate(input, output);
        }
        public byte[] GetState()
        {
                float[] weights = new float[Network.GetNumberOfWeights()];
                weights = Network.GetWeights();
                byte[] result = new byte[Network.GetNumberOfWeights() * sizeof(float)];
                for (int i = 0; i < Network.GetNumberOfWeights(); i++)
            {
                BitConverter.GetBytes(weights[i]).CopyTo(result, i * sizeof(float));
            }
            return result;
        }
        public override void ResetState()
        {
            //Restart with initial state.
            Network = new SimpleFeedForwardNetwork((int)NumInput, NumHidden, (int)NumOutput, null, GatedOutput, LearningRate);
        }
        public override void SetState(byte[] state)
        {
            float[] floatState = new float[state.Length / sizeof(float)];
            Debug.Assert(floatState.Length == Network.GetNumberOfWeights(), "SetState: state array has wrong lenght.");
            for (int i = 0; i < floatState.Length; i++)
                floatState[i] = BitConverter.ToSingle(state, i * sizeof(float));
            Network = new SimpleFeedForwardNetwork((int)NumInput, NumHidden, (int)NumOutput, floatState, GatedOutput, LearningRate);
        }
        public KFoldCrossValidationNN(char[] inputs, char[] outputs, int[] numberOfHiddenNeurons, bool recurrent, float learningRate, float k = 5.0f, byte[] initialState = null, bool gatedOutput = false, float[,] inputRange = null, float[,] outputRange = null)
            : base (inputs, outputs, numberOfHiddenNeurons, recurrent, initialState, gatedOutput, inputRange, outputRange, learningRate)
        {
            LearningRate = learningRate;
            K = k;
            Debug.Assert(K > 1, "KFoldCrossValidationNN: k needs to be at least bigger than 1.");
            //Setup Neural Network
            if (Recurrent)
            {
                throw new NotImplementedException("Recurrent network is not yet implemented for KFoldCrossValidationNN.");
            }
        }

    }
}
