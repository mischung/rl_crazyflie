﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Kinect_reinforcement.Feature_Tracking;
using Kinect_reinforcement.ReinforcementLearner;
using System.Threading.Tasks.Schedulers;


namespace Kinect_reinforcement.Commander
{
    public class CrazyflieLearningCommander : ILearningCommander
    {
        public event CommanderLearningResetHandler learningReset;
        public event CommanderLearningStartHandler learningStart;
        public event CommanderLearningResetHandler learningError;

        public bool _isLearning = false;
        public bool IsLearning
        {
            get
            {
                return _isLearning;
            }
            protected set
            {
                _isLearning = value;
                if (_isLearning)
                {
                    if (learningStart != null)
                        learningStart(this, null);
                }
                else
                {
                    if (learningReset != null)
                        learningReset(this, null);
                }
            }
        }
        private const double LockingTimeout = 10000;
        private IReinforcementLearner Learner;
        private LuaScriptRules Rules;
        private FeatureTracker Tracker;
        private float FeatureDetectorThreshold;
        private FeatureTracker.Trajectory LockedTrajectory = null;
        private static SemaphoreSlim Semaphore;
        private Dictionary<string, float> CrazyflieOutputs;
        private Dictionary<string, float> LearnerInputs;
        private QueuedTaskScheduler qts;
        Feature_Tracking.FeatureTracker.Entity LatestLockedEntity;
        public CrazyflieLearningCommander()
        {
            IsLearning = false;
            qts = new QueuedTaskScheduler(TaskScheduler.Default, maxConcurrencyLevel:2);
          
        }
        public void CreateLearningSession(FeatureTracker tracker, IRewardRules rules, IReinforcementLearner learner)
        {
            CreateLearningSession(tracker, (LuaScriptRules)rules, learner);
        }
        public void CreateLearningSession(FeatureTracker tracker, LuaScriptRules rules, IReinforcementLearner learner, float featureDetectorThreshold = 100)
        {

            CloseLearningSession();
            Learner = learner;
            Tracker = tracker;
            Rules = rules;
            FeatureDetectorThreshold = featureDetectorThreshold;
            Learner.learningStart += Learner_learningStart;
            Learner.learningReset += Learner_learningReset;
            Tracker.imageFrameUpdated += Tracker_imageFrameUpdated;
        }

        public async void InitializeLearning(Dictionary<string, float> parameters, float featureDetectorThreshold = 100)
        {
            try
            {
                FeatureDetectorThreshold = featureDetectorThreshold;
                Semaphore = new SemaphoreSlim(0, 1);
                // Initialize tracker.
                Tracker.TrackFeatures(new FeatureTracker.CrazyFlieConfiguratuion1Features(FeatureDetectorThreshold), new FeatureTracker.CrazyFlieConfiguration1());
                Tracker.trajectoriesUpdated += Tracker_trajectoriesUpdated;
                await Task.Run(() => LockTrajectory(300));
                Task.Factory.StartNew(() =>
                {
                    Learner.InitializeLearning(parameters);
                }, CancellationToken.None, TaskCreationOptions.None, qts);
                Rules.rewardIssued += Rules_rewardIssued;
                Rules.rulesReset += Rules_rulesReset;
                Rules.scriptError += Rules_scriptError;
            }
            catch (Exception e)
            {
                Debug.Write("Error initializing learning: ", e.Message);
                if (learningError != null)
                    learningError(this, null);
                Tracker.PauseTracking();
                Tracker.ResetTracking();
            }
            finally
            {
                Tracker.trajectoriesUpdated -= Tracker_trajectoriesUpdated;
            }
        }

        void Rules_scriptError(object sender, RewardEventArgs e)
        {
            if (learningError != null)
                learningError(this, null);
        }
        public void SaveLearningSession(string session)
        {
            throw new NotImplementedException();
        }
        public void LoadLearningSession(string session, out IRewardRules rules, out IReinforcementLearner learner)
        {
            rules = null;
            learner = null;
        }
        public void ResetEpisode(float reward)
        {
            try
            {
                IsLearning = false;
                LockedTrajectory.trajectoryUpdated -= LockedTrajectory_trajectoryUpdated;
                LockedTrajectory = null;
                LatestLockedEntity = null;
                Rules.Reset();
                Rules.rewardIssued -= Rules_rewardIssued;
                Rules.rulesReset -= Rules_rulesReset;
                Rules.scriptError -= Rules_scriptError;
                if (Learner != null)
                    Learner.ResetLearning(reward);
                if (Tracker != null)
                {
                    // DeInitialize tracker.
                    Tracker.PauseTracking();
                    Tracker.ResetTracking();
                }
            }
            catch (Exception ex)
            {
                Debug.Write("Error resetting episode" + ex.Message);
            }
        }
        void CloseLearningSession()
        {
            if (Tracker != null)
                Tracker.imageFrameUpdated -= Tracker_imageFrameUpdated;
            if (Learner != null)
            {
                Learner.learningStart -= Learner_learningStart;
                Learner.learningReset -= Learner_learningReset;
            }
            Learner = null;
            Tracker = null;
            Rules = null;
        }
        // Lock on the first stable trajectory. lockTime in ms.
        void LockTrajectory(long lockTime)
        {
            Stopwatch timer = Stopwatch.StartNew();
            while (LockedTrajectory == null)
            {
                if (timer.ElapsedMilliseconds > LockingTimeout)
                    throw new Exception("Trajectory lock timed out: Crazyflie not found in visual range.");
                Semaphore.Wait(1000);
                List<FeatureTracker.Trajectory> trajectories = Tracker.GetTrajectories();
                foreach (var trajectory in trajectories)
                {
                    int duration = Math.Abs(trajectory.Entities[0].Time.Subtract(trajectory.Entities[trajectory.Entities.Count - 1].Time).Milliseconds);
                    Debug.Write("Duration in trajectories: " + duration);
                    if (duration >= lockTime)
                    {
                        LockedTrajectory = trajectory;
                        LockedTrajectory.trajectoryUpdated += LockedTrajectory_trajectoryUpdated;
                    }
                }
            }
        }

        void LockedTrajectory_trajectoryUpdated(object sender, FeatureTracker.TrajectoryUpdatedEventArgs e)
        {
            if (IsLearning)
                LatestLockedEntity = e.LatestState;
        }
        void Tracker_trajectoriesUpdated(object sender, EventArgs e)
        {
            Semaphore.Release();
        }
        void Tracker_imageFrameUpdated(object sender, EventArgs e)
        {
            if (IsLearning && LatestLockedEntity != null)
            {
                //Propagate rules.
                try
                {
                    CrazyflieOutputs = Learner.GetOutput();
                    LearnerInputs = Learner.GetInput();
                    List<Tuple<string, dynamic>> data = new List<Tuple<string, dynamic>>();
                    foreach (var crazyflieOutput in CrazyflieOutputs)
                    {
                        data.Add(new Tuple<string, dynamic>(crazyflieOutput.Key, crazyflieOutput.Value));
                    }
                    foreach (var learnerInput in LearnerInputs)
                    {
                        data.Add(new Tuple<string, dynamic>(learnerInput.Key, learnerInput.Value));
                    }
                    // Get trajectory data from camera.
                    //Feature_Tracking.FeatureTracker.Entity latestEntity = LockedTrajectory.GetLatestEntity();
                    data.Add(new Tuple<string, dynamic>("POSX", LatestLockedEntity.Position.X));
                    data.Add(new Tuple<string, dynamic>("POSY", LatestLockedEntity.Position.Y));
                    data.Add(new Tuple<string, dynamic>("POSZ", LatestLockedEntity.Position.Z));
                    data.Add(new Tuple<string, dynamic>("FORX", LatestLockedEntity.Forward.X));
                    data.Add(new Tuple<string, dynamic>("FORY", LatestLockedEntity.Forward.Y));
                    data.Add(new Tuple<string, dynamic>("FORZ", LatestLockedEntity.Forward.Z));
                    data.Add(new Tuple<string, dynamic>("UPX", LatestLockedEntity.Up.X));
                    data.Add(new Tuple<string, dynamic>("UPY", LatestLockedEntity.Up.Y));
                    data.Add(new Tuple<string, dynamic>("UPZ", LatestLockedEntity.Up.Z));
                    data.Add(new Tuple<string, dynamic>("FTIME", LatestLockedEntity.Time.TotalSeconds));
                    Rules.PropagateRules(data.ToArray());
                }
                catch (Exception ex)
                {
                    ResetEpisode(0);
                    Debug.Write("Error propagating rules: ", ex.Message);
                }
            }
        }
        void Learner_learningStart(object sender, LearningEventArgs e)
        {
            IsLearning = true;
        }
        void Learner_learningReset(object sender, LearningEventArgs e)
        {
            ResetEpisode(0);
        }
        void Rules_rewardIssued(object sender, RewardEventArgs e)
        {
            Learner.Reward(e.Reward);
        }
        void Rules_rulesReset(object sender, RewardEventArgs e)
        {
            if (IsLearning)
            {
                if (learningReset != null)
                    learningReset(this, null);
                ResetEpisode(e.Reward);
            }
        }
        public void Terminate()
        {
            ResetEpisode(0);
            CloseLearningSession();

        }

    }
}
