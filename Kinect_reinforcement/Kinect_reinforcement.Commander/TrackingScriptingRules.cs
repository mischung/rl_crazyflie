﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Emgu.CV;
using Emgu.CV.Structure;
namespace Kinect_reinforcement.Commander
{
    // Basic Lua scripting API with function for kalman filtering added.
    public class TrackingScriptingRules : LuaScriptRules
    {
        // Linear Kalman filter with 3D position, velocity, acceleration euler orietation, angular rate and acceleration.
        KalmanFilter Filter = new KalmanFilter(18, 6, 0);
        //Measurement.
        Matrix<float> Zk = new Matrix<float>(6, 1);
        //State.
        Matrix<float> Xk = new Matrix<float>(18, 1);
        // Transition error.
        Matrix<float> Wk = new Matrix<float>(18, 1);
        //Transition matrix.
        Matrix<float> F = new Matrix<float>(18, 18);
        //Measurement matrix.
        Matrix<float> H = new Matrix<float>(6, 18);
        // Time difference.
        float DeltaT = 1.0f / 30.0f; //30HZ default.
        public struct TrackingResult
        {
            public double X;
            public double Y;
            public double Z;
            public double VX;
            public double VY;
            public double VZ;
            public double AX;
            public double AY;
            public double AZ;
            public double PITCH;
            public double ROLL;
            public double YAW;
            public double PITCHRATE;
            public double ROLLRATE;
            public double YAWRATE;
            public double PITCHACC;
            public double ROLLACC;
            public double YAWACC;
        }
        public struct Coord
        {
            public double X, Y, Z;
        }
        //Direction consine matrix.
        Matrix<float> DCM = new Matrix<float>(3, 3);
        bool StatePreIsSet = false;
        float SigmaA = 0.015f;
        float SigmaM = 0.05f;
        private Matrix<float> RndNorm; //Matrix of random numbers to generate offsprings.

        public TrackingScriptingRules() : base()
        {
            // Generate random values
            if (RndNorm == null)
                RndNorm = new Matrix<float>(10000, 10000);
            CvInvoke.Randn(RndNorm, new MCvScalar(0), new MCvScalar(1));
            ResetKalmanFilter();
            rulesReset += TrackingScriptingRules_rulesReset;
        }

        void TrackingScriptingRules_rulesReset(object sender, RewardEventArgs e)
        {
            ResetKalmanFilter();
        }

        ~TrackingScriptingRules()
        {
            Zk.Dispose();
            Xk.Dispose();
            F.Dispose();
            H.Dispose();
            F.Dispose();
            Wk.Dispose();
            DCM.Dispose();
            RndNorm.Dispose();
        }
        public override void InitializeAPI()
        {
                base.InitializeAPI();
                Lua.KalmanPositionAndOrientationTracking = new Func<double, double, double, double, double, double, double, double, double, TrackingResult>((x, y, z, roll, pitch, yaw, deltaT, sigmaA, sigmaM) =>
                {
                    TrackingResult result = new TrackingResult();
                    try
                    {
                        //Update deltaT.
                        DeltaT = (float)deltaT;
                        SigmaM = (float)sigmaM;
                        SigmaA = (float)sigmaA;
                        //Update transition matrix.
                        float halfDeltaTSquare = 0.5f * (float)Math.Pow(DeltaT, 2);
                        F.Data[0, 3] = DeltaT;
                        F.Data[0, 6] = halfDeltaTSquare;
                        F.Data[1, 4] = DeltaT;
                        F.Data[1, 7] = halfDeltaTSquare;
                        F.Data[2, 5] = DeltaT;
                        F.Data[2, 8] = halfDeltaTSquare;
                        F.Data[3, 6] = DeltaT;
                        F.Data[4, 7] = DeltaT;
                        F.Data[5, 8] = DeltaT;
                        F.Data[9, 12] = DeltaT;
                        F.Data[9, 15] = halfDeltaTSquare;
                        F.Data[10, 13] = DeltaT;
                        F.Data[10, 16] = halfDeltaTSquare;
                        F.Data[11, 14] = DeltaT;
                        F.Data[11, 17] = halfDeltaTSquare;
                        F.Data[12, 15] = DeltaT;
                        F.Data[13, 16] = DeltaT;
                        F.Data[14, 17] = DeltaT;
                        Filter.TransitionMatrix.SetTo(F.Data);
                        //Update measurement.
                        Zk[0,0] = (float)x;
                        Zk[1,0] = (float)y;
                        Zk[2,0] = (float)z;
                        Zk[3,0] = (float)roll;
                        Zk[4,0] = (float)pitch;
                        Zk[5,0] = (float)yaw;
                        if (!StatePreIsSet)
                        {
                            Xk.Data[0, 0] = (float)x;
                            Xk.Data[1, 0] = (float)y;
                            Xk.Data[2, 0] = (float)z;
                            Xk.Data[9, 0] = (float)roll;
                            Xk.Data[10, 0] = (float)pitch;
                            Xk.Data[11, 0] = (float)yaw;
                            Filter.StatePre.SetTo(Xk.Data);
                            StatePreIsSet = true;
                        }
                        //Update transition error.
                        Wk[0, 0] = halfDeltaTSquare;
                        Wk[1, 0] = halfDeltaTSquare;
                        Wk[2, 0] = halfDeltaTSquare;
                        Wk[3, 0] = DeltaT;
                        Wk[4, 0] = DeltaT;
                        Wk[5, 0] = DeltaT;
                        Wk[9, 0] = halfDeltaTSquare;
                        Wk[10, 0] = halfDeltaTSquare;
                        Wk[11, 0] = halfDeltaTSquare;
                        Wk[12, 0] = DeltaT;
                        Wk[13, 0] = DeltaT;
                        Wk[14, 0] = DeltaT;
                        // Update covariant matricies.
                        CvInvoke.SetIdentity(Filter.ProcessNoiseCov, new MCvScalar(Math.Pow(SigmaA, 2)));
                        CvInvoke.SetIdentity(Filter.MeasurementNoiseCov, new MCvScalar(Math.Pow(SigmaM, 2)));
                        DateTime timeSeed = DateTime.Now;
                        Random rnd = new Random((int)timeSeed.Ticks);
                        using (Mat predicted = Filter.Predict())
                        using (Matrix<float> accError = new Matrix<float>(Filter.StatePre.Rows, 1))
                        {
                            for (int i = 0; i < Filter.StatePre.Rows; i++)
                                accError[i, 0] = SigmaA * RndNorm[(int)Math.Floor(rnd.NextDouble() * (RndNorm.Rows - 1)), (int)Math.Floor(rnd.NextDouble() * (RndNorm.Cols - 1))];
                            // Add process noise to account for changing velocity and acceleration.
                            CvInvoke.Multiply(accError, Wk, accError, 1, Emgu.CV.CvEnum.DepthType.Cv32F);
                            CvInvoke.Add(accError, predicted, Filter.StatePre);
                        }
                        using (Mat estimated = Filter.Correct(Zk.Mat))
                        {
                            float[,] resultArray = new float[18,1];
                            estimated.CopyTo(resultArray);
                            result.X = resultArray[0, 0];
                            result.Y = resultArray[1, 0];
                            result.Z = resultArray[2, 0];
                            result.VX = resultArray[3, 0];
                            result.VY = resultArray[4, 0];
                            result.VZ = resultArray[5, 0];
                            result.AX = resultArray[6, 0];
                            result.AY = resultArray[7, 0];
                            result.AZ = resultArray[8, 0];
                            result.ROLL = resultArray[9, 0];
                            result.PITCH = resultArray[10, 0];
                            result.YAW = resultArray[11, 0];
                            result.ROLLRATE = resultArray[12, 0];
                            result.PITCHRATE = resultArray[13, 0];
                            result.YAWRATE = resultArray[14, 0];
                            result.ROLLACC = resultArray[15, 0];
                            result.PITCHACC = resultArray[16, 0];
                            result.YAWACC = resultArray[17, 0];
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Debug.Write("Error using Kalman filter." + ex.Message);
                        Lua.Log(ex.Message);
                        Lua.Restart(0);
                        return result;
                    }

                });
                Lua.ResetKalmanFilter = new Action(() =>
                {
                    try
                    {
                        ResetKalmanFilter();
                    }
                    catch (Exception ex)
                    {
                        Debug.Write("Error reseting Kalman filter." + ex.Message);
                        Lua.Log(ex.Message);
                        Lua.Restart(0);
                    }

                });
                Lua.DirectionCosineTransform = new Func<double, double, double, double, double, double, Coord>((x, y, z, roll, pitch, yaw) =>
                {
                    Coord result = new Coord();
                    try
                    {
                        using (Matrix<float> inCoord = new Matrix<float>(3, 1))
                        {
                            inCoord[0, 0] = (float)x;
                            inCoord[1, 0] = (float)y;
                            inCoord[2, 0] = (float)z;
                            double rollRad = roll * Math.PI / 180;
                            double pitchRad = -pitch * Math.PI / 180;
                            double yawRad = yaw * Math.PI / 180;
                            // Update direction cosine matrix from euler angles.
                            DCM[0, 0] = (float)(Math.Cos(pitchRad) * Math.Cos(yawRad));
                            DCM[0, 1] = (float)(Math.Cos(pitchRad) * Math.Sin(yawRad));
                            DCM[0, 2] = (float)-Math.Sin(pitchRad);
                            DCM[1, 0] = (float)(Math.Sin(rollRad) * Math.Sin(pitchRad) * Math.Cos(yawRad) - Math.Cos(rollRad) * Math.Sin(yawRad));
                            DCM[1, 1] = (float)(Math.Sin(rollRad) * Math.Sin(pitchRad) * Math.Sin(yawRad) + Math.Cos(rollRad) * Math.Cos(yawRad));
                            DCM[1, 2] = (float)(Math.Sin(rollRad) * Math.Cos(pitchRad));
                            DCM[2, 0] = (float)(Math.Cos(rollRad) * Math.Sin(pitchRad) * Math.Cos(yawRad) + Math.Sin(rollRad) * Math.Sin(yawRad));
                            DCM[2, 1] = (float)(Math.Cos(rollRad) * Math.Sin(pitchRad) * Math.Sin(yawRad) - Math.Sin(rollRad) * Math.Cos(yawRad));
                            DCM[2, 2] = (float)(Math.Cos(rollRad) * Math.Cos(pitchRad));
                            using (Matrix<float> OutCoord = DCM.Mul(inCoord))
                            {
                                result.X = OutCoord[0, 0];
                                result.Y = OutCoord[1, 0];
                                result.Z = OutCoord[2, 0];
                            }
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Debug.Write("Error using direction cosine transform." + ex.Message);
                        Lua.Log(ex.Message);
                        Lua.Restart(0);
                        return result;
                    }

                });

        }
        private void ResetKalmanFilter()
        {
            Filter = new KalmanFilter(18, 6, 0);
            F.Data = new float[,] {{1f, 0f, 0f, DeltaT, 0f, 0f, 0.5f * (float)Math.Pow(DeltaT, 2), 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0.5f * (float)Math.Pow(DeltaT, 2), 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0.5f * (float)Math.Pow(DeltaT, 2), 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0.5f * (float)Math.Pow(DeltaT, 2), 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0.5f * (float)Math.Pow(DeltaT, 2), 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f, 0.5f * (float)Math.Pow(DeltaT, 2)},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, DeltaT},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f}};
            H.Data = new float[,] {{1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                                    {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f}};
            Wk.Data = new float[,] {{0.5f * (float)Math.Pow(DeltaT, 2)}, {0.5f * (float)Math.Pow(DeltaT, 2)}, {0.5f * (float)Math.Pow(DeltaT, 2)}, {DeltaT}, {DeltaT}, {DeltaT}, {1}, {1}, {1}, 
                                    {0.5f * (float)Math.Pow(DeltaT, 2)}, {0.5f * (float)Math.Pow(DeltaT, 2)}, {0.5f * (float)Math.Pow(DeltaT, 2)}, {DeltaT}, {DeltaT}, {DeltaT}, {1}, {1}, {1}};
            CvInvoke.SetIdentity(Filter.ProcessNoiseCov, new MCvScalar(Math.Pow(SigmaA, 2)));
            CvInvoke.SetIdentity(Filter.MeasurementNoiseCov, new MCvScalar(Math.Pow(SigmaM, 2)));
            CvInvoke.SetIdentity(Filter.ErrorCovPost, new MCvScalar(1));
            StatePreIsSet = false;
            Filter.TransitionMatrix.SetTo(F.Data);
            Filter.MeasurementMatrix.SetTo(H.Data);
        }
    }
}
