﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.Commander
{
    public class RewardEventArgs : EventArgs
    {
        public float Reward;

        public RewardEventArgs(float reward)
        {
            Reward = reward;
        }
    }
}
