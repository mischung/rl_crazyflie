﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using DynamicLua;

namespace Kinect_reinforcement.Commander
{   
    public class LuaScriptRules : IRewardRules, ILuaScripting
    {
        const int MAXLOGSIZE = 10000; //Max number of characters Log can have. This reduces the load on the UI thread as Log is displayed in real time.
        public event RewardHandler rewardIssued;
        public event RewardHandler rulesReset;
        public event RewardHandler scriptError;
        protected dynamic Lua = null;
        string dummyScript = "function Propagate () Log('Please insert Propagate function') end";
        public List<string> _slog = new List<string>();
        public string Log { get; private set; }
        public string SLog 
        { 
            get 
            {
                StringBuilder builder = new StringBuilder();
	            foreach (string value in _slog)
	            {
                    builder.Append(value).AppendLine();
	            }
	            return builder.ToString();
            }
            private set
            {
                _slog.Add(value);
            }
        }
        public string Script { get; private set; }

        public LuaScriptRules()
        {
            try
            {
                Lua = new DynamicLua.DynamicLua();

                //Load dummy Propagate script.
                InitializeAPI();
                Lua(dummyScript);
            }
            catch (Exception ex)
            {
                Debug.Write("Error runing lua script." + ex.Message);
                Lua.Log(ex.Message);
                if (rulesReset != null)
                    rulesReset(this, new RewardEventArgs(0));
            }
        }
        public void ClearLog()
        {
            _slog.Clear();
            Log = "";
        }
        public bool LoadScript(string script)
        {
            try
            {
                Lua = new DynamicLua.DynamicLua();
                if (script == null)
                    script = dummyScript;
                Script = script;
                InitializeAPI();
                Lua(Script);
                return true;
            }
            catch (Exception ex)
            {
                Debug.Write("Error runing lua script." + ex.Message);
                if (rulesReset != null)
                    rulesReset(this, new RewardEventArgs(0));
                if (scriptError != null)
                    scriptError(this, new RewardEventArgs(0));
                Lua.Log(ex.Message);
                return false;
            }
        }
        public virtual void InitializeAPI()
        {
            Lua.Restart = new Action<double>((reward) => 
            { 
                if (rulesReset != null) 
                    rulesReset(this, new RewardEventArgs((float)reward)); 
            });
            Lua.Reward = new Action<double>((reward) => 
            { 
                if (rewardIssued != null) 
                    rewardIssued(this, new RewardEventArgs((float)reward)); 
            });
            Lua.SLog = new Action<string>((log) =>
            {
                SLog = log;
            });
            Lua.Log = new Action<string>((log) => 
            { 
                //Flush if log gets too big.
                if (Log.Length > MAXLOGSIZE)
                {
                    Log = log;
                }
                else
                {
                    Log += log;
                }
            });
        }
        public void PropagateRules(Tuple<string, dynamic>[] inputs)
        {
            try
            {
                foreach (var input in inputs)
                {
                    Lua[input.Item1] = input.Item2;
                }
                Lua.Propagate();
            }
            catch (Exception ex)
            {
                Debug.Write("Error runing lua script." + ex.Message);
                Lua.Log(ex.Message);
                if (rulesReset != null)
                    rulesReset(this, new RewardEventArgs(0));
            }
        }
        public void Reset()
        {
            //We load the Lua states and scripts again.
            LoadScript(Script);           
            if (rulesReset != null)
                rulesReset(this, new RewardEventArgs(0));
        }
    }
}
