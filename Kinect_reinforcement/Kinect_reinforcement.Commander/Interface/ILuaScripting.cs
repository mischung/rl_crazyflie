﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinect_reinforcement.Commander
{
    public interface ILuaScripting
    {
        event RewardHandler scriptError;

        string Log { get; }
        void ClearLog();
        bool LoadScript(string script); //Returns true if the script has no problems.
        void InitializeAPI();
        void Reset();
    }
}
