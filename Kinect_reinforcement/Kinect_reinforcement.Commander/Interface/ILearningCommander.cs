﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinect_reinforcement.ReinforcementLearner;
using Kinect_reinforcement.Feature_Tracking;


namespace Kinect_reinforcement.Commander
{
    public delegate void CommanderLearningResetHandler(object sender, EventArgs e);
    public delegate void CommanderLearningStartHandler(object sender, EventArgs e);
    public interface ILearningCommander
    {
        event CommanderLearningResetHandler learningReset;
        event CommanderLearningStartHandler learningStart;
        event CommanderLearningResetHandler learningError;
        bool IsLearning { get; }
        void CreateLearningSession(FeatureTracker tracker, IRewardRules rules, IReinforcementLearner learner);
        void InitializeLearning(Dictionary<string, float> parameters, float featureDetectorThreshold = 100);
        void SaveLearningSession(string session);
        void LoadLearningSession(string session, out IRewardRules rules, out IReinforcementLearner learner);
        void ResetEpisode(float reward);
        void Terminate(); //Termination of all learning.

// Feature_Tracking functions to use.
// List<Trajectory> GetTrajectories()
/*        public Coord3D[] Position
        {
            get
            {
                return Entities.Select(entity => entity.Position).ToArray();
            }
        }
        public Coord3D[] Up
        {
            get
            {
                return Entities.Select(entity => entity.Up).ToArray();
            }
        }
        public Coord3D[] Forward
        {
            get
            {
                return Entities.Select(entity => entity.Forward).ToArray();
            }
        }
        public TimeSpan[] Time
        {
            get
            {
                return Entities.Select(entity => entity.Time).ToArray();
            }
        }
*/
    }
}
