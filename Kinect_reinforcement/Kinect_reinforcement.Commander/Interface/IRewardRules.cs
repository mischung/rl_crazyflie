﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinect_reinforcement.ReinforcementLearner;

namespace Kinect_reinforcement.Commander
{
    public delegate void RewardHandler(object sender, RewardEventArgs e);

    public interface IRewardRules
    {
        event RewardHandler rewardIssued;
        event RewardHandler rulesReset;

        void PropagateRules(Tuple<string, dynamic>[] inputs);
    }
}
