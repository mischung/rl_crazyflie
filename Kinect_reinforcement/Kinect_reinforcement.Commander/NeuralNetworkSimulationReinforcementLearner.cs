﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Kinect_reinforcement.ReinforcementLearner;
using Kinect_reinforcement.Feature_Tracking;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Kinect_reinforcement.Commander
{
    public class NeuralNetworkSimulationReinforcementLearner : ILearningCommander
    {
        public event CommanderLearningResetHandler learningReset;
        public event CommanderLearningStartHandler learningStart;
        public event CommanderLearningResetHandler learningError;
        private bool _isLearning = false;
        private bool Stop = false;
        private float TrajectoryNoise = 1f;
        private IReinforcementLearner Learner;
        private IPropagatorFloat Simulator;
        private TrackingScriptingRules Rules;
        private Dictionary<string, float> InitialValues;
        private Dictionary<string, float> SimulatorValues;
        private Dictionary<string, float> LearnerInputs;
        private Dictionary<string, float> InitialLearnerValues;
        private static SemaphoreSlim SimulationTaskSemaphore = new SemaphoreSlim(0, 1);
        float RewardStopThreshold;
        float MaxSimulations;
        float NumSimulations = 0;
        float CurrentReward = 0;
        float DeltaTime;
        float TotalTime = 0;
        private Matrix<float> RndNorm; //Matrix of random numbers for trajectory noise.

        
        public bool IsLearning
        {
            get
            {
                return _isLearning;
            }
            protected set
            {
                _isLearning = value;
                if (_isLearning)
                {
                    if (learningStart != null)
                        learningStart(this, null);
                }
                else
                {
                    if (learningReset != null)
                        learningReset(this, null);
                }
            }
        }
        public void CreateLearningSession(IRewardRules rules, IReinforcementLearner learner)
        {
            CreateLearningSession(null, rules, learner);
        }
        public void CreateLearningSession(FeatureTracker tracker, IRewardRules rules, IReinforcementLearner learner)
        {
            // We won't need any feature tracking.
            Rules = (TrackingScriptingRules)rules;
            Learner = learner;
            Learner.learningStart += Learner_learningStart;
            Learner.learningReset += Learner_learningReset;
        }
        public void InitializeLearning(Dictionary<string, float> parameters, float featureDetectorThreshold = 100)
        {
            try
            {
                InitialLearnerValues = parameters;
                Learner.InitializeLearning(InitialLearnerValues);
                Rules.rewardIssued += Rules_rewardIssued;
                Rules.rulesReset += Rules_rulesReset;
                Rules.scriptError += Rules_scriptError; 
            }
            catch (Exception e)
            {
                Debug.Write("Error initializing learning: ", e.Message);
                if (learningError != null)
                    learningError(this, null);
            }
        }
        void Rules_scriptError(object sender, RewardEventArgs e)
        {
            if (learningError != null)
                learningError(this, null);
        }
        void Rules_rewardIssued(object sender, RewardEventArgs e)
        {
            Learner.Reward(e.Reward);
        }
        void Rules_rulesReset(object sender, RewardEventArgs e)
        {
            if (IsLearning)
            {
                if (learningReset != null)
                    learningReset(this, null);
                ResetEpisode(e.Reward);
            }
        }
        void Learner_learningStart(object sender, LearningEventArgs e)
        {
            IsLearning = true;
            SimulationTaskSemaphore.Wait(1000);
            Task.Run(() =>
            {
                SimulationStep();
            }).ContinueWith((r) => 
            {
                SimulatorValues = new Dictionary<string, float>(InitialValues);
                Simulator.ResetState();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            });
        }
        void Learner_learningReset(object sender, LearningEventArgs e)
        {
            ResetEpisode(0);
        }
        public void SaveLearningSession(string session)
        {
            throw new NotImplementedException();
        }
        public void LoadLearningSession(string session, out IRewardRules rules, out IReinforcementLearner learner)
        {
            rules = null;
            learner = null;
        }
        public async void ResetEpisode(float reward)
        {
            try
            {
                //Debug.WriteLine("Reset learning episode - " + NumSimulations + "(Maximum:" + MaxSimulations + "), with reward - " + reward + ".");
                IsLearning = false;
                //We wait for 10ms for the simulation task to receive the reset signal.
                await Task.Delay(10);
                Rules.Reset();
                Rules.rewardIssued -= Rules_rewardIssued;
                Rules.rulesReset -= Rules_rulesReset;
                Rules.scriptError -= Rules_scriptError;
                if (Learner != null)
                    Learner.ResetLearning(reward);
                NumSimulations++;
                CurrentReward = (float)reward; // Reward given by learning rules.
                //If reward is not at or above the reward stop threshold or not exceeding the  number of simulations, then we restart learning again.
                if (NumSimulations <= MaxSimulations && CurrentReward < RewardStopThreshold && !Stop)
                {
                    InitializeLearning(InitialLearnerValues);
                }
                else
                {
                    NumSimulations = 0;
                    CurrentReward = 0;
                }
            }
            catch (Exception ex)
            {
                Debug.Write("Error resetting episode" + ex.Message);
            }
        }
        public NeuralNetworkSimulationReinforcementLearner(IPropagatorFloat simulator, float trajectoryNoise, Dictionary<string, float> initialValues, float deltaTime, float rewardStopThreshold, float maxSimulations = 1e5f)
        {
            Simulator = simulator;
            TrajectoryNoise = trajectoryNoise;
            InitialValues = initialValues;
            SimulatorValues = new Dictionary<string, float>(InitialValues);
            MaxSimulations = maxSimulations;
            DeltaTime = deltaTime;
            RewardStopThreshold = rewardStopThreshold;
            if (RndNorm == null)
                RndNorm = new Matrix<float>(10000, 10000);
            CvInvoke.Randn(RndNorm, new MCvScalar(0f), new MCvScalar(trajectoryNoise));
        }
        void SimulationStep()
        {
            try
            {
                while (IsLearning)
                {

                    //Propagate simulator.
                    float[] simInputs = new float[(int)Simulator.GetNumberOfInputs()];
                    string[] simInputKeys = new string(Simulator.GetInputMapping()).Split(',');
                    for (int i = 0; i < simInputs.Length; i++)
                    {
                        if (!SimulatorValues.TryGetValue(simInputKeys[i], out simInputs[i]))
                            simInputs[i] = 0;
                    }
                    float[] simOutputs = Simulator.PropagateF(Array.ConvertAll(simInputs, x => (float)x));
                    string[] simOutputKeys = new string(Simulator.GetOutputMapping()).Split(',');
                    //Make random initial state.
                    DateTime timeSeed = DateTime.Now;
                    Random rnd = new Random(timeSeed.Millisecond + timeSeed.Second * 1000);
                    float[,] simOutputRange = Simulator.GetOutputRangeF();
                    // Adds simulator output with trajectory noise.
                    for (int i = 0; i < simOutputs.Length; i++)
                        SimulatorValues[simOutputKeys[i]] = simOutputs[i];// + 
                            //Simulator.LinearTransformValueWithClamp(new float[]{simOutputRange[i, 0], simOutputRange[i, 1]}, RndNorm[(int)Math.Floor(rnd.NextDouble() * (RndNorm.Rows - 1)), (int)Math.Floor(rnd.NextDouble() * (RndNorm.Cols - 1))], true);
                    SimulatorValues["FTIME"] = TotalTime;
                    TotalTime += DeltaTime;

                    //Propagate rules.
                    LearnerInputs = Learner.GetInput();
                    List<Tuple<string, dynamic>> data = new List<Tuple<string, dynamic>>();
                    foreach (var values in SimulatorValues)
                    {
                        data.Add(new Tuple<string, dynamic>(values.Key, values.Value));
                    }
                    foreach (var learnerInput in LearnerInputs)
                    {
                        data.Add(new Tuple<string, dynamic>(learnerInput.Key, learnerInput.Value));
                    }
                    Rules.PropagateRules(data.ToArray());

                    //Propagate learner.
                    Learner.Input(SimulatorValues);
                    Learner.LearningPropagate();
                    Dictionary<string,float> lInputs = Learner.GetOutput(); 
                    foreach (var lInput in lInputs)
                    {
                        SimulatorValues[lInput.Key] = lInput.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                ResetEpisode(0);
                Debug.Write("Error propagating rules: ", ex.Message);
            }
            finally
            {
                SimulationTaskSemaphore.Release();
            }
        }
        public void Terminate()
        {
            Stop = true;
            IsLearning = false;
            Learner = null;
            Rules = null;

        }
        ~NeuralNetworkSimulationReinforcementLearner()
        {
            if (RndNorm != null)
            {
                RndNorm.Dispose();
            }
        }
    }
}
