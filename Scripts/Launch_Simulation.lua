StartPosX = nil
StartPosY = nil
StartPosZ = nil
StartTime = nil
CurrentPosX = nil
CurrentPosY = nil
CurrentPosZ = nil
CurrentTime = nil
GoalPosX = nil
GoalPosY = nil
GoalPosZ = nil
TimeoutCounter = 0
MaxTimeout = 100
TotalRewards = 0.000001
TotalSeconds = 10
ElapsedTime = 0
ThisReward = 0
DeltaTime = 0.03
LAND = -1
LandThreshold = 0.2
LandThresholdRange = 0.05
LateralDist = 0;

SLog('POSX,POSY,POSZ,MOTOR1,MOTOR2,MOTOR3,MOTOR4')
-- Calculates position and yaw from simulation output.
function Calculate ()
--	ROLL = ROLL + ROLLRATE * DeltaTime
--	PITCH = PITCH + PITCHRATE * DeltaTime
	YAW = YAW + YAWRATE * DeltaTime
	YAW = math.fmod(YAW, 180)
	VWorld = DirectionCosineTransform(VXb, VYb, VZb, -ROLL, -PITCH, -YAW)
	POSX = POSX + VWorld.X * DeltaTime
	POSY = POSY + VWorld.Y * DeltaTime
	POSZ = POSZ + VWorld.Z * DeltaTime
	if StartPosX == nil then
		LAND = -1
	else
		LAND = math.tanh((POSZ - StartPosZ - LandThreshold) / (0.5 * LandThresholdRange))
	end

end

function Propagate () 

	if CurrentTime == nil or CurrentTime == 0 then
			CurrentPosX = POSX
			CurrentPosY = POSY
			CurrentPosZ = POSZ
			CurrentTime = FTIME
			-- Log('Learning  started.. \n Goal POSZ is ' .. GoalPosZ .. ' and Start pos Z is ' .. StartPosZ .. '\n')
	else

		if FTIME <= CurrentTime then
			TimeoutCounter = TimeoutCounter + 1
		else
			DeltaTime = FTIME - CurrentTime
			Calculate()
			if StartTime == nil then
				StartPosX = POSX
				StartPosY = POSY
				StartPosZ = POSZ
				StartTime = FTIME
				GoalPosX = POSX
				GoalPosY = POSY 
				GoalPosZ = POSZ + LAUNCHHEIGHT
			end
			CurrentPosX = POSX
			CurrentPosY = POSY
			CurrentPosZ = POSZ
			CurrentTime = FTIME
			TimeoutCounter = 0
			LateralDist = math.sqrt(math.pow(GoalPosX - CurrentPosX, 2) + math.pow(GoalPosY - CurrentPosY, 2))
			Distance = math.sqrt(math.pow(GoalPosX - CurrentPosX, 2) + math.pow(GoalPosY - CurrentPosY, 2) + math.pow(GoalPosZ - CurrentPosZ, 2))
			
			if Distance ~= Distance or LateralDist ~= LateralDist then
				SLog('Learning reset - Simulation BLEW UP!\n')
				Log('Learning reset - Simulation BLEW UP!\n')
				Restart(0.000001)
			end
			ThrustScore = (MOTOR1 + MOTOR2 + MOTOR3 + MOTOR4) / (4 * 256)
			ThisReward = (ThrustScore + (math.pow(10, math.max(0, LAUNCHHEIGHT - Distance - 0.05)) - 1) * 100) * DeltaTime
			if LateralDist > (LAUNCHHEIGHT / 2) then
				SLog('Learning reset - Total reward is:' .. TotalRewards .. '\n')
				Log('Learning reset (LateralDistance exceeded)- Total reward is:' .. TotalRewards .. '\n')
				Restart(TotalRewards)
			end		
			--Reward(ThisReward)
			TotalRewards = TotalRewards + ThisReward
			ElapsedTime = ElapsedTime + DeltaTime
			SLog(POSX .. ',' .. POSY .. ',' .. POSZ .. ',' .. MOTOR1 .. ',' .. MOTOR2 .. ',' .. MOTOR3 .. ',' .. MOTOR4)

		end
		if TimeoutCounter >= MaxTimeout then
			SLog('Learning reset - Total reward is:' .. TotalRewards .. '\n')
			Log('Learning reset (Timeout)- Total reward is:' .. TotalRewards .. '\n')
			Restart(TotalRewards)
		end
		if ElapsedTime >= TotalSeconds then
			SLog('Learning ended successfully - Total reward is:' .. TotalRewards .. '\n')
			Log('Learning ended successfully - Total reward is:' .. TotalRewards .. '\n')
			Restart(TotalRewards)
		end
	end
end