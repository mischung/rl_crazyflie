StartPosX = nil
StartPosY = nil
StartPosZ = nil
StartTime = nil
CurrentPosX = nil
CurrentPosY = nil
CurrentPosZ = nil
CurrentTime = nil
GoalPosX = nil
GoalPosY = nil
GoalPosZ = nil
TimeoutCounter = 0
MaxTimeout = 100
TotalRewards = 0
TotalSeconds = 3
ElapsedTime = 0
ThisReward = 0
function Propagate () 

	if StartTime == nil then
		StartPosX = POSX
		StartPosY = POSY
		StartPosZ = POSZ
		StartTime = FTIME
		GoalPosX = POSX
		GoalPosY = POSY 
		GoalPosZ = POSZ + LAUNCHHEIGHT
	end
	if CurrentTime == nil then
			CurrentPosX = POSX
			CurrentPosY = POSY
			CurrentPosZ = POSZ
			CurrentTime = FTIME
			Log('Learning  started.. \n Goal POSZ is ' .. GoalPosZ .. ' and Start pos Z is ' .. StartPosZ .. '\n')
	else

		if FTIME <= CurrentTime then
			TimeoutCounter = TimeoutCounter + 1
		else
			DeltaTime = FTIME - CurrentTime
			CurrentPosX = POSX
			CurrentPosY = POSY
			CurrentPosZ = POSZ
			CurrentTime = FTIME
			TimeoutCounter = 0
			LateralDist = math.sqrt(math.pow(GoalPosX - CurrentPosX, 2) + math.pow(GoalPosY - CurrentPosY, 2))
			Distance = math.sqrt(math.pow(GoalPosX - CurrentPosX, 2) + math.pow(GoalPosY - CurrentPosY, 2) + math.pow(GoalPosZ - CurrentPosZ, 2))
			ThisReward = (math.pow(10, math.max(0, LAUNCHHEIGHT - Distance - 0.05)) - 1) * DeltaTime
			if LateralDist > (LAUNCHHEIGHT / 2) then
				Restart(0)
				Log('Learning reset - Total reward is:' .. TotalRewards .. '\n')
			end		
			Reward(ThisReward)
			TotalRewards = TotalRewards + ThisReward
			ElapsedTime = ElapsedTime + DeltaTime
		end
		if TimeoutCounter >= MaxTimeout then
			Restart(0)
			Log('Learning reset - Total reward is:' .. TotalRewards .. '\n')
		end
		if ElapsedTime >= TotalSeconds then
			Restart(0)
			Log('Learning ended successfully - Total reward is:' .. TotalRewards .. '\n')
		end
	end
end